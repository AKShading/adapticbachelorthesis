using System.Collections.Generic;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SliderController : MonoBehaviour
    {
        [SerializeField] private Slider _slider;
        [SerializeField] private Image _imageToChange;
        [SerializeField] private List<Sprite> _volumeImages;
        [SerializeField] private Button _minus;
        [SerializeField] private Button _plus;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private GameEvent _volumeChangedGameEvent;
        
        private void OnEnable()
        {
            _slider = GetComponent<Slider>();
            _minus.gameObject.SetActive(_playerData.tremble);
            _plus.gameObject.SetActive(_playerData.tremble);
            _slider.value = _playerData.soundVolume * 100f;
        }

        public void ChangeSliderValueBy(int val)
        {
            var value = _slider.value;
            value += val;
            Mathf.Clamp(value, _slider.minValue, _slider.maxValue);
            _slider.value = value;
            SliderValueChanged();
        }

        public void SliderValueChanged()
        {
            _playerData.soundVolume = _slider.value / 100f;
            if (_volumeChangedGameEvent != null)
            {
                _volumeChangedGameEvent.Raise();
            }
            CheckIfNewImageIsNeeded();
        }

        private void CheckIfNewImageIsNeeded()
        {
            var neededIndex = CheckNeededImageIndex();

            Mathf.Clamp(neededIndex, 0, _volumeImages.Count - 1);
            _imageToChange.sprite = _volumeImages[neededIndex];
        }

        private int CheckNeededImageIndex()
        {
            var imageCount = _volumeImages.Count;
            var min = (int) _slider.minValue;
            var max = (int) _slider.maxValue;
            var value = (int) _slider.value;            
            var step = max / (imageCount - 2); 
            
            if (value == min)
            {
                return  0;
            }
            
            if (value == max)
            {
                return imageCount - 1;
            }

            for (var i = 1; i < imageCount - 1; i++)
            {
                if (value < (i * step))
                {
                    return i;
                }
            }
            return 0;
        }
    }
}