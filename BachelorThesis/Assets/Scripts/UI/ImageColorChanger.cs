using System.Collections.Generic;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class ImageColorChanger : MonoBehaviour
    {
        [SerializeField] private List<Image> _imagesToChange;
        [SerializeField] private Color _colorToChangeTo;
        [SerializeField] private PlayerData _playerData;

        private void OnEnable()
        {
            if (_playerData.redGreenBlind)
            {
                ChangeColors();
            }
        }

        private void ChangeColors()
        {
            foreach (var image in _imagesToChange)
            {
                image.color = _colorToChangeTo;
            }
        }
    }
}