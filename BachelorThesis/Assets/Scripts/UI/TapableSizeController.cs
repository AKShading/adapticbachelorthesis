﻿using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TapableSizeController : MonoBehaviour 
    {
        [SerializeField] private FloatVariable _pixelsPerPoint;
        [SerializeField] private FloatVariable _tapableSize;
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private bool _width;
        [SerializeField] private bool _height;
        
        private void Awake()
        {
            SetSizes();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
        }

        private void SetSizes()
        {   
            var layoutElement = _rectTransform.GetComponent<LayoutElement>();
            var size = _tapableSize.value * _pixelsPerPoint.value;
            
            if (layoutElement == null)
            {
                _rectTransform.sizeDelta = new Vector2(_width ? size : _rectTransform.sizeDelta.x, _height ? size : _rectTransform.sizeDelta.y);
                return;
            }
            
            if (_width && (size > layoutElement.minWidth))
            {
                layoutElement.minWidth = size;
            }
            
            if (_height && (size > layoutElement.minHeight))
            {
                layoutElement.minHeight = size;
            }
        }
    }
}
