using System;
using System.Globalization;
using Common.Events;
using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreMarkTextController : MonoBehaviour
    {
        private static readonly int Fade = Animator.StringToHash("Fade");
        
        [SerializeField] private Animator _animator;
        [SerializeField] private TMP_Text _text;

        private CultureInfo _elGR = CultureInfo.CreateSpecificCulture("el-GR");
        
        public void OnReachedScoreMark(IntGameEvent e)
        {
            _text.text = string.Format(_elGR, "{0:0,0} Punkte!", e.parameter);
            _animator.SetTrigger(Fade);
        }
    }
}