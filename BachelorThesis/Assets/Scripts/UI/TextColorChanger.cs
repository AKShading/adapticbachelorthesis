using System.Collections.Generic;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class TextColorChanger : MonoBehaviour
    {
        [SerializeField] private List<Text> _textsToChange;
        [SerializeField] private Color _colorToChangeTo;
        [SerializeField] private PlayerData _playerData;

        private void OnEnable()
        {
            if (_playerData.redGreenBlind)
            {
                ChangeColors();
            }
        }

        private void ChangeColors()
        {
            foreach (var text in _textsToChange)
            {
                text.color = _colorToChangeTo;
            }
        }
    }
}