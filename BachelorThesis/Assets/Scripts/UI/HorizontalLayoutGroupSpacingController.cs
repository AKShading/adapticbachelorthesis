using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HorizontalLayoutGroupSpacingController : MonoBehaviour
    {
        [SerializeField] private HorizontalLayoutGroup _horizontalLayoutGroup;
        [SerializeField] private FloatVariable _pixelsPerPoint;
        [SerializeField] private FloatVariable _minPadding;

        private void OnEnable()
        {
            _horizontalLayoutGroup.spacing = Mathf.Max(_minPadding.value * _pixelsPerPoint.value, _horizontalLayoutGroup.spacing);
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        }

    }
}