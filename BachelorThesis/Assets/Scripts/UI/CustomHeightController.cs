using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CustomHeightController : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private float _factorOfScreenHeight;
        [SerializeField] private Vector2Variable _resolution;

        private void Start()
        {
            SetSizes();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
        }

        private void SetSizes()
        {
            var layoutElement = _rectTransform.GetComponent<LayoutElement>();

            var size = _factorOfScreenHeight * _resolution.value.y;

            if (layoutElement == null)
            {
                var sizeDelta = _rectTransform.sizeDelta;
                sizeDelta.y = sizeDelta.y < size ? size : sizeDelta.y;
                _rectTransform.sizeDelta = sizeDelta;
                return;
            }

            if (size > layoutElement.minHeight)
            {
                layoutElement.minHeight = size;
            }
        }

    }
}