using System.Collections.Generic;
using Common.Variables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LanguageChooser : MonoBehaviour
    {
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private TMP_Dropdown _dropdown;
        [SerializeField] private List<Toggle> _toggles;
        
        private void Start()
        {
            _dropdown.gameObject.SetActive(!_playerData.tremble);
            
            foreach (var toggle in _toggles)
            {
                toggle.gameObject.SetActive(_playerData.tremble);
            }
        }

        private void Update()
        {
            _dropdown.RefreshShownValue();
        }
    }
}