using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class SoundButtonController : MonoBehaviour
    {
        [SerializeField] private GameEvent _soundButtonPressedGameEvent;
        [SerializeField] private BoolVariable _soundUnmuted;
        [SerializeField] private Image _imageToChange;
        [SerializeField] private Sprite _muted;
        [SerializeField] private Sprite _unmuted;
        
        private void OnEnable()
        {
            ChangeImage();
        }

        public void OnButtonPressed()
        {
            _soundUnmuted.value = !_soundUnmuted.value;
            ChangeImage();
            if (_soundButtonPressedGameEvent != null)
            {
                _soundButtonPressedGameEvent.Raise();
            }
        }

        private void ChangeImage()
        {
            _imageToChange.sprite = _soundUnmuted.value ? _unmuted : _muted;
        }
    }
}