﻿using System.Collections.Generic;
using System.Net.Mime;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
	public class PauseGameController : MonoBehaviour
	{
		[SerializeField] private GameEvent _pauseButtonPressedGameEvent;
		[SerializeField] private GameEvent _cancelGameButtonPressedGameEvent;
		[SerializeField] private LevelStateVariable _levelState;
		[SerializeField] private Button _homeButton;
		[SerializeField] private Button _soundButton;
		[SerializeField] private Image _imageToChange;
		[SerializeField] private Sprite _resume;
		[SerializeField] private Sprite _pause;
		
		public void OnPauseButtonPressed()
		{
			if (_pauseButtonPressedGameEvent != null)
			{
				_pauseButtonPressedGameEvent.Raise();		
			}
		}

		public void OnLevelStateChanged()
		{
			if (_levelState.value.showPauseMenu)
			{
				ShowPauseMenu(true);
			}

			if (_levelState.value.running)
			{
				ShowPauseMenu(false);
			}
		}

		private void ShowPauseMenu(bool visible)
		{
			_homeButton.gameObject.SetActive(visible);
			_soundButton.gameObject.SetActive(visible);
			_imageToChange.sprite = visible ? _resume : _pause;
		}

		public void OnCancelGameButtonPressed()
		{
			if (_cancelGameButtonPressedGameEvent != null)
			{
				_cancelGameButtonPressedGameEvent.Raise();
			}
		}
	}
}
