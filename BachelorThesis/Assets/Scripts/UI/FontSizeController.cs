using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class FontSizeController : MonoBehaviour
    {
        [SerializeField] private FloatVariable _pixelsPerPoint;
        [SerializeField] private Text _text;

        private int _normalSize; 
        
        private void Awake()
        {
            _text.fontSize = (int) (_text.fontSize * _pixelsPerPoint.value);
        }
    }
}