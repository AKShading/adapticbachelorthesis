using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Common.Configs;
using Common.Events;
using Common.Variables;
using UnityEngine;

namespace UI
{
    public class PanInformationPreparator : InformationPreparator
    {
        private const string RED = "1";
        private const string GREEN = "2";
        private const string LEFT = "l";
        private const string RIGHT = "r";
        
        [SerializeField] private IntVariable _left;
        [SerializeField] private IntVariable _right;
        [SerializeField] private FloatVariable _halfLeft;
        [SerializeField] private FloatVariable _halfRight;
        [SerializeField] private IntVariable _middle;
        [SerializeField] private IntVariable _undefined;

        [SerializeField] private GameEvent _getValuesFromBucket;
        [SerializeField] private float _acceptedVarianceFactor;
        [SerializeField] private float _acceptedMiddleMargin = 10f;
        [SerializeField] private float _minRedGreenCorrectPercentage;
        
        private int _screenHeight;
        private int _screenWidth;
        private float _halfWidth;
        private float _halfHeight;
        
        private List<InputController.TapInitInfo> _tapInitInfoObjects = new List<InputController.TapInitInfo>();
        private FlowerLevelConfig _curFlowerLevel
        {
            get { return _curLevel.value as FlowerLevelConfig; }
        }

        private WaterLevelConfig _curWaterLevel
        {
            get { return _curLevel.value as WaterLevelConfig; }
        }

        private class PanPositionAndDirectionInfo
        {
            public Vector3 startPos;
            public IntVariable dirFrom; // means where on screen startPos is situated, left, middle, right
            public IntVariable dirTo; // direction to which the pan is going, -1 left, 1 right, 0 straight
            public string pattern; // shows if the pan is like a curve etc.
            public IntVariable hand; // -1 left, 1 right, 0 = anderer Finger, sehr gerade
            public bool successful;
            public bool hit; 
        }

        private class HandAndDistance
        {
            public IntVariable hand;
            public float distanceToEdge;
        }
        
        public void OnLevelStateChanged()
        {
            if (!_levelState.value.calculateGameOverValues)
            {
                return;
            }

            var w = Screen.width;
            var h = Screen.height;

            _screenHeight = _curLevel.value.portraitModus ? Mathf.Max(w, h) : Mathf.Min(w, h);
            _screenWidth = _curLevel.value.portraitModus ? Mathf.Min(w, h) : Mathf.Max(w, h);
            _halfWidth = _screenWidth * 0.5f;
            _halfHeight = _halfHeight * 0.5f;

            CheckIfDeviceIsHeldInHand();
            EvalCurLevel();
            DrawPanLevelValues();
            
            ChangedDataNeedToBeSaved();

            if (_calculatedGameOverValuesGameEvent)
            {
                _calculatedGameOverValuesGameEvent.Raise();
            }
        }

        private void EvalCurLevel()
        {
            if (_curLevel.value.panLevel && _curLevel.value.tapLevel)
            {
                PrepareFlowerLevelInfos();
                EvalCurFlowerLevel();    
            }
            else if (_curLevel.value.panLevel)
            {
                EvalCurWaterLevel(); 
            }
        }

        private void PrepareFlowerLevelInfos()
        {
            if (_getValuesFromBucket != null)
            {
                _getValuesFromBucket.Raise();
            }


            foreach (var panInfo in _curFlowerLevel.panInfoObjects)
            {
                _tapInitInfoObjects.Add(panInfo.tapInit);
            }

            var avgAccuracy = CalculateAvgAccuracy(_tapInitInfoObjects);
            _curLevel.value.avgAccuracy.accuracyPercent = avgAccuracy;
            //_curLevel.value.avgAccuracy.accuracyString = InputController.AccuracyToString(avgAccuracy);
        }
        
        private void EvalCurFlowerLevel()
        {
            var panObjectCounter = 0;
            var levelInfo = new LevelInfo();
            var pointsPerSecond = 0f;
            var tremble = 0f;
            var redAmount = _curFlowerLevel.redYellowInTargetBucket + _curFlowerLevel.redYellowInWrongBucket;
            var greenAmount = _curFlowerLevel.greenBlueInTargetBucket + _curFlowerLevel.greenBlueInWrongBucket;
            var hitSthWhilePanning = 0;
            var dist = 0f;
            var normDist = 0f;
            var successful = 0;
            var trembleList = new List<float>();
            
            // l = links, r = rechts, o = oben, u = unten, 1 = rot(gelb), 2 = grün(blau) (depends on level)
            var sideAndColor = new List<string>();
             
            foreach (var panInfo in _curFlowerLevel.panInfoObjects)
            {
                if (panInfo.tapInit.hitTarget)
                {
                    panObjectCounter++;
                    var color = panInfo.tapInit.targetRedYellow ? RED : GREEN;
                    var sideH = panInfo.tapInit.tapPos.x < _halfWidth ? LEFT : RIGHT;
                    sideAndColor.Add(string.Format("{0}{1}", color, sideH));
                    
                    var checkedDuration = panInfo.duration <= 0f ? 0.001f : panInfo.duration;
                    var checkPPP = _pixelsPerPoint.value <= 0f ? 1f : _pixelsPerPoint.value; 
                    pointsPerSecond += panInfo.distance / checkPPP / checkedDuration;
                    
                    
                    var checkedtrembleFactor = panInfo.normalizedDistance <= 0 ? 1 : panInfo.distance / panInfo.normalizedDistance;
                    tremble += Mathf.Max(1f,  checkedtrembleFactor);
                    trembleList.Add(Mathf.Max(1f,  checkedtrembleFactor));
                    //dist += panInfo.distance;                    
                    //normDist += panInfo.normalizedDistance > panInfo.distance ? panInfo.distance : panInfo.normalizedDistance;
                    
                    if (panInfo.endedSuccessfully)
                    {
                        successful++;
                        Debug.LogFormat("Pan ended succsessful.");
                    }
                    else
                    {
                        Debug.LogFormat("Pan ended NOT succsessful.");   
                    }
                    foreach (var pointOnPath in panInfo.pointsOnPath)
                    {
                        if (pointOnPath.hitObj)
                        {
                            hitSthWhilePanning++;
                            break; // erstmal so, später evtl besser
                        }         
                    }
                }
            }
            
            Debug.LogFormat("Section avg tremble rate2 calculated (1 = everything fine): {0}", dist/normDist);

            panObjectCounter = Mathf.Max(1, panObjectCounter);
            var avgTremble = tremble / panObjectCounter;
            var avgPointsPerSecond = pointsPerSecond / panObjectCounter;
            var greenCorrectPercentage = _curFlowerLevel.greenBlueInTargetBucket / (float) Mathf.Max(greenAmount, 1) * 100f;
            var redCorrectPercentage = _curFlowerLevel.redYellowInTargetBucket / (float) Mathf.Max(redAmount, 1) * 100f;
            var accSD = CalculateAccuracySD(_curLevel.value.avgAccuracy.accuracyPercent, _tapInitInfoObjects);
            var trembleSD = CalculateTrembleSD(avgTremble, trembleList);
            levelInfo.levelSectionInfos.Add(new PanTapLevelSectionInfo()
            {
                level = _curFlowerLevel.levelNumber,
                size = _minTapableSize.value,
                avgPointsPerSecond = avgPointsPerSecond,
                avgTrembleRate = avgTremble,
                trembleSD = trembleSD,
                amountOfGreens = greenAmount,
                greenCorrectPercentage = greenCorrectPercentage,
                amountOfReds = redAmount,
                redCorrectPercentage = redCorrectPercentage,
                avgHitSthPercentage = (float) hitSthWhilePanning / panObjectCounter * 100f,
                avgAccuracy = _curLevel.value.avgAccuracy.accuracyPercent,
                accSD = accSD,
                avgPanAnObjectPercentage = (float) panObjectCounter / _curFlowerLevel.panInfoObjects.Count * 100f,
                avgPanSuccessfullyLandedInBucket = (float) successful / panObjectCounter * 100f,
                sideAndColorList = sideAndColor
            });

            if (!_curFlowerLevel._extraLevel && !_playerData.redGreenBlind)
            {
                _playerData.redGreenBlind = greenCorrectPercentage < _minRedGreenCorrectPercentage || redCorrectPercentage < _minRedGreenCorrectPercentage || redAmount == 0 || greenAmount == 0;
            }

            if (!_playerData.tremble)
            {
                _playerData.tremble = avgTremble >= 1.2f;
            }
            _playerData.flowerLevelInfo.Add(levelInfo);
        }

        private void EvalCurWaterLevel()
        {
            // ist stein links -1 , rechts 1 oder mittig 0
            // trifft er linke oder rechte steine besser und nicht nur checken ob es ein treffer ist sondern ob pan erfolgreich
            
            // 0 means (nearly) start pos, 1 means right of startpos, -1 means left of startpos
            // vorher noch schauen ob rechte oder linke seite des bildschirms
            
            var panPositionAndDirectionList = new List<PanPositionAndDirectionInfo>();
            
            foreach (var panInfo in _curWaterLevel.panInfoObjects)
            {   
                var directionList = new List<float>();
                var panPositionAndDirectionInfo = new PanPositionAndDirectionInfo();

                if (!panInfo.tapInit.hitTarget)
                {
                    panPositionAndDirectionInfo.hit = false;
                    if (!_playerData.tremble)
                    {
                        panPositionAndDirectionList.Add(panPositionAndDirectionInfo);
                        continue;   
                    }
                }
                else
                {
                    panPositionAndDirectionInfo.hit = true;
                }
             
                /*
                 * Zu jedem Pan wird der initiale x-Wert der Position gespeichert. Zunächst werden die beiden in positiver und negativer Richtung maximal abweichenden Punkte in der Liste der Strecken-Punkte gesucht und
                 * zwischengespeichert. Der Initialwert trägt die Richtung 0. Dieser wird einer Liste von Richtungen hinzugefügt.
                 * Nun wird zu jedem Punkt der Liste festgestellt, ob er links (-0.5) oder rechts (0.5) vom Initialwert liegt oder ob er sogar der Maximalwert ist (links = -1, rechts = 1). Das Kürzel wird einer Liste hinzugefügt,
                 * sofern nicht schon der letzte Eintrag das gleiche Kürzel trägt. So ergibt sich ein Pan-Muster, das ausgewertet werden kann.
                 */
                var initialX = panInfo.tapInit.tapPos.x;
                panPositionAndDirectionInfo.successful = panInfo.endedSuccessfully;
                panPositionAndDirectionInfo.startPos = panInfo.tapInit.tapPos; 
                panPositionAndDirectionInfo.dirFrom = CheckSideOfScreen(initialX);
                panPositionAndDirectionInfo.pattern = string.Format("{0}", 0);
                directionList.Add(0);

                var leftMax = panInfo.tapInit.tapPos;
                var rightMax = panInfo.tapInit.tapPos;
                foreach (var pointOnPath in panInfo.pointsOnPath)
                {
                    var pos = pointOnPath.pos;
                    if (pos.x < leftMax.x)
                    {
                        leftMax = pos;
                    }
                    if (pos.x > rightMax.x)
                    {
                        rightMax = pos;
                    }
                }
                
                foreach (var pointOnPath in panInfo.pointsOnPath)
                {
                    var pos = pointOnPath.pos;

                    var dir = (float) _middle.value;
                    if (pos.x < initialX - _acceptedMiddleMargin)
                    {
                        if (pos == leftMax)
                        {
                            dir = _left.value;
                        }
                        else
                        {
                            dir = _halfLeft.value;
                        }
                    }
                    else if (pos.x > initialX + _acceptedMiddleMargin)
                    {
                        if (pos == rightMax)
                        {
                            dir = _right.value;
                        }
                        else
                        {
                            dir = _halfRight.value;
                        }
                    }
                    
                    
                    if (directionList[directionList.Count - 1] != dir)
                    {
                        panPositionAndDirectionInfo.pattern += string.Format("{0}", dir);
                    }
                    directionList.Add(dir);
                   // Debug.Log(dir + ", ");   
                }
                
                CheckHandAndDirection(panPositionAndDirectionInfo);
                
                Debug.LogFormat("##### DIR STRING IS: {0} #####", panPositionAndDirectionInfo.pattern);
                Debug.LogFormat("##### Dir from: {0}. Dir to: {1}. Hand used: {2}. #####", panPositionAndDirectionInfo.dirFrom.value, panPositionAndDirectionInfo.dirTo.value, panPositionAndDirectionInfo.hand.value);
                Debug.Log("##### end #####");
                
                panPositionAndDirectionList.Add(panPositionAndDirectionInfo);
            }

            // check tremble - to pan anywhere
            
            if (_curWaterLevel.initHandednessLevel)
            {
                _playerData.waterLevelInfo.Add(EvalInitHandednessLevel(panPositionAndDirectionList));         
            }
            else
            {
                _playerData.waterLevelInfo.Add(EvalHandUsedAndDistancesLevel(panPositionAndDirectionList));
            }
        }

        private LevelInfo EvalInitHandednessLevel(List<PanPositionAndDirectionInfo> panPositionAndDirectionList)
        {
            var levelInfo = new LevelInfo();
            var left = 0;
            var right = 0;
            var undefined = 0;
            var successful = 0;
            var missed = 0;
            var patterns = new Dictionary<string, int>();
            
            foreach (var pan in panPositionAndDirectionList)
            {
                if (!pan.hit)
                {
                    missed++;
                    if (!_playerData.tremble)
                    {
                        continue;   
                    }
                }

                if (!patterns.ContainsKey(pan.pattern))
                {
                    patterns.Add(pan.pattern, 1);
                }
                else
                {
                    patterns[pan.pattern] += 1;
                }
                
                if (pan.successful)
                {
                    successful++;
                }
                
                if (pan.hand == _right)
                {
                    right++;
                }
                else if (pan.hand == _left)
                {
                    left++;
                }
                else
                {
                    undefined++;
                }
                    
                Debug.LogFormat("Hand used: {0}.", pan.hand);
            }
            _playerData.handedness = right > left ? _right : left > right ? _left : (right == 0 && left == 0) ? _undefined : _right;
            Debug.LogFormat("Player is {0} handed.", _playerData.handedness.name);

            levelInfo.levelSectionInfos.Add(new CheckHandednessLevelSectionInfo()
            {
                level = _curWaterLevel.levelNumber,
                usedRightHandPercentage = right / (float) panPositionAndDirectionList.Count * 100f,
                usedLeftHandPercentage = left / (float) panPositionAndDirectionList.Count * 100f,
                percentageStartedPanFromCorrectPos = (panPositionAndDirectionList.Count - missed) / (float) panPositionAndDirectionList.Count * 100f,
                percentageOfSuccessfullyPanned = successful / (float) panPositionAndDirectionList.Count * 100f,
                panPattern = string.Join(";", patterns.Select(x => string.Format("({0}:{1})", x.Value, x.Key)).ToArray())
                
            });
            return levelInfo;
        }

        private LevelInfo EvalHandUsedAndDistancesLevel(List<PanPositionAndDirectionInfo> panPositionAndDirectionList)
        {
            var levelInfo = new LevelInfo();
            var handAndDistanceList = new List<HandAndDistance>();
            var farthestPosRight = (float) _screenWidth;
            var farthestPosLeft = 0f;
            var throwingsPerPos = 0;
            var left = 0;
            var right = 0;
            var undefined = 0;
            var successful = 0;
            var missed = 0;
            var patterns = new Dictionary<string, int>();

            foreach (var pan in panPositionAndDirectionList)
            {
                if (!pan.hit)
                {
                    missed++;
                    if (!_playerData.tremble)
                    {
                        continue;   
                    }
                }
                
                if (!patterns.ContainsKey(pan.pattern))
                {
                    patterns.Add(pan.pattern, 1);
                }
                else
                {
                    patterns[pan.pattern] += 1;
                }
                
                if (pan.successful)
                {
                    successful++;
                }
                
                if (pan.hand == _right)
                {
                    right++;
                    farthestPosRight = pan.startPos.x < farthestPosRight ? pan.startPos.x : farthestPosRight;
                }
                else if (pan.hand == _left)
                {
                    left++;
                    farthestPosLeft = pan.startPos.x > farthestPosLeft ? pan.startPos.x : farthestPosLeft;
                }
                else
                {
                    undefined++;
                }

                throwingsPerPos++;
                if (throwingsPerPos == _curWaterLevel.amountOfThrowingsPerPos)
                {
                    var handAndDistance = new HandAndDistance();
                    handAndDistance.hand = right > left ? _right : left > right ? _left : (right == 0 && left == 0) ? _undefined : _playerData.handedness == _right ? _right : _left;
                    handAndDistance.distanceToEdge = handAndDistance.hand == _right ? _screenWidth - farthestPosRight : handAndDistance.hand == _left ? farthestPosLeft : 0f;
                   
                    Debug.LogFormat("Used hand: {0}. Distance to edge of screen: {1}.", handAndDistance.hand, handAndDistance.distanceToEdge);
                    handAndDistanceList.Add(handAndDistance);

                    var panStartedFromCorrectPos = _playerData.tremble ? (throwingsPerPos - missed) / (float) throwingsPerPos * 100f : (throwingsPerPos) / (float) (throwingsPerPos + missed) * 100f;

                    
                    levelInfo.levelSectionInfos.Add(new CheckHandUsedAndDistancesLevelSectionInfo()
                    {
                        level = _curWaterLevel.levelNumber,
                        section = levelInfo.levelSectionInfos.Count + 1,
                        handUsed = handAndDistance.hand.name,
                        distanceToEdgeOfScreen = handAndDistance.distanceToEdge / _pixelsPerPoint.value,
                        percentageStartedPanFromCorrectPos = panStartedFromCorrectPos,
                        percentageOfSuccessfullyPanned = successful / (float) throwingsPerPos * 100f, 
                        panPattern = string.Join(";", patterns.Select(x => string.Format("({0}:{1})", x.Value, x.Key)).ToArray())
                    });

                    throwingsPerPos = left = right = undefined = successful = missed = 0;
                    farthestPosLeft = 0f;
                    farthestPosRight = _screenWidth;
                    patterns.Clear();
                }
            }
            return levelInfo;
        }

        private IntVariable CheckSideOfScreen(float posX)
        {
            if (_curWaterLevel.initHandednessLevel)
            {
                return _middle;
            }
                
            return posX < _halfWidth - _acceptedMiddleMargin ? _left : posX > _halfWidth + _acceptedMiddleMargin ? _right : _middle;
        }

        private void CheckHandAndDirection(PanPositionAndDirectionInfo info)
        {
            var right0 = "0-0.5-1-0.5"; // starts with
            var right1 = "0-0.5-10.5"; 
            var right2 = "0-0.5-10";
            var right3 = "0-0.5-11";
            var right4 = "0-0.50";
            var right5 = "0-10"; 
            var right6 = "0-10.5";
            var right7 = "0-1-0.5";
            var right8 = "0-11";
            
            var startWithRightPatterns = new List<string>{right0, right1, right2, right3, right4, right5, right6, right7, right8};
            
            var right9  = "00.51"; // exact
            var right10 = "00.501";
            var right11 = "01";
            
            var exactRightPatterns = new List<string> {right9, right10, right11};
            
            var left0 = "00.510.5";// starts with
            var left1 = "00.51-0.5";
            var left2 = "00.510";
            var left3 = "00.51-1";
            var left4 = "00.50";
            var left5 = "010"; 
            var left6 = "01-0.5"; 
            var left7 = "010.5";
            var left8 = "01-1";
            
            var startWithLeftPatterns = new List<string>{left0, left1, left2, left3, left4, left5, left6, left7, left8};
            
            var left9  = "0-0.5-1"; //exact
            var left10 = "0-0.50-1";
            var left11 = "0-1";

            var exactLeftPatterns = new List<string> {left9, left10, left11};

            if(info.pattern == "0")
            {
                info.dirTo = _middle;
                info.hand = _undefined;
                return;
            }

            foreach (var val in exactRightPatterns)
            {
                if (info.pattern == val)
                {
                    info.dirTo = info.hand = _right;
                    return;
                }
            }      
            
            foreach (var val in exactLeftPatterns)
            {
                if (info.pattern == val)
                {
                    info.dirTo = info.hand = _left;
                    return;
                }
            } 
            
            foreach(var val in  startWithRightPatterns)
            {
                if (info.pattern.StartsWith(val))
                {
                    info.dirTo = info.hand = _right;
                    return;
                }
            }
            
            foreach(var val in  startWithLeftPatterns)
            {
                if (info.pattern.StartsWith(val))
                {
                    info.dirTo = info.hand = _left;
                    return;
                }
            }

            info.dirTo = info.hand = _undefined;             
        }

        private bool CheckIfCurved(float dist, float normDist)
        {
            var variance = dist - normDist;
            return variance > dist * _acceptedVarianceFactor;
        }      
        
        private void DrawPanLevelValues()
        {
            if (_curLevel.value.panLevel && !_curLevel.value.tapLevel)
            {
                if (_curWaterLevel.initHandednessLevel)
                {
                    DrawInitHandednessLevelValues();
                }
                else
                {
                    DrawHandUsedAndDistancesLevelValues();
                }
            }
            else if (_curLevel.value.panLevel && _curLevel.value.tapLevel)
            {
                DrawPanTapLevelValues();
            }
        }

        private void DrawInitHandednessLevelValues()
        {
            foreach (var levelSectionInfo in _playerData.waterLevelInfo[_playerData.waterLevelInfo.Count - 1].levelSectionInfos)
            {
                var checkHandednessLevelSectionInfo = levelSectionInfo as CheckHandednessLevelSectionInfo;
                if (checkHandednessLevelSectionInfo == null)
                {
                    Debug.LogFormat("Trying to draw check handedness level values, but it's null!");
                    return;
                }

                var values = new List<string>
                {
                    _playerData.deviceID,
                    string.Format("{0}", _playerData.handHeld ? "Handheld" : "Desktop"),
                    string.Format("{0}", checkHandednessLevelSectionInfo.usedRightHandPercentage),
                    string.Format("{0}", checkHandednessLevelSectionInfo.usedLeftHandPercentage),
                    string.Format("{0}", checkHandednessLevelSectionInfo.percentageStartedPanFromCorrectPos),
                    string.Format("{0}", checkHandednessLevelSectionInfo.percentageOfSuccessfullyPanned),
                    checkHandednessLevelSectionInfo.panPattern
                };

                if (_curWaterLevel.sendFormConfig != null)
                {
                    _curWaterLevel.sendFormConfig.Send(this, values);
                }

                Debug.LogFormat("###################################################");
                Debug.LogFormat("Information for level {0}!", checkHandednessLevelSectionInfo.level);
                Debug.LogFormat("Handheld Device: {0}", _playerData.handHeld ? "Handheld" : "Desktop");
                Debug.LogFormat("Percentage used right hand {0}!", checkHandednessLevelSectionInfo.usedRightHandPercentage);
                Debug.LogFormat("Percentage used left hand {0}!", checkHandednessLevelSectionInfo.usedLeftHandPercentage);
                Debug.LogFormat("Percentage of started pan from correct pos {0}!", checkHandednessLevelSectionInfo.percentageStartedPanFromCorrectPos);
                Debug.LogFormat("Percentage of successfully panned {0}!", checkHandednessLevelSectionInfo.percentageOfSuccessfullyPanned);
                Debug.LogFormat("Pan patterns {0}!", checkHandednessLevelSectionInfo.panPattern);
                Debug.LogFormat("###################################################");
            }
        }

        private void DrawHandUsedAndDistancesLevelValues()
        {
            foreach (var levelSectionInfo in _playerData.waterLevelInfo[_playerData.waterLevelInfo.Count - 1].levelSectionInfos)
            {
                var checkHandUsedAndDistancesLevelSectionInfo =
                    levelSectionInfo as CheckHandUsedAndDistancesLevelSectionInfo;
                if (checkHandUsedAndDistancesLevelSectionInfo == null)
                {
                    Debug.LogFormat("Trying to draw check handedness level values, but it's null!");
                    return;
                }

                var values = new List<string>
                {
                    _playerData.deviceID,
                    string.Format("{0}", checkHandUsedAndDistancesLevelSectionInfo.section),
                    string.Format("{0}", _playerData.handHeld ? "Handheld" : "Desktop"),
                    string.Format("{0}, {1}", _playerData.deviceSizeInCm.x, _playerData.deviceSizeInCm.y),
                    string.Format("{0}", checkHandUsedAndDistancesLevelSectionInfo.handUsed),
                    string.Format("{0}", checkHandUsedAndDistancesLevelSectionInfo.distanceToEdgeOfScreen),
                    string.Format("{0}", checkHandUsedAndDistancesLevelSectionInfo.percentageStartedPanFromCorrectPos),
                    string.Format("{0}", checkHandUsedAndDistancesLevelSectionInfo.percentageOfSuccessfullyPanned),
                    checkHandUsedAndDistancesLevelSectionInfo.panPattern
                };

                if (_curWaterLevel.sendFormConfig != null)
                {
                    _curWaterLevel.sendFormConfig.Send(this, values);
                }

                Debug.LogFormat("###################################################");
                Debug.LogFormat("Information for level {0}!", checkHandUsedAndDistancesLevelSectionInfo.level);
                Debug.LogFormat("Information for section {0}!", checkHandUsedAndDistancesLevelSectionInfo.section);
                Debug.LogFormat("Handheld Device: {0}", _playerData.handHeld ? "Handheld" : "Desktop");
                Debug.LogFormat("Size of screen in cm: {0}, {1}", _playerData.deviceSizeInCm.x, _playerData.deviceSizeInCm.y);
                Debug.LogFormat("Hand used {0}!", checkHandUsedAndDistancesLevelSectionInfo.handUsed);
                Debug.LogFormat("Distance to edge {0}!", checkHandUsedAndDistancesLevelSectionInfo.distanceToEdgeOfScreen);
                Debug.LogFormat("Percentage of started pan from correct pos {0}!", checkHandUsedAndDistancesLevelSectionInfo.percentageStartedPanFromCorrectPos);
                Debug.LogFormat("Percentage of successfully panned {0}!", checkHandUsedAndDistancesLevelSectionInfo.percentageOfSuccessfullyPanned);
                Debug.LogFormat("Pan patterns {0}!", checkHandUsedAndDistancesLevelSectionInfo.panPattern);
                Debug.LogFormat("###################################################");
            }
        }

        private void DrawPanTapLevelValues()
        {
            foreach (var levelSectionInfo in _playerData.flowerLevelInfo[_playerData.flowerLevelInfo.Count - 1].levelSectionInfos)
            {
                var panLevelSectionInfo = levelSectionInfo as PanTapLevelSectionInfo;
                if (panLevelSectionInfo == null)
                {
                    Debug.LogFormat("Trying to draw pan level values, but it's null!");
                    return;
                }

                var values = new List<string>
                {
                    _playerData.deviceID,
                    string.Format("{0}", panLevelSectionInfo.level),
                    string.Format("{0}", panLevelSectionInfo.size),
                    string.Format("{0}", panLevelSectionInfo.avgPointsPerSecond),
                    string.Format("{0};{1}", panLevelSectionInfo.avgTrembleRate, panLevelSectionInfo.trembleSD),
                    string.Format("{0}", panLevelSectionInfo.amountOfGreens),
                    string.Format("{0}", panLevelSectionInfo.greenCorrectPercentage),
                    string.Format("{0}", panLevelSectionInfo.amountOfReds),
                    string.Format("{0}", panLevelSectionInfo.redCorrectPercentage),
                    string.Format("{0}", panLevelSectionInfo.avgHitSthPercentage),
                    string.Format("{0};{1}", panLevelSectionInfo.avgAccuracy, panLevelSectionInfo.accSD),
                    string.Format("{0}", panLevelSectionInfo.avgPanAnObjectPercentage),
                    string.Format("{0}", panLevelSectionInfo.avgPanSuccessfullyLandedInBucket),
                    string.Join(";", panLevelSectionInfo.sideAndColorList.Select(x => x.ToString()).ToArray())
                };

                if (_curFlowerLevel.sendFormConfig != null)
                {
                    _curFlowerLevel.sendFormConfig.Send(this, values);
                }

                Debug.LogFormat("###################################################");
                Debug.LogFormat("Information for level {0}!", panLevelSectionInfo.level);
                Debug.LogFormat("Section tapable Size: {0}", panLevelSectionInfo.size);
                Debug.LogFormat("Section avg points panned per second (speed): {0}", panLevelSectionInfo.avgPointsPerSecond);
                Debug.LogFormat("Section avg tremble rate (1 = everything fine): {0}; SD: {1}", panLevelSectionInfo.avgTrembleRate, panLevelSectionInfo.trembleSD);
                Debug.LogFormat("Section amound of greens: {0}", panLevelSectionInfo.amountOfGreens);
                Debug.LogFormat("Section green correct percentage: {0}", panLevelSectionInfo.greenCorrectPercentage);
                Debug.LogFormat("Section amound of reds: {0}", panLevelSectionInfo.amountOfReds);
                Debug.LogFormat("Section red correct percentage: {0}", panLevelSectionInfo.redCorrectPercentage);
                Debug.LogFormat("Section percentage hit something while pan: {0}", panLevelSectionInfo.avgHitSthPercentage);
                Debug.LogFormat("Section avg tap accuracy: {0}, SD: {1}", panLevelSectionInfo.avgAccuracy, panLevelSectionInfo.accSD);
                Debug.LogFormat("Section percentage of panned an object while panning: {0}", panLevelSectionInfo.avgPanAnObjectPercentage);
                Debug.LogFormat("Section percentage of pans ended successfully in bucket: {0}", panLevelSectionInfo.avgPanSuccessfullyLandedInBucket);
                Debug.LogFormat("SidesAndColors: {0}", string.Join(";", panLevelSectionInfo.sideAndColorList.Select(x => x.ToString()).ToArray()));
                Debug.LogFormat("###################################################");
            }
        }
    }
}