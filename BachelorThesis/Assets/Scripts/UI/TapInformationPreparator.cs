using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Common.Configs;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class TapInformationPreparator : InformationPreparator
    {
        [SerializeField] private FloatVariable _minPaddingSize;
        [SerializeField] private IntVariable _bossHitsToDisappear;
        [SerializeField] private FloatVariable _maximumTouchPressureSupported;
        
        private MoleLevelConfig _curMoleLevel
        {
            get
            {
                return _curLevel.value as MoleLevelConfig;
            }
        }	


        public void OnLevelStateChanged()
        {
            if (!_levelState.value.calculateGameOverValues)
            {
                return;
            }

            var avgAccuracy = CalculateAvgAccuracy(_curMoleLevel.tapInitInfoObjects);
            _curLevel.value.avgAccuracy.accuracyPercent = avgAccuracy;
            //_curLevel.value.avgAccuracy.accuracyString = InputController.AccuracyToString(avgAccuracy);

            CheckIfDeviceIsHeldInHand();
            EvalCurMoleLevel();
            DrawCurMoleLevelValues();
            ChangedDataNeedToBeSaved();
            
            if (_calculatedGameOverValuesGameEvent)
            {
                _calculatedGameOverValuesGameEvent.Raise();
            }
        }
        
        public void EvalCurMoleLevel()
        {       
            // handle tapable size level
            if (!_curMoleLevel.paddingLevel)
            {
                EvalTapableSizeLevel();
            }
            else // handle padding level
            {
                EvalPaddingLevel();
            }
        }

        private void EvalPaddingLevel()
        {
            var sizesAndAmountsIndex = 0;
            var size = _suggestedMinSize.value;
            var padding = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].size;
            var amount = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].amount;
            
            var innerZoneMoleHits = 0;
            var innerZoneMoleMisses = 0;

            var outerZoneMoleHits = 0;
            var outerZoneMoleMisses = 0;

            var accidents = 0;
            
            // Einteilung der maximal Distanzen der Zonen
            var innerZoneDist = (size * 0.5f + padding) * _pixelsPerPoint.value;
            var outerZoneDist = innerZoneDist + (size * _pixelsPerPoint.value);
                        
            var levelInfo = new LevelInfo();
            
            /*
             * Nachdem die Einteilung in Zonen vorgenommen wurde, kann nun die Tap-Position mithilfe der Ziel-Position einer Zone zugeordnet werden.                 Dafür wird die Art und Weise angewand wie für die berechnung der genauigkeit im vorherigen Punkt.
             * Es wird die Distanz in horizontaler und vertikaler Richtung vom Tap zur Zielposition berechnet und der größere Wert ausgewählt.
             * Liegt dieser noch innerhalb der Maximaldistanz für die erste Zone, dann wird geprüft ob das Ziel getroffen wurde. Liegt der Tap innerhalb der zweiten Zone, so wird geprüft, ob ein resistentes Objekt getroffen wurde. Es werden jeweils Zähler inkrementiert. 
             * Ist die Distanz größer, so liegt ein Treffer außerhalb der Zonen vor, was ebenfalls eine Zähler-Inkrementierung veranlasst.  
             */

            for (var i = 0; i < _curMoleLevel.tapInitInfoObjects.Count; i++)
            {
                var targetPos = _curMoleLevel.tapInitInfoObjects[i].possibleTargetPos;
                var tapPos = _curMoleLevel.tapInitInfoObjects[i].tapPos;
                //var dist = (_curMoleLevel.tapInitInfoObjects[i].possibleTargetPos - _curMoleLevel.tapInitInfoObjects[i].tapPos).magnitude;
                var distX = Math.Abs(tapPos.x - targetPos.x);
                var distZ = Math.Abs(tapPos.y - targetPos.y);
                
                var dist = distX >= distZ ? distX : distZ;
                
                if (dist <= innerZoneDist)
                {
                    if (_curMoleLevel.tapInitInfoObjects[i].hitTarget)
                    {
                        innerZoneMoleHits++;
                    }
                    else
                    {
                        innerZoneMoleMisses++;
                    }
                }
                else if (dist <= outerZoneDist)
                {
                    if(_curMoleLevel.tapInitInfoObjects[i].hitResistent)
                    {
                        outerZoneMoleHits++;
                    }
                    else
                    {
                        outerZoneMoleMisses++;
                    }
                }
                else
                {
                    accidents++;
                }
                
                if (innerZoneMoleHits == amount)
                {
                    var innerZoneTouches = innerZoneMoleHits + innerZoneMoleMisses;
                    var outerZoneTouches = outerZoneMoleHits + outerZoneMoleMisses;
                    var touchCount = innerZoneTouches + outerZoneTouches + accidents;
                    
                    var innerZonePercentage = innerZoneTouches / (float) touchCount * 100f;
                    var innerZoneHitPercentage = innerZoneTouches == 0 ? 0 : innerZoneMoleHits / (float) innerZoneTouches * 100f;
                    
                    var outerZonePercentage = outerZoneTouches / (float) touchCount * 100f;
                    var outerZoneHitPercentage = outerZoneTouches == 0 ? 0 : outerZoneMoleHits / (float) outerZoneTouches * 100f;
                    
                    var accidentPercentage = accidents / (float) touchCount * 100f;
                        
                    levelInfo.levelSectionInfos.Add(new PaddingLevelSectionInfo
                    {
                        level = _curMoleLevel.levelNumber,
                        size = size,
                        padding = padding,
                        innerZonePercentage = innerZonePercentage,
                        outerZonePercentage = outerZonePercentage,
                        accidentPercentage = accidentPercentage,
                        innerZoneMoleHitsPercentage = innerZoneHitPercentage,
                        outerZoneMoleHitsPercentage = outerZoneHitPercentage                       
                    });

                    sizesAndAmountsIndex++;
                    if (sizesAndAmountsIndex >= _curMoleLevel.sizesAndAmounts.Count)
                    {
                        break;
                    }
                   
                    padding = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].size;
                    amount = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].amount;
                    innerZoneDist = (size * 0.5f + padding) * _pixelsPerPoint.value;
                    outerZoneDist = innerZoneDist + (size * _pixelsPerPoint.value);
                    innerZoneMoleHits = innerZoneMoleMisses = outerZoneMoleHits = outerZoneMoleMisses = accidents = 0;
                }
            }
            // calculate best padding size / worst
            if (_curMoleLevel.paddingLevel)
            {
                _minPaddingSize.value = GetMinPaddingSize(levelInfo);
            }

            _playerData.moleLevelInfo.Add(levelInfo);
        }

        private void EvalTapableSizeLevel()
        {
            var hitCounter = 0;
            var missCounter = 0;
            var sizesAndAmountsIndex = 0;
            var reactionTime = 0f;
            var hitsPerSecond = 0f;
            var missesPerSecond = 0f;
            var neededTimeForTaps = 0f;
            var tapDuration = 0f;
            var tapPressure = 0f;
            var tapPressureCount = 0;
            var levelInfo = new LevelInfo();
            
            var disappearedCount = 0;
            var firstHitTime = 0f;
            var firstReactionTime = 0f;
            var oldMisses = 0;
            var missesBetween = 0;

            var size = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].size;
            var amount = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].amount;
                
            /*
             Grundlage für die Berechnung aller Kennwerte ist die Iteriereung über die TapInitInfo-Objekte. Innerhalb dieser
             wird geprüft, ob ein Ziel getroffen wurde und dementsprechend ein Treffer- oder Verfehlungszähler hochgezählt.
             Anhand dessen ergibt sich die Trefferquote für jeden Level-Abschnitt einzeln durch folgende Formel: 
             var percentage = (hitCounter / (hitCounter + missCounter)) * 100;
               
                */
            for (var i = 0; i < _curMoleLevel.tapInitInfoObjects.Count; i++)
            {
                if (_curMoleLevel.tapInitInfoObjects[i].hitTarget)
                {
                    hitCounter++;
                    if (_curMoleLevel.bossLevel)
                    {
                        if(hitCounter % _bossHitsToDisappear.value == 1)
                        {
                            firstHitTime = _curMoleLevel.tapInitInfoObjects[i].tapTime;
                            firstReactionTime = _curMoleLevel.tapInitInfoObjects[i].reactionTime;
                            missesBetween = missCounter - oldMisses;
                        }
                        else if(hitCounter % _bossHitsToDisappear.value == 0)
                        {   
                            disappearedCount++;
                            neededTimeForTaps = firstReactionTime + _curMoleLevel.tapInitInfoObjects[i].tapTime - firstHitTime;
                            reactionTime += neededTimeForTaps / (_bossHitsToDisappear.value + missCounter - oldMisses - missesBetween);
                            hitsPerSecond += _bossHitsToDisappear.value / neededTimeForTaps;
                            missesPerSecond += (missCounter - oldMisses - missesBetween) / neededTimeForTaps;
                            oldMisses = missCounter;
                        }
                    }
                    else
                    {
                        disappearedCount++;
                        reactionTime += _curMoleLevel.tapInitInfoObjects[i].reactionTime; 
                        
                        if (disappearedCount == 1)
                        {
                            firstHitTime = _curMoleLevel.tapInitInfoObjects[i].tapTime;
                            firstReactionTime = _curMoleLevel.tapInitInfoObjects[i].reactionTime;
                        }                                                                            
                    }
                }
                else
                {
                    missCounter++;
                }

                if (Input.touchPressureSupported)
                {
                    foreach (var pressure in _curMoleLevel.tapInitInfoObjects[i].tapPressure)
                    {
                        if (pressure > 0)
                        {
                            tapPressure += pressure;
                            tapPressureCount++;    
                        }
                    }
                }

                tapDuration += _curMoleLevel.tapInitInfoObjects[i].tapDuration;
                
                if(disappearedCount == amount)
                {
                    var percentage = hitCounter / (float)(hitCounter + missCounter);
                    percentage *= 100f;
                    var accuracy = CalculateAvgAccuracyInRange(_curMoleLevel.tapInitInfoObjects, i + 1  - hitCounter - missCounter, i);
                    var accSD = CalculateAccuracySDInRange(accuracy, _curMoleLevel.tapInitInfoObjects, i + 1  - hitCounter - missCounter, i);
                    var avgTapDuration = tapDuration / (hitCounter + missCounter);
                    var avgTapPressure = Input.touchPressureSupported ? (tapPressure / tapPressureCount) / _maximumTouchPressureSupported.value * 100f : -1;
                    
                    if (!_curMoleLevel.bossLevel)
                    {
                        // only important for speed level
                        hitsPerSecond = 0;
                        missesPerSecond = 0;
                    }
                    else
                    {
                        hitsPerSecond = hitsPerSecond / disappearedCount;
                        missesPerSecond = missesPerSecond / disappearedCount;
                    }
                    
                    levelInfo.levelSectionInfos.Add(new TapableLevelSectionInfo
                    {
                        level = _curMoleLevel.levelNumber, 
                        size = size, 
                        hitPercentage = percentage, 
                        avgAccuracy = accuracy, 
                        accSD = accSD,
                        avgReactionTime = reactionTime / disappearedCount,                     
                        avgHitsPerSecond = hitsPerSecond,
                        avgMissesPerSecond = missesPerSecond,
                        avgTapDuration = avgTapDuration,
                        avgTapPressurePercentageOfMax = avgTapPressure
                    });
                    
                    sizesAndAmountsIndex++;
                    if (sizesAndAmountsIndex >= _curMoleLevel.sizesAndAmounts.Count)
                    {
                        break;
                    }
                    size = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].size;
                    amount = _curMoleLevel.sizesAndAmounts[sizesAndAmountsIndex].amount;
                    hitCounter = missCounter = missesBetween = disappearedCount = tapPressureCount = 0;
                    reactionTime = neededTimeForTaps = hitsPerSecond = tapDuration = tapPressure = 0f;
                }
            }

            // calculate best tapable size / worst
            if (!_curMoleLevel.bossLevel && !_curMoleLevel.paddingLevel)
            {
                _minTapableSize.value = GetMinTapableSize(levelInfo);
                _playerData.tapDuration = GetAvgTapDuration(levelInfo);
            }
            
            _playerData.moleLevelInfo.Add(levelInfo);
        }

        private float GetMinTapableSize(LevelInfo levelInfo)
        {
            var perc = 0f;
            var index = 0;
            var acc = 0f;
            
            for (var i = 0; i < levelInfo.levelSectionInfos.Count; i++)
            {
                var tapableLevelSectionInfo = levelInfo.levelSectionInfos[i] as TapableLevelSectionInfo;
                if (tapableLevelSectionInfo == null)
                {
                    return _minTapableSize.value;
                }

                if ((tapableLevelSectionInfo.hitPercentage + tapableLevelSectionInfo.avgAccuracy) > (perc + acc))
                {
                    acc = tapableLevelSectionInfo.avgAccuracy;
                    perc = tapableLevelSectionInfo.hitPercentage;
                    index = i;
                }
            }
            return levelInfo.levelSectionInfos[index].size;
        }

        private float GetAvgTapDuration(LevelInfo levelInfo)
        {
            var avgDuration = 0f;
            
            foreach (var levelSectionInfo in levelInfo.levelSectionInfos)
            {
                var tapableLevelSectionInfo = levelSectionInfo as TapableLevelSectionInfo;
                if (tapableLevelSectionInfo == null)
                {
                    return -1;
                }
                avgDuration += tapableLevelSectionInfo.avgTapDuration;
            }

            Debug.LogFormat("AVG tap duration end: {0}", avgDuration / levelInfo.levelSectionInfos.Count);
            return avgDuration / levelInfo.levelSectionInfos.Count;
        }

        private float GetMinPaddingSize(LevelInfo levelInfo)
        {
            var innerZonePerc = 0f;
            var innerZoneHitPerc = 0f;
            var padding = 0f;
            
            for (var i = 0; i < levelInfo.levelSectionInfos.Count; i++)
            {
                var paddingLevelSectionInfo = levelInfo.levelSectionInfos[i] as PaddingLevelSectionInfo;
                if (paddingLevelSectionInfo == null)
                {
                    return _minPaddingSize.value;
                }
                
                if((paddingLevelSectionInfo.innerZonePercentage + paddingLevelSectionInfo.innerZoneMoleHitsPercentage) > (innerZonePerc + innerZoneHitPerc))
                {
                    innerZonePerc = paddingLevelSectionInfo.innerZonePercentage;
                    innerZoneHitPerc = paddingLevelSectionInfo.innerZoneMoleHitsPercentage;
                    padding = paddingLevelSectionInfo.padding;
                }
            }
            return padding;
        }       
        
        private static float CalculateAvgAccuracy(List<InputController.TapInitInfo> tapInitInfoObjects)
        {
            return CalculateAvgAccuracyInRange(tapInitInfoObjects, 0, tapInitInfoObjects.Count - 1);
        }

        // VISUALISATION FUNCTIONS
        private void DrawCurMoleLevelValues()
        {
            if (_curMoleLevel.paddingLevel)
            {
                DrawPaddingLevelValues();
            }
            else if (_curMoleLevel.tapLevel)
            {
                DrawTapableLevelValues();
            }
        }

        private void DrawTapableLevelValues()
        {
            foreach (var levelSectionInfo in _playerData.moleLevelInfo[_playerData.moleLevelInfo.Count - 1].levelSectionInfos)
            {
                var tapableLevelSectionInfo = levelSectionInfo as TapableLevelSectionInfo;
                if (tapableLevelSectionInfo == null)
                {
                    Debug.LogFormat("Trying to draw tapable level values, but it's null!");
                    return;
                }
                
                var values = new List<string>
                {
                    _playerData.deviceID,
                    string.Format("{0}", tapableLevelSectionInfo.size), 
                    string.Format("{0}", tapableLevelSectionInfo.hitPercentage),
                    string.Format("{0};{1}", tapableLevelSectionInfo.avgAccuracy, tapableLevelSectionInfo.accSD), 
                    string.Format("{0}", tapableLevelSectionInfo.avgReactionTime),
                    string.Format("{0}", tapableLevelSectionInfo.avgTapDuration),
                    string.Format("{0}", tapableLevelSectionInfo.avgTapPressurePercentageOfMax)
                };

                if (_curMoleLevel.bossLevel)
                {
                    values.Add(string.Format("{0}", tapableLevelSectionInfo.avgHitsPerSecond));
                    values.Add(string.Format("{0}", tapableLevelSectionInfo.avgMissesPerSecond));
                }

                if (_curMoleLevel.sendFormConfig != null)
                {
                    _curMoleLevel.sendFormConfig.Send(this, values);
                }
                
                Debug.LogFormat("###################################################");
                Debug.LogFormat("Information for level {0}!", tapableLevelSectionInfo.level);
                Debug.LogFormat("Section tapable Size: {0}", tapableLevelSectionInfo.size);
                Debug.LogFormat("Section hit percentage: {0}", tapableLevelSectionInfo.hitPercentage);
                Debug.LogFormat("Section average accuracy: {0}, SD: {1}", tapableLevelSectionInfo.avgAccuracy, tapableLevelSectionInfo.accSD);
                Debug.LogFormat("Section average reaction time: {0}", tapableLevelSectionInfo.avgReactionTime);
                Debug.LogFormat("Section average tap duration: {0}", tapableLevelSectionInfo.avgTapDuration);
                Debug.LogFormat("Section average tap pressure: {0}", tapableLevelSectionInfo.avgTapPressurePercentageOfMax);
                Debug.LogFormat("Section average hits per second: {0}", tapableLevelSectionInfo.avgHitsPerSecond);
                Debug.LogFormat("Section average misses per second: {0}", tapableLevelSectionInfo.avgMissesPerSecond);
                Debug.LogFormat("###################################################");
            }
        }

        private void DrawPaddingLevelValues()
        {
            foreach (var levelSectionInfo in _playerData.moleLevelInfo[_playerData.moleLevelInfo.Count - 1].levelSectionInfos)
            {
                var paddingLevelSectionInfo = levelSectionInfo as PaddingLevelSectionInfo;
                if (paddingLevelSectionInfo == null)
                {
                    Debug.LogFormat("Trying to draw padding level values, but it's null!");
                    return;
                }
                
                var values = new List<string>
                {
                    _playerData.deviceID,
                    string.Format("{0}",paddingLevelSectionInfo.size), 
                    string.Format("{0}", paddingLevelSectionInfo.padding),
                    string.Format("{0}", paddingLevelSectionInfo.innerZonePercentage), 
                    string.Format("{0}", paddingLevelSectionInfo.outerZonePercentage),
                    string.Format("{0}", paddingLevelSectionInfo.accidentPercentage),
                    string.Format("{0}", paddingLevelSectionInfo.innerZoneMoleHitsPercentage),
                    string.Format("{0}", paddingLevelSectionInfo.outerZoneMoleHitsPercentage)
                };

                if (_curMoleLevel.sendFormConfig != null)
                {
                    _curMoleLevel.sendFormConfig.Send(this, values);
                }
                
                Debug.LogFormat("###################################################");
                Debug.LogFormat("Information for level {0}!", paddingLevelSectionInfo.level);
                Debug.LogFormat("Section tapable Size: {0}", paddingLevelSectionInfo.size);
                Debug.LogFormat("Section padding size: {0}", paddingLevelSectionInfo.padding);
                Debug.LogFormat("Section percentage touches inner zone: {0}", paddingLevelSectionInfo.innerZonePercentage);
                Debug.LogFormat("Section percentage hits inner zone: {0}", paddingLevelSectionInfo.innerZoneMoleHitsPercentage);
                Debug.LogFormat("Section percentage touches outer zone: {0}", paddingLevelSectionInfo.outerZonePercentage);
                Debug.LogFormat("Section percentage hits outer zone: {0}", paddingLevelSectionInfo.outerZoneMoleHitsPercentage);
                Debug.LogFormat("Section percentage touches that are accidents (maybe) : {0}", paddingLevelSectionInfo.accidentPercentage);
                Debug.LogFormat("###################################################");
            }
        }
        
        private void DrawAllHitPointsInCoordSys()
        {
            
        }

        private void DrawGraphOfDistances()
        {
            
        }
    }
}