using Common.Variables;
using UnityEngine;

namespace UI
{
    public class VolumeController : MonoBehaviour
    {
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _clip;
        [SerializeField] private BoolVariable _soundUnmuted;
        
        private void Start()
        {
            _audioSource.clip = _clip;
            OnVolumeChanged();
            _audioSource.Play();
        }

        public void OnVolumeChanged()
        {
            _audioSource.volume = _playerData.soundVolume;
            _soundUnmuted.value = _playerData.soundVolume > 0;
        }

        public void OnMuteUnmuteButtonPressed()
        {
            _audioSource.mute = !_audioSource.mute;
        }
    }
}