using System.Collections;
using System.Collections.Generic;
using Common.Variables;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    public class GameInfoDisplayer : MonoBehaviour
    {
        [SerializeField] private LevelStateVariable _levelState;
        [SerializeField] private LevelVariable _curLevel;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private FloatVariable _pixelsPerPoint;
        
        [FormerlySerializedAs("_gameInfoButtonRect")] [SerializeField] private RectTransform _levelInfoButtonRect;
        [SerializeField] private RectTransform _textButtonRect;
        [SerializeField] private RectTransform _textRect;
        
        [SerializeField] private Text _levelText;

        [SerializeField] private RectTransform _child;
          
        private float _screenWidth;
        private float _screenHeight;
        private bool _currentSideLeft;
        
        public void OnLevelStateChanged()
        {
            if (_levelState.value.showStartMenu)
            {
                _levelText.text = _curLevel.value.description[_playerData.language];
                // _levelInfoButtonRect.sizeDelta = new Vector2(_playerData.minTapableSize.value * _pixelsPerPoint.value, _playerData.minTapableSize.value * _pixelsPerPoint.value);              
                                   
                var w = Screen.width;
                var h = Screen.height;

                _screenHeight = _curLevel.value.portraitModus ? Mathf.Max(w, h) : Mathf.Min(w, h);
                _screenWidth = _curLevel.value.portraitModus ? Mathf.Min(w, h) : Mathf.Max(w, h);
                _currentSideLeft = true;
            
                PositionToCorrectDir(_playerData.handedness.value == -1);
            }               
        }

        public void OnDisplayInfo()
        {
            //_textRect.gameObject.SetActive(!_textRect.gameObject.activeSelf);
            _textButtonRect.gameObject.SetActive(!_textButtonRect.gameObject.activeSelf);
        }

        private void PositionToCorrectDir(bool left)
        {
            var posX = -_levelInfoButtonRect.anchoredPosition.x;
            var posY = _levelInfoButtonRect.anchoredPosition.y;
            var newPos = new Vector3(posX, posY, 0);

            if (!left && _currentSideLeft)
            {                
                _levelInfoButtonRect.anchorMin = Vector2.one;
                _levelInfoButtonRect.anchorMax = Vector2.one;
                _levelInfoButtonRect.pivot = Vector2.one;
                _levelInfoButtonRect.anchoredPosition = newPos;
                
                _textButtonRect.anchorMin = Vector2.zero;
                _textButtonRect.anchorMax = Vector2.zero;
                _textButtonRect.pivot = Vector2.one;
                SetInfoTextSize();
                SetChildren(true);
            }
            else if(left && !_currentSideLeft)
            {
                SetChildren(false);
                _levelInfoButtonRect.anchorMin = Vector2.up;
                _levelInfoButtonRect.anchorMax = Vector2.up;
                _levelInfoButtonRect.pivot = Vector2.up;
                _levelInfoButtonRect.anchoredPosition = newPos;
                
                _textButtonRect.anchorMin = Vector2.right;
                _textButtonRect.anchorMax = Vector2.right;
                _textButtonRect.pivot = Vector2.up;
                SetInfoTextSize();
            }
            
        }

        private void SetChildren(bool set)
        {
            _child.transform.parent = set ? transform : null;
            _child.anchorMin = Vector2.right;
            _child.anchorMax = Vector2.right;
            _child.pivot = Vector2.one;
        }

        private void SetInfoTextSize()
        {
            _textRect.GetComponent<LayoutElement>().preferredWidth = _screenWidth * 0.5f;
            _textRect.GetComponent<LayoutElement>().minHeight = _screenHeight * 0.5f;
        }
    }
}