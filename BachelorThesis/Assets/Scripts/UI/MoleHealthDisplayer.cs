using UnityEngine;
using System.Collections;
using Common.Variables;
using UnityEngine.UI;

public class MoleHealthDisplayer : MonoBehaviour
{
    [SerializeField] private IntVariable _maxHealth;
    [SerializeField] private Image _healthImage;
    
    private int _curHealth;

    private void OnEnable()
    {
        _curHealth = _maxHealth.value;
        _healthImage.fillAmount = 1;
    }
    
    public void OnDamageReceived()
    {
        _curHealth--;
        var amount = _curHealth / (float) _maxHealth.value;
        _healthImage.fillAmount = amount;
        _healthImage.color = amount <= 0.2f ? Color.red : amount <= 0.5f ? Color.yellow : Color.green;
        
        if (_curHealth == 0)
        {
            _curHealth = _maxHealth.value;
            _healthImage.color = Color.green;
        }
    }
}