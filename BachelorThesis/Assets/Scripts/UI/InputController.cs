using System;
using System.Collections.Generic;
using Common.Configs;
using Common.Enum;
using Common.Events;
using Common.Variables;
using Environment;
using Environment.FlowerbedGame.Controller;
using Environment.MoleGame.Mole;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] protected GameEvent _calculateScoreGameEvent;
        [SerializeField] protected InputControllerConfig _config;
        [SerializeField] protected TouchablesInfoListGameEvent _touchablesInfoListGameEvent;
        [SerializeField] protected Button _tapVisualizer;
        [SerializeField] protected Button _nearestTargetVisualizer;
        [SerializeField] protected Camera _mainCam;
        
        [SerializeField] protected bool _debugTapDistances;
        [SerializeField] protected bool _debugTapPos;
        [SerializeField] protected GameController _gameController;
        [SerializeField] protected Vector3ListVariable _accelerometerInput;
        
        private readonly List<TouchablesInfoListGameEvent.TouchableInfo> _touchablesInfo = new List<TouchablesInfoListGameEvent.TouchableInfo>();
        
        [Serializable]
        public class AccuracyObject
        {
            public float accuracyPercent;
            public string accuracyString;
        }

        // info for ONE touch
        // only for touch initiation, not moving. same for apple and mole game  
        [Serializable]
        public class TapInitInfo
        {
            public Vector3 tapPos;
            public bool hitTarget;
            public bool targetRedYellow;
            public bool hitResistent;
            public float tapTime;
            public float reactionTime;
            public float tapDuration;
            public List<float> tapPressure = new List<float>();
            public AccuracyObject accuracyObject;            
            public Vector3 possibleTargetPos; 
        }

        // info for one point on the pan path
        [Serializable]
        public class PanPointInfo
        {
            public Vector3 pos;
            public bool hitObj;
        }
        
        // info for sequence of touches
        [Serializable]
        public class PanInfo
        { 
            // the distance and normalized distance, and the total duration when input ends  
            public TapInitInfo tapInit;
            public readonly List<PanPointInfo> pointsOnPath = new List<PanPointInfo>();
            public float distance;
            public float normalizedDistance;
            public float duration;
            public bool endedSuccessfully;
        }

        protected void Start()
        {
            _accelerometerInput.value.Clear();
        }

        protected void AddValueToAccelerometerList()
        {
            _accelerometerInput.value.Add(Input.acceleration);
        }
        protected void VisualizeTap(Vector3 touchPos)
        {
            if(_debugTapPos) _tapVisualizer.transform.position = touchPos;
        }
        
        protected void VisualizeDistance(Vector3 pos1, Vector3 pos2, Color color, float duration)
        {
            Debug.DrawLine(pos1, pos2, color, duration);
        }
        
                
        protected void DrawRay(Ray ray, float distance, Color color, float seconds)
        {
            Debug.DrawRay(ray.origin, ray.direction * distance, color, seconds);
        }

        private AccuracyObject CreateAccuracyObject(Touchable touchableObj, Vector3 pos)
        {
            var accuracyFactor = CalculateAccuracyFactor(pos, touchableObj.GetGlobalColliderCenterPosition(), touchableObj.GetWidthAndHeightOfCollider());
            var accuracyPercent = AccuracyFactorToAccuracy(accuracyFactor);
            var accuracyString = AccuracyToString(accuracyPercent);
            return new AccuracyObject{accuracyPercent = accuracyPercent, accuracyString = accuracyString};
        }

        private float CalculateAccuracyFactor(Vector3 touchPos, Vector3 center, Vector2 widthAndHeight)
        {
//            var maxDistX = widthAndHeight.x * 0.5f; // stores x value
//            var maxDistZ = widthAndHeight.y * 0.5f; // stores z value
//            
//            Debug.Log("MaxDistX: " + maxDistX);
//            Debug.Log("MaxDistZ: " + maxDistZ);
//
//            var touchPosInUnits = GetComponent<GameController>().VectorInPixelsToUnits(touchPos);
//
//            Debug.Log("TouchPosInUnitsX: " + touchPosInUnits.x);
//            Debug.Log("TouchPosInUnitsZ: " + touchPosInUnits.z);
//            
//            Debug.Log("centerPosX: " + center.x);
//            Debug.Log("centerPosZ: " + center.z);
//            
//            var touchDistX = Math.Abs(touchPosInUnits.x - center.x);
//            var touchDistZ = Math.Abs(touchPosInUnits.z - center.z);
//            
//            Debug.Log("TouchDistX: " + touchDistX);
//            Debug.Log("TouchDistZ: " + touchDistZ);
//
//            var distFactor = touchDistX / maxDistX;
//            var distFactor2 = touchDistZ / maxDistZ;
//            
//            Debug.Log("DistFactor: " + distFactor);
//            Debug.Log("DistFactor2: " + distFactor2);	
//
//            distFactor = distFactor >= distFactor2 ? distFactor : distFactor2;
//            
//            Debug.Log("BiggerFactor: " + distFactor);
            
            
            //andersrum 
            var maxDistX = _gameController.UnitsToPixels(widthAndHeight.x * 0.5f); // stores x value
            var maxDistY = _gameController.UnitsToPixels(widthAndHeight.y * 0.5f); // stores z value
            
            Debug.Log("MaxDistX: " + maxDistX);
            Debug.Log("maxDistY: " + maxDistY);

            Debug.Log("TouchPosX: " + touchPos.x);
            Debug.Log("TouchPosY: " + touchPos.y);

            center = _gameController.VectorInUnitsToPixels(center);
            
            Debug.Log("centerPosX: " + center.x);
            Debug.Log("centerPosY: " + center.y);
            
            var touchDistX = Math.Abs(touchPos.x - center.x);
            var touchDistY = Math.Abs(touchPos.y - center.y);
            
            Debug.Log("TouchDistX: " + touchDistX);
            Debug.Log("TouchDistY: " + touchDistY);

            var distFactor = touchDistX / maxDistX;
            var distFactor2 = touchDistY / maxDistY;
            
            Debug.Log("DistFactor: " + distFactor);
            Debug.Log("DistFactor2: " + distFactor2);	

            distFactor = distFactor >= distFactor2 ? distFactor : distFactor2;
            
            Debug.Log("BiggerFactor: " + distFactor);

            return distFactor;
        }

        protected string AccuracyToString(float percent)
        {
            if (percent >= _config.perfectAccuracy.percentageFrom)
            {
                return _config.perfectAccuracy.stringName;
            }
            if (percent >= _config.veryGoodAccuracy.percentageFrom)
            {
                return _config.veryGoodAccuracy.stringName;
            }
            if (percent >= _config.goodAccuracy.percentageFrom)
            {
                return _config.goodAccuracy.stringName;
            }
            if (percent >= _config.okAccuracy.percentageFrom)
            {
                return _config.okAccuracy.stringName;
            }
            if (percent >= _config.badAccuracy.percentageFrom)
            {
                return _config.badAccuracy.stringName;
            }
            
            return _config.undefinedAccuracy.stringName;		
        }

        private float AccuracyFactorToAccuracy(float factor)
        {
            var percent = (1f - factor) * 100f;
            Debug.Log("Percent: " + percent);
            return percent;
        }
        
        protected TapInitInfo CreateTapInitObject(Touchable touchableObject, Vector3 pos)
        {
            var tapInfo = new TapInitInfo {tapPos = pos, tapTime = Time.time};

            if (touchableObject != null)
            {              
                tapInfo.hitTarget = true;
                var mole = touchableObject as MoleController;
                if (mole != null)
                {
                    tapInfo.hitTarget = !mole.touchResistent;
                    tapInfo.hitResistent = mole.touchResistent;
                    tapInfo.reactionTime = tapInfo.tapTime - mole.appearedTime;
                }
                var apple = touchableObject as AppleController;
                if (apple != null)
                {
                    tapInfo.targetRedYellow = apple.IsRedYellow();
                }
                tapInfo.accuracyObject = CreateAccuracyObject(touchableObject, pos);
            }
            else
            {
                tapInfo.hitTarget = false;
                var accuracyPercent = 0f;
                var accuracyString = AccuracyToString(accuracyPercent);
                tapInfo.accuracyObject = new AccuracyObject{accuracyPercent = accuracyPercent, accuracyString = accuracyString};    
                Debug.LogFormat("Accuracy is: {0}", accuracyPercent);
            }

            return tapInfo;
        }

//        protected static float CalculateAvgAccuracy(List<TapInitInfo> tapInitInfoObjects)
//        {
//            var acc = 0f;
//            
//            foreach (var info in tapInitInfoObjects)
//            {
//                acc += info.accuracyObject.accuracyPercent;
//            }
//            return acc/tapInitInfoObjects.Count;
//        }
        
        protected void ResetAndRequestTouchableInfo()
        {
            _touchablesInfo.Clear();
            _touchablesInfoListGameEvent.parameter = _touchablesInfo;
            _touchablesInfoListGameEvent.Raise(); 
        }
        
        protected Vector3 SaveAndVisualizeDistances(Vector3 tapPos)
        {
            var gameController = GetComponent<GameController>();

            var tapPosInUnits = gameController.VectorInPixelsToUnits(tapPos);

            var smallestDist = float.MaxValue;
            var nearestPos = Vector3.zero;
            
            foreach(var info in _touchablesInfo)
            {                
                tapPosInUnits.y = info.colliderPosUnits.y;

                var dist = (tapPosInUnits - info.colliderPosUnits).magnitude;
                
                nearestPos = dist < smallestDist ? gameController.VectorInUnitsToPixels(info.colliderPosUnits) : nearestPos;
                smallestDist = dist < smallestDist ? dist : smallestDist; 
                
                if(_debugTapDistances) VisualizeDistance(tapPosInUnits, info.colliderPosUnits, Color.white, 3f);	
            }

            return nearestPos;
        }
        
        protected void VisualizeNearestPossibleTarget(Vector3 possibleTargetsPosition)
        {
            _nearestTargetVisualizer.transform.position = possibleTargetsPosition;
            //Debug.Log("Visualizer center position: " + _nearestTargetVisualizer.transform.position);
        }     
        
        protected float CalculatePanDistance(List<PanInfo> panInfoObjects)
        {
            var distance = 0f;
            if (panInfoObjects.Count > 0 && panInfoObjects[panInfoObjects.Count - 1].pointsOnPath.Count > 0)
            {
                distance = (panInfoObjects[panInfoObjects.Count - 1].pointsOnPath[0].pos - panInfoObjects[panInfoObjects.Count - 1].tapInit.tapPos).magnitude;
                for (var i = 0; i < panInfoObjects[panInfoObjects.Count - 1].pointsOnPath.Count - 1; i++)
                {
                    distance += (panInfoObjects[panInfoObjects.Count - 1].pointsOnPath[i + 1].pos - panInfoObjects[panInfoObjects.Count - 1].pointsOnPath[i].pos).magnitude;               
                }    
            }
           
            return distance;
        }
        
        protected float CalculatePanDuration(List<PanInfo> panInfoObjects)
        {
            return Time.time - panInfoObjects[panInfoObjects.Count - 1].tapInit.tapTime;
        }
    }
}