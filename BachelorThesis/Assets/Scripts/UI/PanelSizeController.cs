﻿using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class PanelSizeController : MonoBehaviour 
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private RectTransform _textRect;
        [SerializeField] private int _offset;

        private void Start()
        {
            var textSize = _textRect.sizeDelta;       
            var size = _rectTransform.sizeDelta;
            Debug.Log("text size x: " + textSize.x);
            Debug.Log("rect size x: " + size.x);
            size.x = size.x < textSize.x + _offset ? textSize.x + _offset : size.x;
            size.y = size.y < textSize.y + _offset ? textSize.y + _offset : size.y;
            _rectTransform.sizeDelta = size;
        }
    }
}
