using System.Collections;
using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class CustomWidthController : MonoBehaviour
    {
        [SerializeField] private RectTransform _rectTransform;
        [SerializeField] private float _factorOfScreenWidth;
        [SerializeField] private Vector2Variable _resolution;
        
        private void Start()
        {
            SetSizes();
            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
        }

        private void SetSizes()
        {   
            var layoutElement = _rectTransform.GetComponent<LayoutElement>();
            
            var size = _factorOfScreenWidth * _resolution.value.x;
            
            if (layoutElement == null)
            {
                var sizeDelta = _rectTransform.sizeDelta;
                sizeDelta.x = sizeDelta.x < size ? size : sizeDelta.x;
                _rectTransform.sizeDelta = sizeDelta;
                return;
            }
            
            if (size > layoutElement.minWidth)
            {
                layoutElement.minWidth = size;
            }
        }   
    }
}