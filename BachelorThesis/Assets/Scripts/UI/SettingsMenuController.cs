using Common.Events;
using Environment.Manager;
using UnityEngine;

namespace UI
{
    public class SettingsMenuController : MonoBehaviour
    {
        [SerializeField] private GameEvent _needToResetGameDataGameEvent;
        [SerializeField] private GameEvent _sendDataManuallyGameEvent;
        
        public void OnResetDataButtonPressed()
        {
            if (_needToResetGameDataGameEvent != null)
            {
                _needToResetGameDataGameEvent.Raise();
            }
        }

        public void OnSendDataButtonPressed()
        {
            if (_sendDataManuallyGameEvent != null)
            {
                _sendDataManuallyGameEvent.Raise();
            }
        }
    }
}