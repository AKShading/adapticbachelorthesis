﻿using Common.Variables;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GUIScaler : MonoBehaviour 
    {
        [SerializeField] private CanvasScaler _canvasScaler;
        [SerializeField] private FloatVariable _pixelsPerPoint;
        
        private void OnEnable()
        {
            Debug.Assert(_canvasScaler != null, "_canvasScaler != null");
            _canvasScaler.scaleFactor = _pixelsPerPoint.value;
        }
    }
}
