using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class VerticalLayoutGroupSpacingController : MonoBehaviour
    {
        [SerializeField] private VerticalLayoutGroup _verticalLayoutGroup;
        [SerializeField] private FloatVariable _pixelsPerPoint;
        [SerializeField] private FloatVariable _minPadding;
        
        private void OnEnable()
        {
            _verticalLayoutGroup.spacing = Mathf.Max(_minPadding.value * _pixelsPerPoint.value, _verticalLayoutGroup.spacing);
            LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
        }
    }
}