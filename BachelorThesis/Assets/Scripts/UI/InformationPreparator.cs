using System;
using System.Collections.Generic;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class InformationPreparator : MonoBehaviour
    {
        [SerializeField] protected FloatVariable _minTapableSize;
        [SerializeField] protected FloatVariable _suggestedMinSize;
        [SerializeField] protected FloatVariable _pixelsPerPoint;
        [SerializeField] protected PlayerData _playerData;
        [SerializeField] protected LevelVariable _curLevel;
        [SerializeField] protected LevelStateVariable _levelState;
        [SerializeField] protected Vector3ListVariable _accelerometerInput;
        [SerializeField] protected GameEvent _calculatedGameOverValuesGameEvent;
        [SerializeField] protected GameEvent _changedDataNeedToBeSavedPersistentGameEvent;
        
        
        [Serializable]
        public class LevelSectionInfo
        {
            public int level;
            public int section;
            public float size; // tapable size or padding is stored here
        }
        
        [Serializable]
        public class TapableLevelSectionInfo : LevelSectionInfo
        {
            public float hitPercentage;
            //following are average, lets save the best and worst too 
            public float avgAccuracy;
            public float accSD;
            public float avgReactionTime;
            public float avgHitsPerSecond;
            public float avgMissesPerSecond;
            public float avgTapDuration;
            public float avgTapPressurePercentageOfMax;
        }
        
        [Serializable]
        public class PaddingLevelSectionInfo : LevelSectionInfo
        {
            public float padding;
            public float innerZonePercentage;
            public float innerZoneMoleHitsPercentage;
            public float outerZonePercentage;
            public float outerZoneMoleHitsPercentage;
            public float accidentPercentage;
        }
        
        [Serializable]
        public class PanTapLevelSectionInfo : LevelSectionInfo
        {
            public float avgPointsPerSecond; // speed
            public float avgTrembleRate;
            public float trembleSD;
            public int amountOfGreens;
            public float greenCorrectPercentage;
            public int amountOfReds;
            public float redCorrectPercentage;
            public float avgHitSthPercentage;
            public float avgAccuracy;
            public float accSD;
            public float avgPanAnObjectPercentage;
            public float avgPanSuccessfullyLandedInBucket;
            public List<string> sideAndColorList;
        }

        [Serializable]
        public class CheckHandednessLevelSectionInfo : LevelSectionInfo
        {
            public float usedRightHandPercentage;
            public float usedLeftHandPercentage;
            public float percentageStartedPanFromCorrectPos;
            public float percentageOfSuccessfullyPanned;
            public string panPattern;
        }
        
        [Serializable]
        public class CheckHandUsedAndDistancesLevelSectionInfo : LevelSectionInfo
        {
            public string handUsed;
            public float distanceToEdgeOfScreen;
            public float percentageStartedPanFromCorrectPos;
            public float percentageOfSuccessfullyPanned;
            public string panPattern;
        }

        [Serializable]
        public class LevelInfo
        {
            public List<LevelSectionInfo> levelSectionInfos = new List<LevelSectionInfo>();
        }
        
        protected static float CalculateAvgAccuracyInRange(List<InputController.TapInitInfo> tapInitInfoObjects, int start, int end)
        {
            var acc = 0f;
            var hits = 0;
            
            for (var i = start; i <= end; i++)
            {
                if (tapInitInfoObjects[i].hitTarget)
                {
                    hits++;
                    acc += tapInitInfoObjects[i].accuracyObject.accuracyPercent;
                }
            }

            return acc / hits;            
        }
        
        protected static float CalculateAccuracySDInRange(float avg, List<InputController.TapInitInfo> tapInitInfoObjects, int start, int end)
        {
            var temp = 0f;
            var hits = 0;
            
            for (var i = start; i <= end; i++)
            {
                if (tapInitInfoObjects[i].hitTarget)
                {
                    hits++;
                    temp += Mathf.Pow((tapInitInfoObjects[i].accuracyObject.accuracyPercent - avg), 2);
                }
            }

            return Mathf.Sqrt(temp / hits);            
        }
        
        protected static float CalculateAccuracySD(float avg, List<InputController.TapInitInfo> tapInitInfoObjects)
        {
            return CalculateAccuracySDInRange(avg, tapInitInfoObjects, 0, tapInitInfoObjects.Count - 1);;            
        }
        
        protected static float CalculateTrembleSD(float avg, List<float> list)
        {
            var temp = 0f;

            foreach (var element in list)
            {
                temp += Mathf.Pow(element - avg, 2);
            }

            return Mathf.Sqrt(temp / list.Count);            
        }

        protected void CheckIfDeviceIsHeldInHand()
        {
            if (_accelerometerInput.value.Count == 0)
            {
                return;
            }

            var temp = Vector3.zero;
            
            foreach (var val in _accelerometerInput.value)
            {
                temp += val;
            }

            temp = temp / _accelerometerInput.value.Count;

            var isLayingDown = (Mathf.Abs(temp.y) < 0.1) && (Mathf.Abs(temp.z) > 0.9);
            _playerData.heldInHand.value.Add(!isLayingDown);
        }
        
        protected static float CalculateAvgAccuracy(List<InputController.TapInitInfo> tapInitInfoObjects)
        {
            return CalculateAvgAccuracyInRange(tapInitInfoObjects, 0, tapInitInfoObjects.Count - 1);
        }

        protected void ChangedDataNeedToBeSaved()
        {
            if (_changedDataNeedToBeSavedPersistentGameEvent != null)
            {
                _changedDataNeedToBeSavedPersistentGameEvent.Raise();
            }
        }
    }
}