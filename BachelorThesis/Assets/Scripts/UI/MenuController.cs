﻿using System.Collections.Generic;
using Common.Variables;
using Environment;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace UI
{
    public class MenuController : MonoBehaviour 
    {
        private const string GAME_OVER = "GameOver";
        private const string STARTED = "Started";
        private const string READY = "Ready";

        [SerializeField] private LevelStateVariable _levelState;
        [SerializeField] private LevelVariable _curLevel;
        [SerializeField] private Animator _animator;
        [SerializeField] private TextMeshProUGUI _levelText;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private List<string> _additionalText;
        [SerializeField] private List<string> _secondAdditionalText;
        [SerializeField] private IntVariable _score;
        [SerializeField] private AnimatorSpeedController _animatorSpeedController;
        [SerializeField] private PlayerData _playerData;
        
        private bool _wasPaused;

        private void OnEnable()
        {
            Debug.Assert(_levelText != null, "_levelText != null");
            Debug.Assert(_scoreText != null, "_accText != null");

            if (_playerData.tapDuration > 1 && _animatorSpeedController != null)
            {
                _animatorSpeedController.SetAnimationTime(_playerData.tapDuration);
            }
            
            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }
        }

        public void OnLevelStateChanged()
        {
            if (_levelState.value.showStartMenu)
            {
                var text = _curLevel.value.description[_playerData.language];
                if (text != "")
                {
                    _levelText.text = text;
                }

                _animator.SetTrigger(READY);
            }

            if (_levelState.value.running)
            {
                if (!_wasPaused)
                {
                    _animator.SetTrigger(STARTED);
                }
            }

            if (_levelState.value.showPauseMenu)
            {
                _wasPaused = true;
            }

            if (_levelState.value.showGameOverMenu)
            {
                var totalScore = _playerData.levelPointsReached.AddAllElements();
                _levelText.text = string.Format("{0}", _curLevel.value.levelEnded[_playerData.language]);
                _scoreText.text = string.Format("{0} {1} \n {2} {3}", _additionalText[_playerData.language], _score.value, _secondAdditionalText[_playerData.language], totalScore);
                _animator.SetTrigger(GAME_OVER);
            }
        }
    }
}
