using Common.Events;
using Common.Variables;
using UnityEngine;

namespace UI
{
    public class HUDController : MonoBehaviour
    {
        private static readonly int Visible = Animator.StringToHash("Visible");
        
        [SerializeField] private LevelStateVariable _levelState;
        [SerializeField] private Animator _animator;
        [SerializeField] private IntListVariable _infoButtonPressedCount;
        
        private int _infoButtonPressed;
        
        public void OnLevelStateChanged()
        {
            if (_levelState.value.running)
            {
                _animator.SetBool(Visible, true);        
            }

            if (_levelState.value.showGameOverMenu)
            {
                _animator.SetBool(Visible, false);    
            }
        }

        public void OnInfoButtonPressed()
        {
            _infoButtonPressed++;
        }

        public void OnGameEnded()
        {
            _infoButtonPressedCount.value.Add((int)(_infoButtonPressed * 0.5f));
        }     
    }
}