﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Environment.WaterGame.Controller;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    [SerializeField] private float _smoothTime = 0.3f;
    [SerializeField] private Vector3 _velocity;
    
    
    [SerializeField] private Throwable _target;
    [SerializeField] private Vector3 _extraOffset;
    
    private Vector3 _acceleration;
    private Vector3 _offsetToTarget;
    private Vector3 _initialPos;

    private float _tempSmooth; 
    
    private void OnEnable()
    {
        _initialPos = transform.position;
        _offsetToTarget = _target.transform.position - _initialPos;    
    }

    // Update is called once per frame
    private void FixedUpdate ()
    {       
        if (_target.transform.position.z - _offsetToTarget.z -1 > transform.position.z)
        {
            
            var targetPos = _target.transform.position - _offsetToTarget - _extraOffset;
            targetPos.y = _initialPos.y;
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref _velocity, _tempSmooth);
            _tempSmooth = Mathf.Lerp(_tempSmooth, 0f, Time.deltaTime);
            /*var pos = _target.transform.position - _offsetToTarget - _extraOffset;
            pos.y = transform.position.y;
            transform.position = pos;*/
        }
        else
        {
            _tempSmooth = _smoothTime;
            transform.position = _initialPos;
            //_offsetToTarget = _target.transform.position - _initialPos;
            _velocity = Vector3.zero;
        }    
    }

    public void OnRespawned()
    {
        _offsetToTarget = _target.transform.position - _initialPos;
    }
}
