using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Environment.Manager
{
    public class DataManager : MonoBehaviour
    {
        [SerializeField] private string _fileNamePattern = "/{0}_{1}.pso";
        [SerializeField] private string _baseFileNameOnDevice;
        [SerializeField] private string _baseOLDFileNameOnDevice;
        
        [SerializeField] private List<ScriptableObject> _scriptableObjectsToSave;

        private void OnEnable()
        {
            if (CheckAndDeleteOldData())
            {
                SaveData();
            }
            else
            {
                LoadData();   
            }
        }

        // check when we need to save values, change this fct call
        /*private void OnDisable()
        {
            SaveData();
        }*/

        private bool CheckAndDeleteOldData()
        {
            var foundData = false;
            for (var i = 0; i < _scriptableObjectsToSave.Count; i++)
            {
                var filename = Application.persistentDataPath + string.Format(_fileNamePattern, _baseOLDFileNameOnDevice, i);
                if (File.Exists(filename))
                {
                    foundData = true;
                    File.Delete(filename);
                    Debug.LogFormat("[Persistence Save] file deleted: {0}", filename);
                }
            }
            return foundData;
        }

        private void LoadData()
        {
            for (var i = 0; i < _scriptableObjectsToSave.Count; i++)
            {
                var filename = Application.persistentDataPath + string.Format(_fileNamePattern, _baseFileNameOnDevice, i);
                if (!File.Exists(filename))
                {
                    Debug.LogFormat("[Persistence Load] file not found: {0}", filename);

                    continue;
                }

                var bf = new BinaryFormatter();
                var file = File.Open(filename, FileMode.Open);
                JsonUtility.FromJsonOverwrite((string) bf.Deserialize(file), _scriptableObjectsToSave[i]);
                file.Close();

                Debug.LogFormat("[Persistence Load] file loaded: {0}", filename);
            }
        }

        public void SaveData()
        {
            for (var i = 0; i < _scriptableObjectsToSave.Count; i++)
            {
                var filename = Application.persistentDataPath + string.Format(_fileNamePattern, _baseFileNameOnDevice, i);
                var bf = new BinaryFormatter();
                var file = File.Create(filename);
                var json = JsonUtility.ToJson(_scriptableObjectsToSave[i]);
                //Debug.LogFormat("{0}", json);
                bf.Serialize(file, json);
                file.Close();

                Debug.LogFormat("[Persistence Save] file saved: {0}", filename);
            }
        }      
    }
}