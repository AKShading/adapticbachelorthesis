using System.Collections.Generic;
using System.Runtime.Serialization;
using Common.Configs;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Environment.Manager
{
    public class GameManager : MonoBehaviour
    {
        private const string MAIN_MENU = "MainGameScene";
        private const string MOLE_GAME = "MoleGameScene";
        private const string FLOWER_GAME = "FlowerbedGameScene";
        private const string WATER_GAME = "WaterGameScene";
        
        [SerializeField] private GameManagerConfig _config;
        [SerializeField] private LevelStateVariable _levelState;
        [SerializeField] private GameEvent _firstStartGameEvent;
        [SerializeField] private GameEvent _startedGameEvent;
        [SerializeField] private GameEvent _gameFinishedGameEvent;
        [SerializeField] private GameEvent _changedDataNeedToBeSavedGameEvent;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private List<SendFormConfig> _sendFormConfigs;

        private bool _sendPlayerData; 
        
        private void Start()
        {
            Debug.Assert(_config != null, "_config != null");
           
            if (_config.firstStart)
            {
                if (_firstStartGameEvent != null)
                {
                    _firstStartGameEvent.Raise();
                    Debug.Log("FirstStartGameEvent raised");
                }
                _config.nextScene = 1;
            }
            else
            {
                if (_startedGameEvent != null)
                {
                    _startedGameEvent.Raise();
                    Debug.Log("StartedGameEvent raised");
                }
            }

            if (_config.finished)
            {
                if (_gameFinishedGameEvent != null)
                {
                    _gameFinishedGameEvent.Raise();
                }
            }

            CheckIfSendingDataFailed();
        }

        public void OnMainStartButtonPressed()
        {
            _config.debug = false;
            if (!_config.finished)
            {
                LoadSceneByIndex(_config.nextScene);    
            }
        }

        public void OnEnteredFirstStartData()
        {
            _config.firstStart = false;
            if (_changedDataNeedToBeSavedGameEvent != null)
            {
                _changedDataNeedToBeSavedGameEvent.Raise();
            }
        }

        public void OnHomeButtonPressed()
        {
            if (!_config.debug)
            {
                LoadSceneByIndex(0);
            }
            else
            {
                OnCancelGame();
            }
        }

        public void OnCancelGame()
        {
            _config.debug = false;
            LoadSceneByIndex(0);
        }
         
        public void OnNextButtonPressed()
        {
            if (!_config.debug)
            {                          
                LoadSceneByIndex(_config.nextScene);
            }
            else
            {
                ReloadScene();
            }
        }
        
        private void CheckWhichSceneIsNext()
        {
            var curSceneIndex = SceneManager.GetActiveScene().buildIndex;
            
            switch (curSceneIndex)
            {
                case 0: break;
                case 1: case 2:
                    _config.nextScene = _config.curLevel.value.lastLevel ? curSceneIndex + 1 : curSceneIndex;
                    break;
                case 3:
                    if (_config.curLevel.value.lastLevel)
                    {
                        _sendPlayerData = true;
                        _config.nextScene = 0;
                        _config.finished = true;
                    }
                    else
                    {
                        _config.nextScene = curSceneIndex;
                    }
                    break;
                default:
                    Debug.LogFormat("Scene {0} is not defined, please add to build settings", curSceneIndex);
                    break;
            }                      
        }

        public void LoadSceneByIndex(int index)
        {
            SceneManager.LoadScene(index);
        }
        
        public void LoadMoleGame()
        {
            _config.debug = true;
            LoadSceneByIndex(1);
            SceneManager.LoadScene(MOLE_GAME);
        }

        public void LoadFlowerGame()
        {
            _config.debug = true;
            LoadSceneByIndex(2);
            SceneManager.LoadScene(FLOWER_GAME);
        }

        public void LoadWaterGame()
        {
            _config.debug = true;
            LoadSceneByIndex(3);
            SceneManager.LoadScene(WATER_GAME);
        }
        
        public void ReloadScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void OnLevelStateChanged()
        {           
            if (_levelState.value.canceled)
            {
                OnCancelGame();
            }

            if (_levelState.value.showGameOverMenu)
            {
                if (_sendPlayerData)
                {
                    SendPlayerDataForm();
                    _sendPlayerData = false;
                }
            }
        }

        public void OnGameEnded()
        {
            CheckWhichSceneIsNext();
        }

        private void SendPlayerDataForm()
        {
            _playerData.SendData(this);
        }

        private void CheckIfSendingDataFailed()
        {
            foreach (var sendForm in _sendFormConfigs)
            {
                foreach (var dataInfo in sendForm.values)
                {
                    if (dataInfo.sendingFailed)
                    {
                        sendForm.TryAgain(this, dataInfo);
                    }
                }
            }
        }

        public void SendDataManually()
        {
            foreach (var sendForm in _sendFormConfigs)
            {
                if (sendForm.values.Count > 0)
                {
                    sendForm.TryManuallyAgain(this);
                }
            }
        }
    }
}