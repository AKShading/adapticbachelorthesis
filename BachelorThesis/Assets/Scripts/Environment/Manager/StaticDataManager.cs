using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Common.Events;
using UnityEngine;
using UnityEngine.Events;

namespace Environment.Manager
{
    public class StaticDataManager : MonoBehaviour
    {
        private static StaticDataManager _instance;
        
        [SerializeField] private string _fileNamePattern = "/{0}_{1}.pso";
        [SerializeField] private string _baseFileNameOnDeviceStatic;           
        [SerializeField] private List<ScriptableObject> _scriptableObjectsToSaveStatic;

        [SerializeField] private GameEvent _gameResetGameEvent;
        
        private bool _first = true;
        
        private void OnEnable()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            _instance = this;
            
            Debug.LogFormat("StaticDataManager first call of enable: {0}.", _first);
            if (!_first)
            {
                return;
            }
            
            SaveStaticData();
            _first = false;
        }

        public void ResetData()
        {
            for (var i = 0; i < _scriptableObjectsToSaveStatic.Count; i++)
            {
                var filename = Application.persistentDataPath + string.Format(_fileNamePattern, _baseFileNameOnDeviceStatic, i);
                if (!File.Exists(filename))
                {
                    Debug.LogFormat("[Persistence Load] file not found: {0}", filename);

                    continue;
                }

                var bf = new BinaryFormatter();
                var file = File.Open(filename, FileMode.Open);
                JsonUtility.FromJsonOverwrite((string) bf.Deserialize(file), _scriptableObjectsToSaveStatic[i]);
                file.Close();

                Debug.LogFormat("[Persistence Load] file loaded: {0}", filename);
            }
            _gameResetGameEvent.Raise();
        }

        
        // only called ONCE -> when starting the game
        public void SaveStaticData()
        {
            for (var i = 0; i < _scriptableObjectsToSaveStatic.Count; i++)
            {
                var filename = Application.persistentDataPath + string.Format(_fileNamePattern, _baseFileNameOnDeviceStatic, i);
                var bf = new BinaryFormatter();
                var file = File.Create(filename);
                var json = JsonUtility.ToJson(_scriptableObjectsToSaveStatic[i]);
                //Debug.LogFormat("{0}", json);
                bf.Serialize(file, json);
                file.Close();

                Debug.LogFormat("[Persistence Save] file saved: {0}", filename);
            }
        }      
    }
}