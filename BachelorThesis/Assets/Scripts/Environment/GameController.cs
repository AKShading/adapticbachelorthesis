﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using Common.Configs;
using Common.Events;
using Common.Variables;
using Environment.MoleGame.Mole;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Environment
{
    public class GameController : MonoBehaviour 
    {
        private const string GAME_OVER = "GameOver";
        private const string START = "Start";
        private const string CALCULATE_GAME_OVER_VALUES = "CalculateGameOverValues";
        private const string READY = "Ready";
        private const string PAUSE = "Pause";
        private const string CANCELED = "Canceled";

        public GameControllerConfig config;

        [SerializeField] private GameEvent _changedDataNeedToBeSavedPersistentGameEvent;
        [SerializeField] private GameEvent _dontNeedCountdownGameEvent;
        [SerializeField] private GameEvent _gameEndedGameEvent;
        [SerializeField] protected MeshRenderer _flowerbedReference;
        [SerializeField] protected Camera _camera;        
       
        [SerializeField] protected Text _gameTimer;
        [SerializeField] protected Animator _animator;
        [SerializeField] protected float _heightAboveFloorInUnits;
        [SerializeField] protected Vector2Variable _resolution;
        
        //protected float _tapAreaNeededSizeInPixels; no need for this anymore, check other classes for errors
        protected float _neededPaddingInPixels;
        protected float _objectsScaler;
            
        protected int _screenWidth;
        protected int _screenHeight;
        protected float _screenWidthUnits;
        private float _screenHeightUnits;
        protected List<Touchable> _activeTouchables;
        protected float _flowerbedToZeroOffsetXInUnits;

        private float _unitsToPixelsOffsetXInUnits;
        protected bool _wasPaused;

        protected int _time;
        
        protected virtual void Start()
        {
            _activeTouchables = new List<Touchable>();
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            var w = Screen.width;
            var h = Screen.height;

            _screenWidth = Mathf.Max(w, h);
            _screenHeight = Mathf.Min(w, h);

            _resolution.value.x = _screenWidth;
            _resolution.value.y = _screenHeight;
                    
            _screenHeightUnits = _camera.orthographicSize * 2;
            _screenWidthUnits = _screenHeightUnits * Screen.width / Screen.height;

            ScaleFlowerbedIfTooBig();
            _unitsToPixelsOffsetXInUnits = Mathf.Abs(_flowerbedReference.transform.position.x - (_screenWidthUnits * 0.5f));
            
            Debug.Log("height in pixels: " + _screenHeight);
            Debug.Log("width in pixels: " + _screenWidth);
            
            Debug.Log("height in units: " + _screenHeightUnits);
            Debug.Log("width in units: " + _screenWidthUnits);      
        }

        private void ScaleFlowerbedIfTooBig()
        {
            var scale = _screenWidthUnits / _flowerbedReference.bounds.size.x;

            if (scale >= 0.98f)
            {
                return;
            }
            var localScale = _flowerbedReference.transform.localScale;
            localScale.x *= scale;
            _flowerbedReference.transform.localScale = localScale;
            
            _flowerbedToZeroOffsetXInUnits = (_flowerbedReference.transform.position.x - _flowerbedReference.bounds.extents.x);
            Debug.Log("Offset from flowerbed to zero: " + _flowerbedToZeroOffsetXInUnits);          
        }
        
        public float PixelsToUnits(float pix)
        {
            return ((int) pix) * _screenWidthUnits / _screenWidth;
        }

        public int UnitsToPixels(float units)
        {             
            return (int) Mathf.Round(units * _screenWidth / _screenWidthUnits);
        }
        
        // converts vector in pixels space to units space with offset
        public Vector3 VectorInPixelsToUnits(Vector3 pos)
        {
            var newPos = Vector3.zero;
            var val = _flowerbedToZeroOffsetXInUnits > 0 ? _flowerbedToZeroOffsetXInUnits : -_unitsToPixelsOffsetXInUnits;
            newPos.x = PixelsToUnits(pos.x) + val;
            newPos.y = _heightAboveFloorInUnits;
            newPos.z = PixelsToUnits(pos.y);
            return newPos;
        }
        
        // converts vector in units space to pixels space with offset
        public Vector3 VectorInUnitsToPixels(Vector3 pos)
        {
            var newPos = Vector3.zero;
            var val = _flowerbedToZeroOffsetXInUnits > 0 ? - UnitsToPixels(Mathf.Abs(_flowerbedToZeroOffsetXInUnits)) : UnitsToPixels(_unitsToPixelsOffsetXInUnits);
            newPos.x = UnitsToPixels(pos.x) + val;
            newPos.y = UnitsToPixels(pos.z);
            newPos.z = 0f;
            return newPos;
        }
        
        public void OnCalculatedGameOverValues()
        {
            _animator.SetTrigger(GAME_OVER);
        }

        public void OnSceneReady()
        {
            _animator.SetTrigger(READY);
        }
        
        public void OnStartButtonPressed()
        {
            _animator.SetTrigger(START);
        }

        public void OnPauseButtonPressed()
        {
            _animator.SetBool(PAUSE, !config.levelState.value.showPauseMenu);
            Time.timeScale = config.levelState.value.showPauseMenu ? 1 : 0;
        }

        public virtual void OnCanceledButtonPressed()
        {
            // DoCanceledStuff
            // ##############################################################

            Time.timeScale = 1;
            config.curLevelScore.value = 0;
            config.curLevel.value.levelDone = false;
            _animator.SetTrigger(CANCELED);
        }

        protected void GameIsOver()
        {
            StopAllCoroutines();
            config.levelPointsReached.value.Add(config.curLevelScore.value);
            config.curLevel.value.levelDone = true;
            
            if (_gameEndedGameEvent != null)
            {
                _gameEndedGameEvent.Raise();
            }
            
            _animator.SetTrigger(CALCULATE_GAME_OVER_VALUES);
        }

        private void DisplayCountDown(int value)
        {
            _gameTimer.text = "" + value;
        }
                
        protected IEnumerator CountDown(float value)
        {
            _time = (int) value;
            while (_time >= 0)
            {
                DisplayCountDown(_time);
                yield return new WaitForSeconds(1f);
                _time--; 				
            }
            GameIsOver();
        }
        
        public static int GetRandomNumberInRange(int min, int max)
        {
            return Random.Range(min, max);
        }

        public void OnNeedTouchablesInfo(TouchablesInfoListGameEvent e)
        {
            foreach (var touchable in _activeTouchables)
            {
                var mole = touchable as MoleController;
                if (mole != null)
                {
                    if (mole.touchResistent)
                    {
                        continue;
                    }
                }
                var touchableInfo = new TouchablesInfoListGameEvent.TouchableInfo{colliderPosUnits = touchable.GetGlobalColliderCenterPosition()};
                e.parameter.Add(touchableInfo);
            }
        }
        
        protected void RaiseDontNeedCountdownGameEventIfNeeded()
        {
            if (config.curLevel.value.levelTimeInSec <= 0)
            {
                _dontNeedCountdownGameEvent.Raise();
            }
        }
        
        protected float CalcObjectSizeInPixels(Touchable touchable)
        {
            var minX = Mathf.Infinity;
            var minY = Mathf.Infinity;
            var maxX = -Mathf.Infinity;
            var maxY = -Mathf.Infinity;

            var bounds = touchable.GetComponentInChildren<BoxCollider>().bounds;
            var v3Center = bounds.center;
            var v3Extents = bounds.extents;

            var corners = new Vector3[8];

            corners[0] =
                new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y,
                    v3Center.z - v3Extents.z); // Front top left corner
            corners[1] =
                new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y,
                    v3Center.z - v3Extents.z); // Front top right corner
            corners[2] =
                new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y,
                    v3Center.z - v3Extents.z); // Front bottom left corner
            corners[3] =
                new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y,
                    v3Center.z - v3Extents.z); // Front bottom right corner
            corners[4] =
                new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y,
                    v3Center.z + v3Extents.z); // Back top left corner
            corners[5] =
                new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y,
                    v3Center.z + v3Extents.z); // Back top right corner
            corners[6] =
                new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y,
                    v3Center.z + v3Extents.z); // Back bottom left corner
            corners[7] =
                new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y,
                    v3Center.z + v3Extents.z); // Back bottom right corner

            for (var i = 0; i < corners.Length; i++)
            {
                var corner = touchable.transform.TransformPoint(corners[i]);
                corner = _camera.WorldToScreenPoint(corner);
                if (corner.x > maxX) maxX = corner.x;
                if (corner.x < minX) minX = corner.x;
                if (corner.y > maxY) maxY = corner.y;
                if (corner.y < minY) minY = corner.y;
                minX = Mathf.Clamp(minX, 0, _screenWidth);
                maxX = Mathf.Clamp(maxX, 0, _screenWidth);
                minY = Mathf.Clamp(minY, 0, _screenHeight);
                maxY = Mathf.Clamp(maxY, 0, _screenHeight);
            }

            var width = maxX - minX;
            var height = maxY - minY;
            
            Debug.Log("object width: " + width);
            Debug.Log("object height: " + height);
            
            return width >=  height ? width : height;
        }
    }
}
