using Common.Configs;
using Common.Events;
using UnityEngine;

namespace Environment.Score
{
    public class PanTapLevelScoreCalculator : ScoreCalculator
    {
        private FlowerLevelConfig _curFlowerLevel
        {
            get
            {
                return _curLevel.value as FlowerLevelConfig;
            }
        }
        
        public void OnCalculatePoints()
        {           
            var hit = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].tapInit.hitTarget;
            //var acc = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].tapInit.accuracyObject.accuracyPercent;
            
            // only subtract points if not hit object because you could tap on objects multiple times and gain many points, that shouldn't be possible
            if (!hit)
            {
                OnCalculateTapInitPoints(false, false, 0);
            }            
        }

        public void OnInteractionWithNotTouchableObject(HitSthGameEvent e)
        {
            if (!e.hit)
            {
                return;
            }
            AddToScore(_hitSthWrongPoints);
            RaiseUpdateScoreGameEvent();
        }

        public void OnEnteredTarget(BoolGameEvent e)
        {
            if (e.value)
            {
                var acc = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].tapInit.accuracyObject.accuracyPercent;
                OnCalculateTapInitPoints(true, false, acc);
                RaiseUpdateScoreGameEvent();
            }
        }
    }
}