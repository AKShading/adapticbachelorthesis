﻿using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Environment.Score
{
	public class ScoreDisplayer : MonoBehaviour
	{
		[FormerlySerializedAs("_text")] [SerializeField] private Text _scoreText;
		[SerializeField] private Text _countdownText;
		[SerializeField] private string _additionalText;
		[SerializeField] private IntVariable _score;
		[SerializeField] private IntListVariable _levelPoints;

		private bool _needCountdown = true;

		public void OnUpdateScore()
		{
			UpdateScore(_needCountdown ? _scoreText : _countdownText);
		}

		public void OnTotalScore()
		{
			var totalPoints = 0;
			foreach (var points in _levelPoints.value)
			{
				totalPoints += points;
			}

			if (totalPoints > 0)
			{
				_scoreText.text = string.Format("{0} {1}", _additionalText, totalPoints);
			}
		}
		
		public void OnDontNeedCountdown()
		{
			_needCountdown = false;
		}

		private void UpdateScore(Text text)
		{
			text.text = string.Format("{0}", _score.value);
		}
	}
}
