using Common.Configs;
using UnityEngine;

namespace Environment.Score
{
    public class TapLevelScoreCalculator : ScoreCalculator
    {
        private MoleLevelConfig _curMoleLevel
        {
            get
            {
                return _curLevel.value as MoleLevelConfig;
            }
        }        
        
        public void OnCalculatePoints()
        {
            var hit = _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].hitTarget;
            var hitResistent = _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].hitResistent;
            var acc = _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].accuracyObject.accuracyPercent;

            OnCalculateTapInitPoints(hit, hitResistent, acc);
        }          
    }
}