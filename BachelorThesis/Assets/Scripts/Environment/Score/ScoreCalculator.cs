using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;

namespace Environment.Score
{
    public class ScoreCalculator : MonoBehaviour
    {
        [SerializeField] protected LevelVariable _curLevel;
        [SerializeField] protected IntVariable _score;
        [SerializeField] protected GameEvent _updateScoreGameEvent;
        
        [FormerlySerializedAs("_hitPoints")] [SerializeField] protected int _positivePoints = 10;
        [SerializeField] protected int _hitSthWrongPoints = -20;
        [SerializeField] protected int _missedPoints = -10;

        [SerializeField] private IntGameEvent _reachedScoreMarkGameEvent;
        
        private int _scoreMarkSteps = 1000;
        private int _scoreMarkCount = 1;
        
        private  void Start()
        {
            _score.value = 0;
            _scoreMarkSteps = _curLevel.value.scoreMark;
        }

        protected void OnCalculateTapInitPoints(bool hit, bool hitSthWrong, float acc)
        {
            var accPoints = Mathf.RoundToInt(acc) / 10;
            
            var points = hitSthWrong ? _hitSthWrongPoints : hit ? _positivePoints * Mathf.Max(accPoints, 1) : _missedPoints;
            
            Debug.LogFormat("Points for tapinit: {0}", points);
            
            AddToScore(points);
            RaiseUpdateScoreGameEvent();       
        }

        protected void AddToScore(int value)
        {
            _score.value += value;
            _score.value = Mathf.Max(0, _score.value);

            CheckIfScoreMarkReached();
        }

        protected void RaiseUpdateScoreGameEvent()
        {
            if (_updateScoreGameEvent != null)
            {
                _updateScoreGameEvent.Raise();
            }
        }

        private void CheckIfScoreMarkReached()
        {
            if (_score.value >= _scoreMarkSteps * _scoreMarkCount)
            {
                _scoreMarkCount++;
                if (_reachedScoreMarkGameEvent != null)
                {
                    _reachedScoreMarkGameEvent.parameter = Mathf.FloorToInt(_score.value / _scoreMarkSteps) * _scoreMarkSteps;
                    _reachedScoreMarkGameEvent.Raise();
                }
            }
        }
    }
}