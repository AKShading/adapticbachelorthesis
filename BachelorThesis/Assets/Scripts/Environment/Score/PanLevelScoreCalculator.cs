using Common.Configs;
using Common.Events;

namespace Environment.Score
{
    public class PanLevelScoreCalculator : ScoreCalculator
    {
        private WaterLevelConfig _curWaterLevel
        {
            get
            {
                return _curLevel.value as WaterLevelConfig;
            }
        }
        
        public void OnCalculatePoints(IntGameEvent e)
        {
            if (e.parameter == 0)
            {
                return;
            }
            AddToScore(e.parameter * _positivePoints);
            RaiseUpdateScoreGameEvent();
        }
    }
}