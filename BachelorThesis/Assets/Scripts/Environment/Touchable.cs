﻿using UnityEngine;

namespace Environment
{
    public abstract class Touchable : MonoBehaviour
    {
        [SerializeField] private BoxCollider _boxCollider;
        
        public virtual bool CanDieNow()
        {
            return false;
        }

        public Vector3 GetGlobalColliderCenterPosition()
        {
            return _boxCollider.transform.TransformPoint(_boxCollider.center);
        }

        public Vector2 GetWidthAndHeightOfCollider()
        {
            return new Vector2(_boxCollider.bounds.size.x, _boxCollider.bounds.size.y);
        }
    }
}
