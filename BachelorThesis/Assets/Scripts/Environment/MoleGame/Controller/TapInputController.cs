﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Common.Configs;
using Common.Enum;
using Common.Events;
using Common.Variables;
using Environment.MoleGame.Mole;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Environment.MoleGame.Controller
{	
    public class TapInputController : InputController 
    {   
        [SerializeField] private FloatVariable _maximumTouchPressureSuppported;
        
        private int _fingerID;
        private MoleLevelConfig _curMoleLevel
        {
            get
            {
                return _config.curLevel.value as MoleLevelConfig;
            }
        }		

        
        // Update is called once per frame
        private void Update () 
        {
            if (!_config.levelState.value.running)
            {
                return;
            }
            
            if (Input.touchCount > 0)
            {                
                for (var i = 0; i < Input.touchCount; ++i)
                {
                    var touch = Input.GetTouch(i);

                    if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        return;
                    }
                    
                    if (touch.phase == TouchPhase.Began && _fingerID == -1)
                    {
                        ReactOnTap(touch.position);
                        _fingerID = touch.fingerId;
                        AddValueToAccelerometerList();
                        AddValueToTouchPressureList(touch);                       
                    }

                    if ((touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) && _fingerID == touch.fingerId)
                    {
                        AddValueToTouchPressureList(touch);
                    }

                    if ((touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) && _fingerID == touch.fingerId)
                    {
                        _fingerID = -1;
                        ReactOnTapEnded();
                        AddValueToAccelerometerList();
                        AddValueToTouchPressureList(touch);
                    }
                }
            }
            else
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                
                if (Input.GetMouseButtonDown(0))
                {
                    ReactOnTap(Input.mousePosition);
                }

                if (Input.GetMouseButtonUp(0))
                {
                    ReactOnTapEnded();
                }
            }          
        }

        private void AddValueToTouchPressureList(Touch touch)
        {
            if (Input.touchPressureSupported)
            {
                _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].tapPressure.Add(touch.pressure);
                _maximumTouchPressureSuppported.value = touch.maximumPossiblePressure > _maximumTouchPressureSuppported.value ? touch.maximumPossiblePressure : _maximumTouchPressureSuppported.value;
            }
        }

        private void ReactOnTap(Vector3 pos)
        {            
            pos.x = Mathf.Round(pos.x);
            pos.y = Mathf.Round(pos.y);
            pos.z = Mathf.Round(pos.z);
                
            ResetAndRequestTouchableInfo();        
                                      
            VisualizeTap(pos);
            
            DoRaycastAndSaveTapInfo(pos);
            
            _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].possibleTargetPos = SaveAndVisualizeDistances(pos);
            
            
            // if not hit, calculate and visualize nearest 
            if (!_curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].hitTarget)
            {
                VisualizeNearestPossibleTarget(_curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].possibleTargetPos);
            }
        }

        private void ReactOnTapEnded()
        {
            _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].tapDuration = Time.time - _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].tapTime;

            Debug.LogFormat("actual Time is : {0}", Time.time);
            Debug.LogFormat("tap time is: {0}", _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].tapTime);
            Debug.LogFormat("TapDuration is: {0}", _curMoleLevel.tapInitInfoObjects[_curMoleLevel.tapInitInfoObjects.Count - 1].tapDuration);
        }

        private void DoRaycastAndSaveTapInfo(Vector3 pos)
        {
            var ray = _mainCam.ScreenPointToRay(pos); 
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                var mole = hit.transform.GetComponentInParent<MoleController>();
                
                if (mole != null && mole.CanDieNow())
                {
                    _curMoleLevel.tapInitInfoObjects.Add(CreateTapInitObject(mole, pos)); 
                    mole.OnTouched();
                    Debug.Log("HITPOS: " + _gameController.VectorInUnitsToPixels(hit.point));
                    Debug.Log("TOUCHPOS: " + pos);
                    DrawRay(ray, hit.distance, Color.yellow, 10f);
                    //Debug.Log("Hit a mole");
                }
                else
                {
                    _curMoleLevel.tapInitInfoObjects.Add(CreateTapInitObject(null, pos)); 
                }
            }
            else
            {   
                _curMoleLevel.tapInitInfoObjects.Add(CreateTapInitObject(null, pos)); 

                DrawRay(ray, 1000f, Color.white, 10f);
                //Debug.Log("Did not Hit");
            }
            if (_calculateScoreGameEvent != null)
            {
                _calculateScoreGameEvent.Raise();
            }
        }       

        /*public void OnLevelStateChanged()
        {
            if (!levelState.value.calculateGameOverValues)
            {
                return;
            }
            
            var avgAccuracy = CalculateAvgAccuracy(_tapInfoObjects);
            _curLevel.value.avgAccuracy.accuracyPercent = avgAccuracy;
            _curLevel.value.avgAccuracy.accuracyString = AccuracyToString(avgAccuracy);
            _curMoleLevel.tapInitInfoObjects = _tapInfoObjects;
            
            if (_calculatedGameOverValuesGameEvent)
            {
                _calculatedGameOverValuesGameEvent.Raise();
            }
        }*/
    }
}
