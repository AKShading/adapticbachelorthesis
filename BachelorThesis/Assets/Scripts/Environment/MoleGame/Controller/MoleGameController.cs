﻿using System.Collections.Generic;
using Common.Configs;
using Common.Events;
using Common.Variables;
using Environment.MoleGame.Mole;
using UnityEngine;

namespace Environment.MoleGame.Controller
{
    public class MoleGameController : GameController
    {        
        const float EXTRA_SCALE = 0.66f;
        
        [SerializeField] private MoleController _molePrefab;
        [SerializeField] private FloatVariable _moleTapAreaSizeInPixelsOnDevice; // for this device
        [SerializeField] private FloatVariable _moleHeightInPixelsOnDevice; // for this device 
        [SerializeField] private List<MoleLevelConfig> _levels;
        
        private float _moleNeededHeightUnits;
        private float _moleNeededHeight;
        private float _moleNeededWidthUnits;
        private float _moleNeededWidth;
        private float _moleTapAreaPlusPadding;
        private int _rows;
        private int _columns;
        private List<MoleController> _recycledMoles;
        
        private int _moleCounter;
        private int _sizesAndAmountsIndex;

        private float _moleScaler;

        private MoleLevelConfig _curMoleLevel
        {
            get
            {
                return config.curLevel.value as MoleLevelConfig;
            }
        }		
    
        
        // Use this for initialization
        protected override void Start ()
        {
            config.curLevel.value = _levels[0];
            
            foreach (var level in _levels)
            {
                if (!level.levelDone)
                {
                    config.curLevel.value = level;
                    break;
                }
            }

            _moleCounter = _curMoleLevel.sizesAndAmounts[0].amount * (_curMoleLevel.paddingLevel ? 2 : 1);
            
            ClearValues();
            
            SetTapAreaAndPaddingSizes();
                         
            base.Start();
            
            // taparea plus padding 
            _moleTapAreaPlusPadding = (config.tapAreaActualNeededSize.value + config.paddingActual.value) * config.pixelsPerPoint.value;
            
            // calculate the pixels size of the object, check the needed scale and then scale object by this value
            //_objectScaler = _tapAreaActualNeededSize / _objectSize;

            if (_curMoleLevel.levelNumber == 1)
            {
                var mole = Instantiate(_molePrefab);
                mole.Init(null, new Vector3(2,2,0), -1, 1f, false, false);
                _moleTapAreaSizeInPixelsOnDevice.value = CalcObjectSizeInPixels(mole.GetComponent<MoleController>());
                _moleHeightInPixelsOnDevice.value = UnitsToPixels(mole.GetComponent<MoleController>().GetMeshRenderer().bounds.size.z);
                Destroy(mole.gameObject);
            }
            
            CalculateMoleSizes();
            CalculateGrid();

            Debug.Log("rows: " + _rows);
            Debug.Log("columns: " + _columns);
            
            _recycledMoles = new List<MoleController>();

            RaiseDontNeedCountdownGameEventIfNeeded();
            
            OnSceneReady();
        }

        private void Update() 
        {
            if (_recycledMoles.Count == 0 || !config.levelState.value.running)
            {
                return;
            }
            
            if (_curMoleLevel.paddingLevel)
            {
                if(_recycledMoles.Count == 2)
                {
                    GenerateMolePair(_recycledMoles[0], _recycledMoles[1]);
                    _recycledMoles.RemoveRange(0, 2);
                }
                return;
            }

            foreach (var mole in _recycledMoles)
            {
                var randGridRow = GetRandomNumberInRange(0, _rows-1); // make sure that top row isn't used 
                var randGridCol = GetRandomNumberInRange(0, _columns);
                
                GenerateMole(mole, randGridRow, randGridCol);
            }
            _recycledMoles.Clear();
        }

        private void CalculateMoleScale()
        {    
            _moleScaler = config.tapAreaActualNeededSize.value * config.pixelsPerPoint.value / _moleTapAreaSizeInPixelsOnDevice.value;
        }

        private void CalculateMoleSizes()
        {                 
            CalculateMoleScale();
            _moleNeededHeight = _moleHeightInPixelsOnDevice.value * _moleScaler + _neededPaddingInPixels;
            _moleNeededHeightUnits = PixelsToUnits(_moleNeededHeight);
            
            _moleNeededWidth = _moleTapAreaSizeInPixelsOnDevice.value * _moleScaler + _neededPaddingInPixels;
            _moleNeededWidthUnits = PixelsToUnits(_moleNeededWidth);
        }

        private void CalculateGrid()
        {           
            _columns = (int) (_screenWidth / _moleNeededWidth);
            _rows = (int) (_screenHeight / _moleNeededHeight);
        }
        
        private void ForTesting()
        {
            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    var mole = Instantiate(_molePrefab);
                    mole.Init(_flowerbedReference.transform, GridPosition(i, j), -1, _moleScaler, _curMoleLevel.bossLevel, false);
                }
            }
        }

        private void GenerateNeededMoles()
        {         
            for (var i = 0; i < _curMoleLevel.molesAmountAtSameTime; i++)
            {   
                if (_curMoleLevel.paddingLevel)
                {
                    GenerateMolePair(null, null);
                }
                else
                {
                    var randGridRow = GetRandomNumberInRange(0, _rows-1);
                    var randGridCol = GetRandomNumberInRange(0, _columns);
                    GenerateMole(null, randGridRow, randGridCol);
                }
            }
        }

        private void GenerateMolePair(MoleController firstMole, MoleController secondMole)
        {
            var randGridRow = GetRandomNumberInRange(0, _rows-1);
            var randGridCol = GetRandomNumberInRange(0, _columns);

            var pos = GridPosition(randGridRow, randGridCol);
            
            var rand = GetRandomNumberInRange(0, 2);
            var sign = rand == 0 ? -1 : 1;

            sign = (randGridCol + sign) >= 0 && (randGridCol + sign) < _columns ? sign : -sign;
            
            var moleDad = GenerateMole(firstMole, randGridRow, randGridCol);
            
            var secondPos = pos;
            var width = moleDad.GetWidthAndHeightOfCollider().x;
            secondPos.x += sign * (width * 0.5f + width * EXTRA_SCALE * 0.5f + PixelsToUnits(_neededPaddingInPixels));
            Debug.LogFormat("Sign: {0}, width: {1}, second width: {2}", sign, UnitsToPixels(width), UnitsToPixels(width * EXTRA_SCALE));
                                   
            var moleChild = GenerateResistentMole(secondMole, secondPos, EXTRA_SCALE);   
            moleDad.AddBuddy(moleChild);
        }
        
        private MoleController GenerateMole(MoleController mole, int randGridRow, int randGridCol)
        {
            if (mole == null)
            {
                var moleC = Instantiate(_molePrefab);
                mole = moleC; 
            }
            mole.Init(_flowerbedReference.transform, GridPosition(randGridRow, randGridCol), _curMoleLevel.molesVisibleTime, _moleScaler, _curMoleLevel.bossLevel, false);         
            _activeTouchables.Add(mole);
            return mole;
        }

        private MoleController GenerateResistentMole(MoleController mole, Vector3 pos, float extraScale)
        {        
            if (mole == null)
            {
                var moleC = Instantiate(_molePrefab);
                mole = moleC;
            }
            mole.Init(_flowerbedReference.transform, pos, _curMoleLevel.molesVisibleTime, _moleScaler * extraScale, _curMoleLevel.bossLevel, true);
            _activeTouchables.Add(mole);
            return mole;
        }

        private Vector3 GridPosition(int row, int col)
        {
            var pos = Vector3.zero;
            
            // place moles inside flowerbed
            pos.x = (_flowerbedReference.bounds.size.x / _columns / 2 + (col * (_flowerbedReference.bounds.size.x / _columns))) + _flowerbedToZeroOffsetXInUnits;
            pos.z = _flowerbedReference.bounds.size.z / _rows / 4 + (row * (_flowerbedReference.bounds.size.z / _rows));
            
            return pos;
        }
        
        public void RecycleMole(MoleControllerGameEvent e)
        {
            _moleCounter--;
            if (_moleCounter == 0)
            {
                _sizesAndAmountsIndex++;
                if (_curMoleLevel.sizesAndAmounts.Count > _sizesAndAmountsIndex)
                {
                    _moleCounter = _curMoleLevel.sizesAndAmounts[_sizesAndAmountsIndex].amount * (_curMoleLevel.paddingLevel ? 2 : 1);
                    SetTapAreaAndPaddingSizes();
                }
                else
                {
                    GameIsOver();
                    return;
                }
                CalculateMoleSizes();
                CalculateGrid();
            }  
            _recycledMoles.Add(e.moleController);
            _activeTouchables.Remove(e.moleController);
        }

        private void SetTapAreaAndPaddingSizes()
        {
            if (!_curMoleLevel.paddingLevel)
            {               
                config.tapAreaActualNeededSize.value = _curMoleLevel.sizesAndAmounts[_sizesAndAmountsIndex].size;
                _neededPaddingInPixels = config.paddingActual.value * config.pixelsPerPoint.value;;
            }
            else
            {
                config.tapAreaActualNeededSize.value = config.suggestedMinSize.value;
                config.paddingActual.value = _curMoleLevel.sizesAndAmounts[_sizesAndAmountsIndex].size;
                _neededPaddingInPixels = config.paddingActual.value * config.pixelsPerPoint.value;
                Debug.LogFormat("Padding: {0}, Converted to Pixels {1}", config.paddingActual.value, _neededPaddingInPixels);
            }    
        }

        public void OnLevelStateChanged()
        {
            if (config.levelState.value.running && !_wasPaused)
            {			
                //ForTesting();	
                GenerateNeededMoles();
                if (config.curLevel.value.levelTimeInSec > 0)
                {
                    StartCoroutine(CountDown(config.curLevel.value.levelTimeInSec));
                }
            }
            
            if (config.levelState.value.showPauseMenu)
            {
                _wasPaused = true;
            }        
        }

        public override void OnCanceledButtonPressed()
        {
            ClearValues();
            base.OnCanceledButtonPressed();
        }

        private void ClearValues()
        {
            _curMoleLevel.tapInitInfoObjects.Clear();
        }
    }
}
