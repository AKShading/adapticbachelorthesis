﻿using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace Environment.MoleGame.Mole
{
    public class MoleController : Touchable
    {
        public const string DOWN = "Down";
        public const string UP = "Up";
        public const string DIE = "Die";
        public const string BLINK = "Blink";


        [SerializeField] private IntVariable _bossTapsToDisappear;
        [SerializeField] private Camera _camera;
        [SerializeField] private Animator _animator;
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private MoleControllerGameEvent _recycleMoleGameEvent;
        [SerializeField] private MoleController _buddy;
        [SerializeField] private Canvas _healthCanvas;
        [SerializeField] private UnityEvent _damageReceivedGameEvent;
        
        private int _amountOfTouchesToDisappear;
        private int _timeToDisappearCount;

        private bool _needToDisappear;
        private bool _died;
        private bool _canDieNow;
        private bool _boss;
        
        public bool touchResistent { get; private set; }
        
        public float appearedTime { get; private set; }

        public void Init(Transform parent, Vector3 pos, int timeToDisappear, float scale, bool boss, bool resistent)
        {
            _died = false;
            _canDieNow = false;
            touchResistent = resistent;
            _boss = boss;
            _amountOfTouchesToDisappear = boss ? _bossTapsToDisappear.value : 1;
            transform.parent = null;
            transform.localScale = Vector3.one * scale;
            transform.parent = parent;
            transform.position = pos;
            _buddy = null;
            CheckIfNeedToDisappear(timeToDisappear);
            Appear();
        }

        public void AddBuddy(MoleController buddy)
        {
            _buddy = buddy;
        }
        
        private void CheckIfNeedToDisappear(int value)
        {
            _timeToDisappearCount = value;
            _needToDisappear = value > 0;
        }
    
        public void Disappear()
        {
            _canDieNow = false;		
            _animator.SetTrigger(DOWN);
            if (_buddy != null)
            {
                _buddy.Disappear();
            }
            
        }

        private void Appear()
        {
            _animator.ResetTrigger(BLINK);
            _animator.SetTrigger(UP);
        }

        private void CountDownIfNeeded()
        {
            if (!_needToDisappear)
            {
                return;
            }

            _timeToDisappearCount--;
            if (_timeToDisappearCount <= 0)
            {
                Disappear();
            }
        }

        private void FinishedAppearing()
        {
            _canDieNow = true;
            appearedTime = Time.time;
            
            _healthCanvas.gameObject.SetActive(_boss);
        }
    
        private void FinishedDisappearing()
        {
            _healthCanvas.gameObject.SetActive(false);
            _recycleMoleGameEvent.moleController = this;
            _recycleMoleGameEvent.Raise();
        }

        public void OnTouched()
        {
            if (touchResistent)
            {
                return;
            }
            _amountOfTouchesToDisappear--;
            if (_boss && _damageReceivedGameEvent != null)
            {
                _damageReceivedGameEvent.Invoke();
            }
            _animator.SetTrigger(BLINK);
            if(!_died && _amountOfTouchesToDisappear <= 0)
            {
                _animator.SetTrigger(DIE);
                _died = true;
                _canDieNow = false;
            }
        }

        public override bool CanDieNow()
        {
            return _canDieNow;
        }

        public MeshRenderer GetMeshRenderer()
        {
            return _meshRenderer;
        }
    }
}
