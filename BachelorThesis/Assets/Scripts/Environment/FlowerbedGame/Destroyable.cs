﻿using Common.Events;
using Environment.FlowerbedGame.Controller;
using UnityEngine;

namespace Environment.FlowerbedGame
{
    public class Destroyable : MonoBehaviour
    {
        private const string HIT = "Hit";
        private const string STOP = "Stop";
        private const float ALLOWED_ROTATION = 90f;
        
        [SerializeField] private Animator _animator;
        [SerializeField] private HitSthGameEvent _hitNotTouchableGameEvent;
        private bool _animating;
        private bool _stop;

        private void Update()
        {
            if (!_animating || _stop)
            {
                return;
            }
            CheckAndStopRotation();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_stop)
            {
                return;
            }
            
            var apple = other.GetComponent<AppleController>();
            if (apple == null)
            {
                return;
            }
            
            _animating = true;
            RaiseInteractionEvent();
            _animator.SetBool(HIT, true);
        }

        private void OnTriggerExit(Collider other)
        {
            if (_stop)
            {
                return;
            }
            
            var apple = other.GetComponent<AppleController>();
            if (apple == null)
            {
                return;
            }
            
            _animator.SetBool(HIT, false);
            _animating = false;
            RaiseInteractionEvent();
            
        }

        private void RaiseInteractionEvent()
        {
            if (_hitNotTouchableGameEvent != null)
            {
                _hitNotTouchableGameEvent.hit = _animating;
                _hitNotTouchableGameEvent.destroyable = true;
                _hitNotTouchableGameEvent.Raise();
            }
        }

        private void CheckAndStopRotation()
        {
            // Debug.Log("Rotation: " + transform.eulerAngles.y);
            if (transform.eulerAngles.y <= 360 - ALLOWED_ROTATION && transform.eulerAngles.y > 0f)
            {
                _animator.SetBool(STOP, true);
                _stop = true;
                _animating = false;
            }
        }
    }
}
