using System.Collections.Generic;
using Common.Configs;
using Common.Events;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;

namespace Environment.FlowerbedGame.Controller
{
    public class AppleController : Touchable
    {
        private const string DISAPPEAR = "Disappear";
        private const string FALLING_SPEED = "FallingSpeed";
        [SerializeField] private AppleControllerGameEvent _removeAppleControllerGameEvent;
        [SerializeField] private Animator _animator;
        [SerializeField] private bool _redYellow;
        [SerializeField] private LevelVariable _curLevel;
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private Material _materialToChangeTo; 
        [SerializeField] private List<Color> _colors;
        [SerializeField] private Renderer _renderer;
        
        private MaterialPropertyBlock _propBlock;
        
        private Vector3 _initialPos;
        
        private FlowerLevelConfig _curFlowerLevel
        {
            get { return _curLevel.value as FlowerLevelConfig; }
        }

        private void OnEnable()
        {
            _propBlock = new MaterialPropertyBlock();
        }

        public bool IsRedYellow()
        {
            return _redYellow;
        }

        public void Init(Vector3 pos, float scale, Transform parent, float speed)
        {
            transform.localScale *= scale;
            transform.parent = parent;
            _initialPos = pos;
            transform.position = _initialPos;
            _animator.SetFloat(FALLING_SPEED, speed);
            SetMaterial();
        }

        public void Respawn()
        {
            transform.position = _initialPos;
            SetMaterial();
            _animator.SetBool(DISAPPEAR, false);
        }

        public void Disappear()
        {
            _animator.SetBool(DISAPPEAR, true);
        }
        
        public void RemoveFromGame()
        {
            if (_removeAppleControllerGameEvent != null)
            {
                _removeAppleControllerGameEvent.appleController = this;
                _removeAppleControllerGameEvent.Raise();
            }
        }

        private void SetMaterial()
        {
            if (_curFlowerLevel._extraLevel)
            {
                _meshRenderer.material = _materialToChangeTo;
            }
            else
            {
                SetColor();
            }
        }

        private void SetColor()
        {
            _renderer.GetPropertyBlock(_propBlock);
            _propBlock.SetColor("_Color", _colors[GameController.GetRandomNumberInRange(0, _colors.Count)]);
            _renderer.SetPropertyBlock(_propBlock);
        }
    }
}