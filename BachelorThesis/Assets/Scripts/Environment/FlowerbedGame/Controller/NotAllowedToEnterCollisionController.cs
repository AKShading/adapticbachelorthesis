﻿using Common.Events;
using UnityEngine;

namespace Environment.FlowerbedGame.Controller
{   
    public class NotAllowedToEnterCollisionController : MonoBehaviour 
    {
        [SerializeField] private GameEvent _dropObjectGameEvent;
        [SerializeField] private HitSthGameEvent _hitNotTouchableGameEvent;
        
        private void OnTriggerEnter(Collider other)
        {
            CheckAndHandleCollision(other);
        }

        private void OnTriggerExit(Collider other)
        {
            var apple = other.GetComponent<AppleController>();
            if (apple == null)
            {
                return;
            }
            RaiseInteractionEvent(false);
        }

        private void AppleWantsToEnterFromWrongDirection()
        {
            
            if (_dropObjectGameEvent != null)
            {
                _dropObjectGameEvent.Raise();
            }

            RaiseInteractionEvent(true);      
        }

        private void CheckAndHandleCollision(Collider other)
        {
            var apple = other.GetComponent<AppleController>();
            if (apple == null)
            {
                return;
            }
            
            AppleWantsToEnterFromWrongDirection();
        }

        private void RaiseInteractionEvent(bool enter)
        {
            if (_hitNotTouchableGameEvent != null)
            {
                _hitNotTouchableGameEvent.hit = enter;
                _hitNotTouchableGameEvent.destroyable = false;
                _hitNotTouchableGameEvent.Raise();
            }
        }
    }
}
