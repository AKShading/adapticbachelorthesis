﻿using Common.Events;
using UnityEngine;

namespace Environment.FlowerbedGame.Controller
{
    public class CameraController : MonoBehaviour 
    {
        [SerializeField] private GameEvent _sceneReadyGameEvent;
        
        public void FinishedAnimations()
        {
            if (_sceneReadyGameEvent != null)
            {
                _sceneReadyGameEvent.Raise();
            }
        }
    }
}
