﻿using System.Collections.Generic;
using Common.Configs;
using Common.Events;
using Common.Variables;
using TMPro;
using UI;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.UIElements;

namespace Environment.FlowerbedGame.Controller
{
    public class PanInputController : InputController
    {
        [SerializeField] private bool _debugDistance;
        [SerializeField] private bool _debugNormDistance;
        [SerializeField] private bool _debugNormCatmullDistance;
        [SerializeField] private bool _debugNearest;
        [SerializeField] private int _normalizePathValue;
        [SerializeField] private int _targetPixelsPerSecond;
      
        [SerializeField] private Catmul catmull;
        
        private DraggableObjectInfo _draggableObjectInfo = new DraggableObjectInfo();
        private bool _hitNotTouchableObject;
        private bool _hitDestroyableObject;
        
        private FlowerLevelConfig _curFlowerLevel
        {
            get
            {
                return _config.curLevel.value as FlowerLevelConfig;
            }
        }

        private class DraggableObjectInfo
        {
            public AppleController draggableObject;
            public int fingerID = -1;
            public bool active;
            public Vector3 offsetToTap;
        }
        
        private void Update()
        {          
            if (!_config.levelState.value.running)
            {
                return;
            }

            if (Input.touchCount > 0)
            {
                for (var i = 0; i < Input.touchCount; ++i)
                {
                    var touch = Input.GetTouch(i);
                    
                    if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        return;
                    }
                    
                    if (Input.GetTouch(i).phase == TouchPhase.Began)
                    {
                        if (_draggableObjectInfo.active)
                        {
                            return;
                        }
                        ReactOnInput(touch.position);
                        if (_draggableObjectInfo.active)
                        {
                            _draggableObjectInfo.fingerID = touch.fingerId;
                            _draggableObjectInfo.offsetToTap = _draggableObjectInfo.draggableObject.transform.position - GetComponent<GameController>().VectorInPixelsToUnits(touch.position);
                            _draggableObjectInfo.offsetToTap.y = 0;
                        }
                        AddValueToAccelerometerList();
                    }

                    if (Input.GetTouch(i).phase == TouchPhase.Moved)
                    {
                        if (_draggableObjectInfo.active && _draggableObjectInfo.fingerID == touch.fingerId)
                        {
                            MoveDraggableObject(touch.position);
                        }
                    }

                    if (Input.GetTouch(i).phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        if (_draggableObjectInfo.active && touch.fingerId == _draggableObjectInfo.fingerID)
                        {
                            OnDropObjectAndCompletePan();
                        }
                        AddValueToAccelerometerList();
                    }
                }
            }
            else
            {              
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    Debug.Log("Clicked on the UI");
                    return;
                }
                
                if (Input.GetMouseButtonDown(0))
                {
                    ReactOnInput(Input.mousePosition);
                    if (_draggableObjectInfo.active)
                    {
                        _draggableObjectInfo.offsetToTap = _draggableObjectInfo.draggableObject.transform.position - GetComponent<GameController>().VectorInPixelsToUnits(Input.mousePosition);
                        _draggableObjectInfo.offsetToTap.y = 0;
                    }
                }

                if (Input.GetMouseButton(0))
                {
                    if (_draggableObjectInfo.active)
                    {
                        MoveDraggableObject(Input.mousePosition);
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (_draggableObjectInfo.active)
                    {
                        OnDropObjectAndCompletePan();
                    }
                }
            }   
            
        }

        private void MoveDraggableObject(Vector3 inputPos)
        {
            var oldPos = _draggableObjectInfo.draggableObject.transform.position;
            var pos = _gameController.VectorInPixelsToUnits(inputPos) + _draggableObjectInfo.offsetToTap;
            pos.y = _draggableObjectInfo.draggableObject.transform.position.y;
            _draggableObjectInfo.draggableObject.transform.position = pos;
            if(_debugDistance) VisualizeDistance(oldPos, pos, Color.yellow, 1000f);     
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count-1].pointsOnPath.Add(CreatePanPointInfoObject(inputPos));
        }

        private void ReactOnInput(Vector3 pos)
        {
            pos.x = Mathf.Round(pos.x);
            pos.y = Mathf.Round(pos.y);
            pos.z = Mathf.Round(pos.z);
            
            ResetAndRequestTouchableInfo();
            
            VisualizeTap(pos);  
            
            DoRaycastAndSaveTapInfo(pos);
            
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].tapInit.possibleTargetPos = SaveAndVisualizeDistances(pos);
            
            // auswertung 
            // if not hit, calculate and visualize nearest
            // TODO: check if nearest even is target by setting up a const max dist 
            if (!_curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count-1].tapInit.hitTarget)
            {
                if(_debugNearest) VisualizeNearestPossibleTarget(_curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count-1].tapInit.possibleTargetPos);
            }
        }
       

        private void CompletePanInfo()
        {
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].distance = CalculatePanDistance(_curFlowerLevel.panInfoObjects);
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].duration = CalculatePanDuration(_curFlowerLevel.panInfoObjects);
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].normalizedDistance = CalculateNormalizedPanDistance();
            
            Debug.Log("Distance of paning in pixels: " + _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].distance);
            Debug.Log("Normalized distance of paning in pixels: " + _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].normalizedDistance);
            Debug.Log("Duration of paning: " + _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].duration);
        }

        public void OnEnteredTarget()
        {
            _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].endedSuccessfully = true;
        }

        private float CalculateNormalizedPanDistance()
        {
            var normDist = 0f;
            var secondNormDist = 0f;

            var points = new List<Vector3>();
            points.Clear();


            if (_curFlowerLevel.panInfoObjects.Count > 0 && _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath.Count > 0)
            {
                var time = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].duration;
                var distance = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].distance;
                var velocity = Mathf.Max(distance / time, 1);

                var factor = _targetPixelsPerSecond * _config.pixelsPerPoint.value / velocity;
                var normalizePathValue = Mathf.Max((int) (_normalizePathValue * factor), 1);

                var firstPos = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].tapInit.tapPos;
                points.Add(firstPos); // add two times bc of catmull calculation
                points.Add(firstPos);
                var secondPos = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath[0].pos;

                normDist += (secondPos - firstPos).magnitude;
                
                if (_debugNormDistance)
                    VisualizeDistance(GetComponent<GameController>().VectorInPixelsToUnits(firstPos),
                        GetComponent<GameController>().VectorInPixelsToUnits(secondPos), Color.red, 1000f);

                for (var i = 1; i < _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath.Count; i++)
                {
                    if (i % normalizePathValue == 0 || i == _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath.Count - 1)
                    {
                        firstPos = secondPos;
                        secondPos = _curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath[i].pos;
                        points.Add(secondPos);
                        normDist += (secondPos - firstPos).magnitude;
                        if (_debugNormDistance)
                            VisualizeDistance(GetComponent<GameController>().VectorInPixelsToUnits(firstPos + new Vector3(-30, 0, -30)),
                                GetComponent<GameController>().VectorInPixelsToUnits(secondPos + new Vector3(-30, 0, -30)), Color.red, 1000f);
                    }
                }

                points.Add(_curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath[_curFlowerLevel.panInfoObjects[_curFlowerLevel.panInfoObjects.Count - 1].pointsOnPath.Count - 1].pos); // add second time bc of catmull calculation
                points = catmull.Calc(points);
                for (var i = 0; i < points.Count - 1; i++)
                {
                    firstPos = points[i];
                    secondPos = points[i + 1];
                    secondNormDist += (secondPos - firstPos).magnitude;
                    if (_debugNormCatmullDistance)
                        VisualizeDistance(GetComponent<GameController>().VectorInPixelsToUnits(firstPos + new Vector3(30, 0, 30)),
                            GetComponent<GameController>().VectorInPixelsToUnits(secondPos + new Vector3(30, 0, 30)), Color.blue, 1000f);
                }

                Debug.LogFormat("Catmull normalized distance of paning in pixels: {0}", secondNormDist);
            }
            return normDist;
        }

        
        
        private PanPointInfo CreatePanPointInfoObject(Vector3 pos)
        {
            return new PanPointInfo{pos = pos, hitObj = _hitDestroyableObject};
        }
        
        private void CreatePanInfoObject(Touchable touchableObj, Vector3 pos)
        {
            var panInfo = new PanInfo {tapInit = CreateTapInitObject(touchableObj, pos)};
            _curFlowerLevel.panInfoObjects.Add(panInfo);
        }
        
        
        private void DoRaycastAndSaveTapInfo(Vector3 pos)
        {
            var ray = _mainCam.ScreenPointToRay(pos);

            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity))
            {
                var apple = raycastHit.transform.GetComponent<AppleController>();
                CreatePanInfoObject(apple, pos);
                if (apple != null)
                {
                    //Debug.Log("Hit a chestnut");
                    _draggableObjectInfo.draggableObject = apple;
                    _draggableObjectInfo.active = true;
                }
            }
            else
            {
                CreatePanInfoObject(null, pos);
                DrawRay(ray, 1000f, Color.white, 10f);
                //Debug.Log("Did not Hit");
            }   
            
            if (_calculateScoreGameEvent != null)
            {
                _calculateScoreGameEvent.Raise();
            }
        }
        
        public void OnDropObjectAndCompletePan()
        {
            if (!_draggableObjectInfo.active)
            {
                return;
            }
            CompletePanInfo();
            _draggableObjectInfo.active = false;
            _draggableObjectInfo.fingerID = -1;
            _draggableObjectInfo.draggableObject = null;
        }
        
        /*public void OnLevelStateChanged()
        {
            if (!levelState.value.calculateGameOverValues)
            {
                return;
            }

            var tapInitInfos = new List<TapInitInfo>();
            
            foreach (var tapInitInfo in _panInfos)
            {
                tapInitInfos.Add(tapInitInfo.tapInit);
            }
            
            var avgAccuracy = CalculateAvgAccuracy(tapInitInfos);
            _curLevel.value.avgAccuracy.accuracyPercent = avgAccuracy;
            _curLevel.value.avgAccuracy.accuracyString = AccuracyToString(avgAccuracy);
            _curFlowerLevel.panInfoObjects = _panInfos;
            
            if (_calculatedGameOverValuesGameEvent)
            {
                _calculatedGameOverValuesGameEvent.Raise();
            }
        }*/
        
        public void OnInteractionWithNotTouchableObject(HitSthGameEvent e)
        {
            _hitNotTouchableObject = e.hit;
            _hitDestroyableObject = _hitNotTouchableObject && e.destroyable;
            
        }
    }
}


