using UnityEngine;

namespace Environment.FlowerbedGame.Controller
{
    public class ObjectOutsideFlowerbedController : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _meshReference;
        
        
        private void OnTriggerStay(Collider other)
        {
            var apple = other.GetComponent<AppleController>();
            
            if (apple != null)
            {
                if (CheckIfCenterIsInside(other.bounds))
                {
                    apple.Disappear();
                }
            }
        }

        private bool CheckIfCenterIsInside(Bounds bounds)
        {
            var centerX = bounds.center.x;
            var centerZ = bounds.center.z;

            var meshbounds = _meshReference.bounds;
            var extendsX = meshbounds.size.x * 0.5f;
            var extendsZ = meshbounds.size.z * 0.5f;
            
            var minX = meshbounds.center.x - extendsX;
            var maxX = meshbounds.center.x + extendsX;
            var minZ = meshbounds.center.z - extendsZ;
            var maxZ = meshbounds.center.z + extendsZ;

            var outsideFlowerbed = centerX < minX || centerX > maxX;
            outsideFlowerbed = outsideFlowerbed || (centerZ < minZ || centerZ > maxZ);

            return outsideFlowerbed;
        }
    }
}