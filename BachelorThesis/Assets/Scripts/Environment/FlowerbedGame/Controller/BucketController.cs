using Common.Configs;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Environment.FlowerbedGame.Controller
{
    public class BucketController : MonoBehaviour
    {
        private const string RED = "Rot";
        private const string GREEN = "Grün";
        private const string YELLOW = "Gelb";
        private const string BLUE = "Blau";

        [SerializeField] private LevelVariable _curLevel;
        [SerializeField] private TextMesh _text;
        [SerializeField] private GameObject _bucketScalable;
        [SerializeField] private Material _bucketMat;
        [SerializeField] private Material _bucketRingMat;
        private bool _redYellow;
        private static readonly int Tiling = Shader.PropertyToID("_Tiling");

        private FlowerLevelConfig _curFlowerLevel
        {
            get { return _curLevel.value as FlowerLevelConfig; }
        }
        public void SetRed(bool red)
        {
            _redYellow = red;
        }

        public bool IsRed()
        {
            return _redYellow;
        }

        public void Init(Vector3 pos, Vector3 scale, float tiling)
        {
            transform.position = pos;
            _bucketScalable.transform.localScale = scale;
            SetMaterialTiling(tiling);
            DisplayText();
        }
        private void DisplayText()
        {
            if (_curFlowerLevel._extraLevel)
            {
                _text.text = _redYellow ? YELLOW : BLUE;
            }
            else
            {
                _text.text = _redYellow ? RED : GREEN;    
            }
        }

        private void SetMaterialTiling(float amount)
        {
            if (amount <= 1)
            {
                return;
            }
            var tilingBucket = _bucketMat.mainTextureScale;
            tilingBucket.x = amount;
            _bucketMat.mainTextureScale = tilingBucket;
            
            var tilingBucketRing = _bucketRingMat.mainTextureScale;
            tilingBucketRing.x = amount * 0.5f;
            _bucketRingMat.mainTextureScale = tilingBucketRing;
        }

        public Vector3 GetScale()
        {
            return _bucketScalable.transform.localScale;
        }
    }
}