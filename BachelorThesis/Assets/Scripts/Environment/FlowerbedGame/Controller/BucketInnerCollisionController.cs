using Common.Configs;
using Common.Events;
using Common.Variables;
using UnityEngine;

namespace Environment.FlowerbedGame.Controller
{
    public class BucketInnerCollisionController : MonoBehaviour
    {
        [SerializeField] private GameEvent _dropObjectGameEvent;
        [SerializeField] private BoolGameEvent _enteredBucketGameEvent;
        [SerializeField] private int _redCount;
        [SerializeField] private int _greenCount;
        [SerializeField] private BucketController _bucketController;
        [SerializeField] private LevelVariable _curLevel;
        
        private bool amIRed;
        
        private FlowerLevelConfig _curFlowerLevel
        {
            get { return _curLevel.value as FlowerLevelConfig; }
        }
        
        private void Start()
        {
            if (_bucketController != null)
            {
                amIRed = _bucketController.IsRed();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var apple = other.GetComponent<AppleController>();
            if (apple == null)
            {
                return;
            }
            
            AppleWantsToEnter(apple);    
        }

        private void AppleWantsToEnter(AppleController apple)
        {
            if (apple.IsRedYellow())
            {
                _redCount++;
            }
            else
            {
                _greenCount++;
            }
            
            apple.Disappear();
            if (_dropObjectGameEvent != null)
            {
                _dropObjectGameEvent.Raise();
            }

            var correctBucket = (apple.IsRedYellow() && amIRed) || (!apple.IsRedYellow() && !amIRed); 
            
            if (_enteredBucketGameEvent != null)
            {
                _enteredBucketGameEvent.value = correctBucket;
                _enteredBucketGameEvent.Raise();
            }
        }

        public void OnPutValuesToLevel()
        {
            if (amIRed)
            {
                _curFlowerLevel.redYellowInTargetBucket = _redCount;
                _curFlowerLevel.greenBlueInWrongBucket = _greenCount;
            }
            else
            {
                _curFlowerLevel.greenBlueInTargetBucket = _greenCount;
                _curFlowerLevel.redYellowInWrongBucket = _redCount;
            }
        }
    }
}