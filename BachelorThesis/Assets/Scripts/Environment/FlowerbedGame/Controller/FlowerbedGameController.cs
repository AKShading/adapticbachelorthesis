using System.Collections.Generic;
using Common.Configs;
using Common.Events;
using Common.Variables;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace Environment.FlowerbedGame.Controller
{
    public class FlowerbedGameController : GameController
    {
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private List<AppleController> _applePrefabs;
        [SerializeField] private List<GameObject> _flowerPrefabs;
        [SerializeField] private BucketController _bucketPrefab;
        [SerializeField] private FloatVariable _appleTapAreaSizeInPixelsOnDevice; // #save
        [SerializeField] private List<FlowerLevelConfig> _levels;
        
        // only for savng initial positions for each level
        [FormerlySerializedAs("_apples")] [SerializeField] private List<AppleController> _applesToSave;
        [FormerlySerializedAs("_flowers")] [SerializeField] private List<GameObject> _flowersToSave;
        [FormerlySerializedAs("_buckets")] [SerializeField] private List<BucketController> _bucketsToSave;
        [SerializeField] private bool _save;

        private float _appleScaler = 1f; 
        private float _posXScaler = 1f;
        private float _posZScaler = 1f;
        private float _appleRadiusUnitsOnDevice;

        private int _randomSpawnNumber;
              
        private List<AppleController> _recycledApples = new List<AppleController>();
        private List<bool> _isApplePositionActiveList = new List<bool>();
        private int _applePositionsAmount;
        private List<bool> _isFlowerPositionActiveList = new List<bool>();
        
        private FlowerLevelConfig _curFlowerLevel
        {
            get
            {
                return config.curLevel.value as FlowerLevelConfig;
            }
        }

        protected override void Start()
        {
            base.Start();

            config.curLevel.value = _levels[0];

            foreach (var level in _levels)
            {
                if (!level.levelDone)
                {
                    config.curLevel.value = level;
                    break;
                }
            }
            
            ClearValues();
            
            // create one apple for saving the needed scale, this is only needed on device not in unity 
            if (_curFlowerLevel.levelNumber == 1)
            {
                var apple = Instantiate(_applePrefabs[0]);            
                apple.Init(new Vector3(2, 0, 2), 1f, null,  1);
                _appleTapAreaSizeInPixelsOnDevice.value = CalcObjectSizeInPixels(apple.GetComponent<AppleController>());
                _appleRadiusUnitsOnDevice = apple.GetWidthAndHeightOfCollider().x * 0.5f;
                Destroy(apple.gameObject);
            }

            _appleScaler = config.minTapableSize.value * config.pixelsPerPoint.value / _appleTapAreaSizeInPixelsOnDevice.value;
            // aspect ratio is important not actual size 
            if((_screenWidth / (float) _screenHeight) < (config.referenceScreenResolution.value.x / config.referenceScreenResolution.value.y))
            {
                _posXScaler = _screenWidth / config.referenceScreenResolution.value.x;
                _posZScaler = _screenHeight / config.referenceScreenResolution.value.y;
            }

            DeactivateOverlappingApplesAndFlowers();

            if (_curFlowerLevel.levelTimeInSec > 0)
            {
                _randomSpawnNumber = GetRandomNumberInRange(1, _applePositionsAmount + 1);
            }
        }
   
        public void OnLevelStateChanged()
        {
            if (config.levelState.value.running && !_wasPaused)
            {			
                if (_save)
                {
                    SavePositions();
                    return;
                }
                
                 InstantiateBuckets();
                 InstantiateApples();
                 InstantiateFlowers();
                

                if (config.curLevel.value.levelTimeInSec > 0)
                {
                    StartCoroutine(CountDown(config.curLevel.value.levelTimeInSec));
                }
            }
            
            if (config.levelState.value.showPauseMenu)
            {
                _wasPaused = true;
            }
        }

        private void DeactivateOverlappingApplesAndFlowers()
        {
            var appleRadius = _appleRadiusUnitsOnDevice * _appleScaler;
            
            for (var i = 0; i < _curFlowerLevel.chestnutPositions.Count; i++)
            {
                _isApplePositionActiveList.Add(true);
            }
            
            for (var i = 0; i < _curFlowerLevel.flowerPositions.Count; i++)
            {
                _isFlowerPositionActiveList.Add(true);
            }
            
            for (var i = 0; i <= _curFlowerLevel.chestnutPositions.Count - 2; i++)
            {
                if (_isApplePositionActiveList[i] == false)
                {
                    continue;
                }
                var position1 = _curFlowerLevel.chestnutPositions[i];
                
                for (var j = i + 1; j <= _curFlowerLevel.chestnutPositions.Count - 1; j++)
                {
                    if (_isApplePositionActiveList[j] == false)
                    {
                        continue;
                    }
                    var position2 = _curFlowerLevel.chestnutPositions[j];
                    _isApplePositionActiveList[j] = !CheckIntersectionOfCircles(position1,position2, appleRadius);
                }

                for (var k = 0; k < _curFlowerLevel.flowerPositions.Count; k++)
                {
                    if (!_isFlowerPositionActiveList[k])
                    {
                        continue;
                    }
                    var position3 = _curFlowerLevel.flowerPositions[k];
                    _isFlowerPositionActiveList[k] = !CheckIntersectionOfCircles(position1,position3, appleRadius);
                }
            }
            
            foreach (var activeOrNot in _isApplePositionActiveList)
            {
                _applePositionsAmount += activeOrNot ? 1 : 0;
                Debug.LogFormat("{0}", activeOrNot);
            }
        }
        private void InstantiateApples()
        {
            for (var i = 0; i < _curFlowerLevel.chestnutPositions.Count; i++)
            {
                if (_isApplePositionActiveList[i] == false)
                {
                    continue;
                }
                var apple = Instantiate(_applePrefabs[i % 2]);
                var scaledPos = _curFlowerLevel.chestnutPositions[i];
                scaledPos.x *= _posXScaler;
                scaledPos.x += _flowerbedToZeroOffsetXInUnits;
                scaledPos.z *= _posZScaler;
                apple.Init(scaledPos, _appleScaler, _flowerbedReference.transform, RandomFloatInRange(0.5f, 2f));
                _activeTouchables.Add(apple);
            }
        }
        
        private bool CheckIntersectionOfCircles(Vector3 center1, Vector3 center2, float rad)
        {
            return (Vector3.Distance(center1, center2) <= (2 * rad));
        }


        private void InstantiateFlowers()
        {
            for (var i = 0; i < _curFlowerLevel.flowerPositions.Count; i++)
            {   
                if (_isFlowerPositionActiveList[i] == false)
                {
                    continue;
                }
                var rand = GetRandomNumberInRange(0, _flowerPrefabs.Count);
                var flower = Instantiate(_flowerPrefabs[rand]);
                var scaledPos = _curFlowerLevel.flowerPositions[i];
                scaledPos.x *= _posXScaler;
                scaledPos.x += _flowerbedToZeroOffsetXInUnits;
                scaledPos.z *= _posZScaler;
                flower.transform.localScale *= Mathf.Min(_appleScaler, 1.5f);
                flower.transform.position = scaledPos;
                //flower.transform.SetParent(_flowerbedReference.transform, true);
            }
        }

        private void InstantiateBuckets()
        {
            if(_curFlowerLevel.bucketPositions.Count >= 2)
            {
                int i = 0;
                while (i < 2)
                {
                    var bucket = Instantiate(_bucketPrefab);
                    if (i == 0)
                    {
                        bucket.SetRed(true);
                    }
                    else
                    {
                        bucket.SetRed(false);
                    }
                    
                    var scaledPos = _curFlowerLevel.bucketPositions[i];
                    scaledPos.x *= _posXScaler;
                    scaledPos.x += _flowerbedToZeroOffsetXInUnits;
                    scaledPos.z *= _posZScaler;
                    var scale = bucket.GetScale();
                    var tiling = 1f;
                    if (_appleScaler > 1)
                    {    
                        scale.x *= _appleScaler;
                        tiling = _appleScaler;
                    }
                    bucket.Init(scaledPos, scale, tiling);
                    //bucket.transform.SetParent(_flowerbedReference.transform, true);
                    i++;
                }
            }
        }
        
        private void SavePositions()
        {
            foreach (var go in _applesToSave)
            {
                _curFlowerLevel.chestnutPositions.Add(go.transform.position);
            }
            foreach (var go in _flowersToSave)
            {
                _curFlowerLevel.flowerPositions.Add(go.transform.position);
            }
            foreach (var go in _bucketsToSave)
            {
                _curFlowerLevel.bucketPositions.Add(go.transform.position);
            }           
        }

        public void OnRemoveAppleGameEvent(AppleControllerGameEvent e)
        {
            _recycledApples.Add(e.appleController);
            _activeTouchables.Remove(e.appleController);
            CheckIfNeedToRespawn();
        }

        private void CheckIfNeedToRespawn()
        {
            if (_curFlowerLevel.levelTimeInSec < 0 || _time < 12)
            {
                if (_activeTouchables.Count == 0 || _time <= 0)
                {
                    GameIsOver();
                }    
            }
            else
            {
                if (_recycledApples.Count >= _randomSpawnNumber)
                {
                    var randomAmount = GetRandomNumberInRange(1, _recycledApples.Count + 1);
                    RespawnApples(randomAmount);
                    _randomSpawnNumber = GetRandomNumberInRange(_randomSpawnNumber - randomAmount, (_applePositionsAmount + 1));
                }
            }
        }

        private void RespawnApples(int amount)
        {
            var i = 0;
            while(i < amount)
            {
                var randomPos = GetRandomNumberInRange(0, _recycledApples.Count);
                var apple = _recycledApples[randomPos];
                _activeTouchables.Add(apple);
                _recycledApples.Remove(apple);
                apple.Respawn();
                i++;
            }
        }

        private float RandomFloatInRange(float from, float to)
        {
            return Random.Range(from, to);
        }
                
        public override void OnCanceledButtonPressed()
        {
            ClearValues();
            base.OnCanceledButtonPressed();
        }

        private void ClearValues()
        {
            _curFlowerLevel.panInfoObjects.Clear();
            _curFlowerLevel.redYellowInTargetBucket = 0;
            _curFlowerLevel.redYellowInWrongBucket = 0;
            _curFlowerLevel.greenBlueInTargetBucket = 0;
            _curFlowerLevel.greenBlueInWrongBucket = 0;
        }
    }
}