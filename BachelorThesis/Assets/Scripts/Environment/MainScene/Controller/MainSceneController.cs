﻿using Common.Events;
using Common.Variables;
using UnityEngine;

namespace Environment.MainScene.Controller
{
    public class MainSceneController : MonoBehaviour
    {
        [SerializeField] private FloatVariable _pixelsPerPoint;
        [SerializeField] private GameEvent _displayTotalPointsGameEvent;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private Vector2Variable _resolution;

        private void Awake()
        {
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            SetPixelsPerPoint();
            SetDeviceInfo();
        }

        private void Start()
        {
            if (_displayTotalPointsGameEvent != null)
            {
                _displayTotalPointsGameEvent.Raise();
            }
        }

        private void SetDeviceInfo()
        {
            _playerData.deviceID = SystemInfo.deviceUniqueIdentifier;
            _playerData.handHeld = SystemInfo.deviceType == DeviceType.Handheld;
        }

        private void SetPixelsPerPoint()
        {
            var dpi = Screen.dpi;

            Debug.Log("Screen DPI from unity: " + dpi);
            Debug.Log("Screen XDPI: " + DisplayMetricsAndroid.XDPI);
            Debug.Log("Screen YDPI: " + DisplayMetricsAndroid.YDPI);

            if (Application.platform == RuntimePlatform.Android)
            {
                dpi = DisplayMetricsAndroid.DensityDPI > 0 ? DisplayMetricsAndroid.DensityDPI : Screen.dpi;
                Debug.Log("Platform: Android");
                
                if (dpi <= 213)
                {
                    _pixelsPerPoint.value = 1f;
                }
                else if (dpi <= 240)
                {
                    _pixelsPerPoint.value = 1.5f;
                }
                else if (dpi <= 320)
                {
                    _pixelsPerPoint.value = 2f;
                }
                else if (dpi <= 480)
                {
                    _pixelsPerPoint.value = 3f;
                }
                else if (dpi <= 640)
                {
                    _pixelsPerPoint.value = 4f;
                }
                else
                {
                    //should not happen
                    _pixelsPerPoint.value = 1f;
                }
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer || SystemInfo.deviceModel.Contains("iPad"))
            {
                Debug.Log("Platform: iOS");
                
                if (dpi <= 263)
                {
                    // dpi should be 132 or 163
                    _pixelsPerPoint.value = 1f;
                }
                else if (dpi <= 400)
                {
                    // dpi should be 264 or 326
                    _pixelsPerPoint.value = 2f;
                }
                else if (dpi <= 458)
                {
                    // dpi should be 401 or 458
                    _pixelsPerPoint.value = 3f;
                }
                else
                {
                    //should not happen
                    _pixelsPerPoint.value = 1f;
                }
            }

            var w = Screen.width;
            var h = Screen.height;

            _resolution.value.x = Mathf.Max(w, h);
            _resolution.value.y = Mathf.Min(w, h);

            _playerData.deviceSizeInCm =
                new Vector2(_resolution.value.x / dpi * 2.54f, _resolution.value.y / dpi * 2.54f);
            _playerData.deviceSizeInPixel = new Vector2(_resolution.value.x, _resolution.value.y);
            _playerData.dpi = dpi;
            _playerData.ppp = _pixelsPerPoint.value;
            _playerData.deviceType = SystemInfo.operatingSystem + " " + SystemInfo.deviceModel;
            Debug.Log("Pixels per Point: " + _pixelsPerPoint.value);
            Debug.Log("Density: " + DisplayMetricsAndroid.Density);
            Debug.Log("DensityDPI: " + DisplayMetricsAndroid.DensityDPI);
            Debug.Log("ScaledDensity: " + DisplayMetricsAndroid.ScaledDensity);

        }
    }

    public class DisplayMetricsAndroid
    {

        // The logical density of the display
        public static float Density { get; protected set; }

        // The screen density expressed as dots-per-inch
        public static int DensityDPI { get; protected set; }

        // The absolute height of the display in pixels
        public static int HeightPixels { get; protected set; }

        // The absolute width of the display in pixels
        public static int WidthPixels { get; protected set; }

        // A scaling factor for fonts displayed on the display
        public static float ScaledDensity { get; protected set; }

        // The exact physical pixels per inch of the screen in the X dimension
        public static float XDPI { get; protected set; }

        // The exact physical pixels per inch of the screen in the Y dimension
        public static float YDPI { get; protected set; }

        static DisplayMetricsAndroid()
        {
            // Early out if we're not on an Android device
            if (Application.platform != RuntimePlatform.Android)
            {
                return;
            }
            // http://developer.android.com/reference/android/util/DisplayMetrics.html

            using (
                AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"),
                metricsClass = new AndroidJavaClass("android.util.DisplayMetrics")
            )
            {
                using (
                    AndroidJavaObject metricsInstance = new AndroidJavaObject("android.util.DisplayMetrics"),
                    activityInstance = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"),
                    windowManagerInstance = activityInstance.Call<AndroidJavaObject>("getWindowManager"),
                    displayInstance = windowManagerInstance.Call<AndroidJavaObject>("getDefaultDisplay")
                )
                {
                    displayInstance.Call("getMetrics", metricsInstance);
                    Density = metricsInstance.Get<float>("density");
                    DensityDPI = metricsInstance.Get<int>("densityDpi");
                    HeightPixels = metricsInstance.Get<int>("heightPixels");
                    WidthPixels = metricsInstance.Get<int>("widthPixels");
                    ScaledDensity = metricsInstance.Get<float>("scaledDensity");
                    XDPI = metricsInstance.Get<float>("xdpi");
                    YDPI = metricsInstance.Get<float>("ydpi");
                }
            }
        }
    }
}
