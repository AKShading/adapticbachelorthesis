using Common.Events;
using Common.Variables;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

namespace Environment.MainScene.Controller
{
    public class MainMenuController : MonoBehaviour
    {    
        [SerializeField] private Button _startButton;
        [SerializeField] private Animator _animator;
        [SerializeField] private TMP_InputField _nameInput;
        [SerializeField] private TMP_InputField _ageInput;
        [SerializeField] private TMP_InputField _emailInput;
        [SerializeField] private TMP_Dropdown _genderDropdown;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private GameEvent _enteredFirstStartDataGameEvent;
        private static readonly int ShowFirstStartMenu = Animator.StringToHash("ShowFirstStartMenu");
        private static readonly int ShowMainMenu = Animator.StringToHash("ShowMainMenu");

        private void OnEnable()
        {
            Debug.Log("MainMenuController is there");
            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }
        }

        public void OnFinishedGame()
        {
            _startButton.interactable = false;
        }

        public void OnStarted(bool first)
        {
            if (first)
            {
                OnFirstStart();
            }
            else
            {
                Debug.Log("received start event");
                _animator.SetTrigger(ShowMainMenu);
            }
        }
        
        private void OnFirstStart()
        {
            Debug.Log("received first start event");
            _animator.SetTrigger(ShowFirstStartMenu);
        }

        public void OnDoneButtonPressed()
        {
            var pname = _nameInput.text;
            var email = _emailInput.text;
            
            int age;
            var number = int.TryParse(_ageInput.text, out age);

            if (!number || pname == "")
            {
                return;
            }

            var gender = _genderDropdown.options[_genderDropdown.value].text;    

            _playerData.playerName = pname;
            _playerData.playerAge = age;
            _playerData.gender = gender;
            _playerData.email = email;
            
            _animator.SetTrigger(ShowMainMenu);
            
            if(_enteredFirstStartDataGameEvent != null)
            {
                _enteredFirstStartDataGameEvent.Raise();    
            }
        }
    }
}