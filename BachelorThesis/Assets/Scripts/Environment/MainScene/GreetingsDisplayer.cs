using Common.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace Environment.MainScene
{
    public class GreetingsDisplayer : MonoBehaviour
    {
        [SerializeField] private Text _text;
        [SerializeField] private string _additionalText;
        [SerializeField] private PlayerData _playerData;

        private void OnEnable()
        {
            SetGreeting();
        }

        public void OnUpdateName()
        {
            SetGreeting();
        }

        private void SetGreeting()
        {
            _text.text = _playerData.playerName == "" ? string.Format("{0}!", _additionalText) : string.Format("{0} {1}!", _additionalText, _playerData.playerName);
        }
    }
}