using UnityEngine;

namespace Environment.WaterGame.Controller
{
    public class SkyboxRotator : MonoBehaviour
    {
        [SerializeField] private float _skyboxSpeed = 1f;
        [SerializeField] private Skybox _skybox;
        
        private static readonly int Rotation = Shader.PropertyToID("_Rotation");

        private void Update()
        {
            _skybox.material.SetFloat(Rotation, Time.time * _skyboxSpeed);
        }
    }
}