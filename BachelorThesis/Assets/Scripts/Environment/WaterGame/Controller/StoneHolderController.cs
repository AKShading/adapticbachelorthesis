using UnityEngine;

namespace Environment.WaterGame.Controller
{
    public class StoneHolderController : MonoBehaviour
    {
        [SerializeField] private Throwable _stone;
        private Vector3 _respawnPosition;
        private Vector3 _respawnRotation;
        private Vector3 _stoneRot;

        private void Start()
        {
            NeedRespawn();  
        }

        public void NeedRespawn()
        {
            transform.position = _respawnPosition;
            transform.eulerAngles = _respawnRotation;
            _stone.transform.eulerAngles = _stoneRot;
        }

        public void SetRespawnPosAndRotation(Vector3 pos, Vector3 rot, Vector3 childRot)
        {
            _respawnPosition = pos;
            _respawnRotation = rot;
            _stoneRot = childRot;
        }
    }
}