﻿using System.Collections;
using Common.Events;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace Environment.WaterGame.Controller
{
    public class Throwable : MonoBehaviour
    {
        [SerializeField] private GameObject _splash;
        [SerializeField] private Vector3 _gravity;
        [SerializeField] private float _maxFallingSpeed = -40f;
        [SerializeField] private float _maxMoveSpeed = 10f;

        [SerializeField] private Vector3 _move = Vector3.zero;
        [SerializeField] private Vector3 _bounceForce;
        [SerializeField] private float _throwForce;

        [SerializeField] private GameEvent _respawned;
        
        private float _varBounceForce;
        [SerializeField] private float _varThrowForce;

        private bool _bouncing;
        private bool _thrown;
        private Vector3 _localGravity;
        private bool _respawning;

        [SerializeField] private StoneHolderController _parent;
        
//        [SerializeField] private CamBase _camBase;

        private void OnEnable()
        {
            SetGravity();
            if (_parent == null)
            {
                GetComponentInParent<StoneHolderController>();
            }
        }

        private void Awake()
        {
            //          Debug.Assert(_camBase != null, "_camBase != null");
        }

        private void FixedUpdate()
        {   
            if (_thrown)
            {
                CalculateMove();
                if (!_respawning && (transform.position.y < 0f || _move.z < 2f))
                {
                    Debug.LogFormat("STONE UPDATE: Pos y: {0}, move z: {1}, respawning: {2}", transform.position.y, _move.z, _respawning);
                    var pos = transform.position;
                    pos.y = -1;
                    transform.position = pos;
                    Respawn();
                }
                else
                {
                    transform.localPosition += _move * Time.deltaTime;   
                }
                
                _bouncing = false;
            }
        }

        public void Respawn()
        {
            StartCoroutine(WaitForRespawn());         
        }
        
        private void CalculateMove()
        {
            _move = _move + (_localGravity + (_bouncing ? _bounceForce * _varBounceForce : Vector3.zero)) * Time.deltaTime;
            _move.z = _throwForce * _varThrowForce * Time.deltaTime;
            _move.z = Mathf.Clamp(_move.z, 0f, _maxMoveSpeed);
            _move.y = Mathf.Max(_maxFallingSpeed, _move.y);
        }
        
        private void OnTriggerEnter(Collider other)
        {            
            _move.y = 0.0f;
            _move.z = 0.0f;
            
            var bounceForceComponent = other.gameObject.GetComponent<BounceForce>();
            if (bounceForceComponent == null)
            {
                _varThrowForce = 0;
                _localGravity = Vector3.zero;
                Debug.LogFormat("Respawn from stone, pos is: {0}, local pos is: {1}, collider: {2}", transform.position, transform.localPosition, other.transform.name);
                Respawn();
            }
            else
            {
                Splash(transform.position);
                _bouncing = true;
                _varThrowForce *= 0.6f;
                _varBounceForce = bounceForceComponent.BounceForceFactor;   
            }
        }

        private void Splash(Vector3 pos)
        {
            var splash = Instantiate(_splash);
            splash.transform.position = pos;
        }
        
        IEnumerator WaitForRespawn()
        {
            _respawning = true;
            yield return new WaitForSeconds(1f);
            _thrown = _bouncing = false;
            _varBounceForce = _varThrowForce = 0;
            _move = Vector3.zero;
            _parent.NeedRespawn();
            transform.localPosition = Vector3.zero;
            SetGravity();
            _respawning = false;
            if (_respawned != null)
            {
                _respawned.Raise();
            }
        }
        
        public void Throw(float throwForce)
        {
            _varThrowForce = throwForce;
            _thrown = true;
            transform.eulerAngles = Vector3.zero;
        }
               
        private void SetGravity()
        {
            _localGravity = _gravity;
        }
    }
}
