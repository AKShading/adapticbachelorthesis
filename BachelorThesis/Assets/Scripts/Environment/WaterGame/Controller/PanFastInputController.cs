using System.Collections.Generic;
using Common.Configs;
using Common.Events;
using Common.Variables;
using Environment.MoleGame.Mole;
using Environment.WaterGame.Controller;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Environment.FlowerbedGame.Controller
{
    public class PanFastInputController : InputController
    {
        [SerializeField] private bool _debugDistance;
        [SerializeField] private GameObject _stone;
        [SerializeField] private GameEvent _panCompleteGameEvent;
        [SerializeField] private LevelStateVariable _levelstate;
        [SerializeField] private PlayerData _playerData;
        [SerializeField] private bool _panAnywhere;
        [SerializeField] private bool _debugPanDistance;
        
        private bool _waitForNextTurn;
        private int _fingerID;
        private Vector3 _oldPos = Vector3.zero;
        
        private WaterLevelConfig _curWaterLevel
        {
            get
            {
                return _config.curLevel.value as WaterLevelConfig;
            }
        }

        private void OnEnable()
        {
            _panAnywhere = _playerData.tremble;
        }

        private void Update()
        {          
            if (!_levelstate.value.running)
            {
                return;
            }
            
            if (Input.touchCount > 0)
            {
                for (var i = 0; i < Input.touchCount; ++i)
                {
                    var touch = Input.GetTouch(i);
                             
                    if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                    {
                        return;
                    }
                    
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (_waitForNextTurn)
                        {
                            return;
                        }
                        ReactOnInput(touch.position);
                        _fingerID = touch.fingerId;
                        AddValueToAccelerometerList();
                    }

                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (!_waitForNextTurn && _fingerID == touch.fingerId)
                        {
                            SavePanValues(touch.position);
                        }
                    }

                    if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        if (!_waitForNextTurn && _fingerID == touch.fingerId)
                        {
                            CompletePan();
                        }
                        AddValueToAccelerometerList();
                    }
                }
            }
            else
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                
                var pos = Input.mousePosition;
                
                if (Input.GetMouseButtonDown(0))
                {
                    if (_waitForNextTurn)
                    {
                        return;
                    }
                    ReactOnInput(pos);
                }

                if (Input.GetMouseButton(0))
                {
                    if (!_waitForNextTurn)
                    {
                        SavePanValues(pos);
                    }
                }

                if (Input.GetMouseButtonUp(0))
                {
                    if (!_waitForNextTurn)
                    {
                        CompletePan();
                    }
                }
            }   
            
        }

        private void SavePanValues(Vector3 inputPos)
        {
            var pos = GetComponent<GameController>().VectorInPixelsToUnits(inputPos);
         
            if (_oldPos == Vector3.zero)
            {
                _oldPos = pos;
            }
            
            //pos.y = _draggableObjectInfo.draggableObject.transform.position.y;
            //_draggableObjectInfo.draggableObject.transform.position = pos;
            if(_debugDistance) VisualizeDistance(_oldPos, pos, Color.white, 3f);     
            _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count-1].pointsOnPath.Add(CreatePanPointInfoObject(inputPos));
            _oldPos = pos;
        }

        private PanPointInfo CreatePanPointInfoObject(Vector3 pos)
        { 
            // evtl raycast if hit stone while paning to check 
            return new PanPointInfo{pos = pos, hitObj = false};
        }
        private void CompletePan()
        {
            CompletePanInfo();
            _fingerID = -1;

            if (!_panAnywhere && !_curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].tapInit.hitTarget)
            {
                return;
            }
            
            _waitForNextTurn = true;
            
            if (_panCompleteGameEvent != null)
            {
                _panCompleteGameEvent.Raise();
            }
        }

        private void CompletePanInfo()
        {
            _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].distance = CalculatePanDistance(_curWaterLevel.panInfoObjects);
            Debug.LogFormat("PanDistance is: {0}", _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].distance);
            if(_debugPanDistance) VisualizePanDistance(_curWaterLevel.panInfoObjects);
            _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].normalizedDistance = CalculateNormalizedPanDistance(_curWaterLevel.panInfoObjects);
            _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].duration = CalculatePanDuration(_curWaterLevel.panInfoObjects);
        }

        private float CalculateNormalizedPanDistance(List<PanInfo> panInfoObjects)
        {
            var normDist = 0f;

            if (panInfoObjects.Count > 0 && panInfoObjects[panInfoObjects.Count - 1].pointsOnPath.Count > 0)
            {
                var firstPos = panInfoObjects[panInfoObjects.Count - 1].tapInit.tapPos;
                var lastPos = panInfoObjects[panInfoObjects.Count - 1].pointsOnPath[panInfoObjects[panInfoObjects.Count - 1].pointsOnPath.Count - 1].pos;

                normDist = (lastPos - firstPos).magnitude;

                Debug.LogFormat("Normalized distance in pixels: {0}", normDist);
            }
            return normDist;
        }

        private void VisualizePanDistance(List<PanInfo> panInfoObjects)
        {
            var lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.SetWidth(0.01f, 0.01f);
            lineRenderer.positionCount = panInfoObjects[panInfoObjects.Count - 1].pointsOnPath.Count+2;
            var camZ = Mathf.Abs(_mainCam.transform.position.z);
            var newPos = panInfoObjects[panInfoObjects.Count - 1].tapInit.tapPos;
            newPos.z = camZ;
            var worldPoint = _mainCam.ScreenToWorldPoint(newPos);
            lineRenderer.SetPosition(0, worldPoint);
            int i = 1;
            foreach (var panPointInfo in panInfoObjects[panInfoObjects.Count - 1].pointsOnPath)
            {
                newPos = panPointInfo.pos;
                newPos.z = camZ;
                worldPoint = _mainCam.ScreenToWorldPoint(newPos);
                lineRenderer.SetPosition(i, worldPoint);
               // Debug.LogFormat("Pos in Pixel: {0}. Pos in Units: {1}", panPointInfo.pos, worldPoint);
                i++;
            }
            
            newPos = panInfoObjects[panInfoObjects.Count - 1].tapInit.tapPos;
            newPos.z = camZ;
            worldPoint = _mainCam.ScreenToWorldPoint(newPos);
            lineRenderer.SetPosition(i, worldPoint);

        }
        private void ReactOnInput(Vector3 pos)
        {
            VisualizeTap(pos);
            DoRaycastAndSaveTapInfo(pos);
        }

        private void DoRaycastAndSaveTapInfo(Vector3 pos)
        {
            var ray = _mainCam.ScreenPointToRay(pos);
            var hit = false;

            RaycastHit raycastHit;

            if (Physics.Raycast(ray, out raycastHit, Mathf.Infinity))
            {
                var stone = raycastHit.transform.GetComponent<Throwable>();
                CreatePanInfoObject(stone, pos);
            }
            else
            {
                CreatePanInfoObject(null, pos);
                DrawRay(ray, 1000f, Color.white, 10f);
            }
        }     
        
        private void CreatePanInfoObject(Throwable throwableObj, Vector3 pos)
        {
            var panInfo = new PanInfo {tapInit = new TapInitInfo{hitTarget = throwableObj != null, tapPos = pos, tapTime = Time.time}};
            _curWaterLevel.panInfoObjects.Add(panInfo);
        }
              
        private void ThrowStone()
        {
            // in gamecontroller 
            _waitForNextTurn = false;
        }
        
        private void VisualizeTap(Vector3 touchPos)
        {
            if(_debugTapPos) _tapVisualizer.transform.position = touchPos;
        }
        
        private void VisualizeDistance(Vector3 pos1, Vector3 pos2, Color color, float duration)
        {
            Debug.DrawLine(pos1, pos2, color, duration);
        }

        public void OnNewPanPossible()
        {
            _waitForNextTurn = false;
        }
    }
}