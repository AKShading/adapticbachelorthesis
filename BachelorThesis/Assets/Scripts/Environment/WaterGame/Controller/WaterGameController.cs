using System;
using System.Collections;
using System.Collections.Generic;
using Common.Configs;
using Common.Variables;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.U2D;

namespace Environment.WaterGame.Controller
{
    public class 
        WaterGameController : GameController
    {
        [SerializeField] private IntVariable _left;
        [SerializeField] private IntVariable _right;
        [SerializeField] private Throwable _stone;
        [SerializeField] private StoneHolderController _stoneHolder;
        [SerializeField] private BounceForce _water;
        [SerializeField] private List<WaterLevelConfig> _levels;
        [SerializeField] private int _throwPosSwitchCount; 
        [SerializeField] private int _throwCount;
        [SerializeField] private PlayerData _playerData;
        
        private int _amountOfPositions;
        private float _startPosX;
        private float _spaceX;
        private float _maxRot;
        private bool _gameOver;
        private float _cameraAspect;
        private WaterLevelConfig _curWaterLevel
        {
            get
            {
                return config.curLevel.value as WaterLevelConfig;
            }
        }

        protected override void Start()
        {
            _stoneHolder.gameObject.SetActive(false);
            _throwCount = 0;
            _amountOfPositions = 0;
            
            config.curLevel.value = _levels[0];
            
            foreach (var level in _levels)
            {
                if (!level.levelDone)
                {
                    config.curLevel.value = level;
                    break;
                }
            }

            ClearValues();
            
            if (_curWaterLevel.portraitModus)
            {
                if (SystemInfo.deviceType != DeviceType.Handheld)
                {
                    _curWaterLevel.portraitModus = false;
                }
            }
                       
            Screen.orientation = _curWaterLevel.portraitModus ? ScreenOrientation.Portrait : ScreenOrientation.LandscapeLeft;
            var w = Screen.width;
            var h = Screen.height;

            _screenHeight = _curWaterLevel.portraitModus ? Mathf.Max(w, h) : Mathf.Min(w, h);
            _screenWidth = _curWaterLevel.portraitModus ? Mathf.Min(w, h) : Mathf.Max(w, h);
            
            _resolution.value.x = _screenWidth;
            _resolution.value.y = _screenHeight;
            
            _cameraAspect = _screenWidth / (float) _screenHeight;
            _curWaterLevel.startFromRight = _playerData.handedness != _left;

            RaiseDontNeedCountdownGameEventIfNeeded();
            
            CalculateValues();
            
            _throwPosSwitchCount = _amountOfPositions > 1 ? 1 : 0;
           
            CalcAndSetThrowPos(_throwPosSwitchCount);
            OnSceneReady();
        }

        public void OnPanComplete()
        {
            var distance = _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].distance;
            var duration = _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].duration;
            var minPanDist = _screenHeight * (_playerData.tremble ? 0f : 0.33f);
            var successful = distance > minPanDist;
            _curWaterLevel.panInfoObjects[_curWaterLevel.panInfoObjects.Count - 1].endedSuccessfully = successful;
            duration = duration <= 0f ? 0.001f : duration;
            var speed = successful ? distance / duration / 200f : 1f;

            speed = Mathf.Max(speed, 1f);          
            
            Debug.LogFormat("Pan is complete, now throw with speed: {0}", speed);
            _water.SetInitForce(speed);
            _stone.Throw(speed); 
            _throwCount++;   
            CheckThrowCount();
        }

        private void CheckThrowCount()
        {
            if (_throwCount >= _curWaterLevel.amountOfThrowingsPerPos)
            {
                _throwCount = 0;
                _throwPosSwitchCount++;
                if (_throwPosSwitchCount >= _amountOfPositions - 1)
                {
                    _gameOver = true;
                }
                else
                {
                    CalcAndSetThrowPos(_throwPosSwitchCount);
                }
            }
        }

        public void OnRespawned()
        {
            if (_gameOver)
            {
                GameIsOver();   
            }
        }

        private void CalculateValues()
        {
            _amountOfPositions = _curWaterLevel.amountOfPositions > 1 ? Mathf.Max(7,Mathf.RoundToInt(_screenWidth / config.pixelsPerPoint.value / config.referenceScreenResolution.value.x * _curWaterLevel.amountOfPositions)) : 1;
            //if even we need to make it odd
            _amountOfPositions += _amountOfPositions % 2 == 0 ? -1 : 0;
            Debug.LogFormat("Amount of throw positions: {0}.", _amountOfPositions);
            // how many positions we need left and right (one is in the middle)
            var posPerSide = (_amountOfPositions - 1) * 0.5;
            var verticalFovInRad = _camera.fieldOfView * Mathf.Deg2Rad * 0.5f;
            var cameraHeightAtPos = Mathf.Tan(verticalFovInRad);
            var heightInUnits = (2f * cameraHeightAtPos) * (_stone.transform.position.z - _camera.transform.position.z);
            Debug.LogFormat("Height in units is: {0}.", heightInUnits);
            _screenWidthUnits = _screenWidth / (float) _screenHeight * heightInUnits;
            Debug.LogFormat("Width in units is: {0}.", _screenWidthUnits);
                      
            var horizontalFovInRad = Mathf.Atan(cameraHeightAtPos * _cameraAspect) * 2;
            var horizontalFov = horizontalFovInRad * Mathf.Rad2Deg;
            Debug.Log("horizontal FOV: " + horizontalFov);
            
            // zero pos should have 0 rotation, left and right should have horizontalFov in angles together, so we need positive and negativ values
            _maxRot = horizontalFov * 0.5f;
            
            _spaceX = _screenWidthUnits / (_amountOfPositions);
            _startPosX = _curWaterLevel.startFromRight ? _screenWidthUnits * 0.5f : -_screenWidthUnits * 0.5f;
        }

        private void CalcAndSetThrowPos(int index)
        {
            var posX = _startPosX + ((_curWaterLevel.startFromRight ? -1 : 1) * (index * _spaceX + (_spaceX * 0.5f)));
            Debug.LogFormat("StartPosX is {0}", posX);
            var pos = _stoneHolder.transform.position;
            pos.x = posX;
            var rot = _maxRot * (posX / (_screenWidthUnits * 0.5f));                  
            var oldRot = _stoneHolder.transform.eulerAngles;
            oldRot.y = rot;
            var stoneRot = oldRot;
            stoneRot.z = rot * 0.5f;
            _stoneHolder.SetRespawnPosAndRotation(pos, oldRot, stoneRot);
        }

        public void OnLevelStateChanged()
        {
            if (config.levelState.value.running && !_wasPaused)
            {			
                _stoneHolder.gameObject.SetActive(true);
                
                if (config.curLevel.value.levelTimeInSec > 0)
                {
                    StartCoroutine(CountDown(config.curLevel.value.levelTimeInSec));
                }
            }
            
            if (config.levelState.value.showPauseMenu)
            {
                _wasPaused = true;
            }
        }
        
        public override void OnCanceledButtonPressed()
        {
            ClearValues();
            base.OnCanceledButtonPressed();
        }

        private void ClearValues()
        {
            _curWaterLevel.panInfoObjects.Clear();
        }
    }
}