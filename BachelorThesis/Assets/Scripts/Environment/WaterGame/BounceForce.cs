using Common.Events;
using UnityEngine;

namespace Environment.WaterGame
{
    public class BounceForce : MonoBehaviour
    {
        private const int MAX_SPEED = 25;
        private const int GOOD_SPEED = 17;
        private const int NORM_SPEED = 10;
        private const int SLOW_SPEED = 5;

        [SerializeField] private IntGameEvent _bouncedGameEvent;
        [SerializeField] private float _bounceForce = 1f;
        [SerializeField] private float _countFactor;
        [SerializeField] private int _counter;
        private float _speed;
        private bool _varCountFactor;
        
        public float BounceForceFactor
        {
            get
            {
                if (_bouncedGameEvent != null)
                {
                    _bouncedGameEvent.parameter = _counter;
                    _bouncedGameEvent.Raise();
                }
                
                _counter++;
                if (_varCountFactor)
                {
                    CalcCountFactor();
                }
                _bounceForce *= _countFactor;
                
                return _bounceForce;
            }
        }

        public void SetInitForce(float speed)
        {
            _speed = speed;
            _varCountFactor = false;
            _counter = 0;
            
            if (_speed >= MAX_SPEED)
            {
                _bounceForce = 6;
                CalcCountFactor();
                _varCountFactor = true;
            }
            else if (_speed >= GOOD_SPEED)
            {
                _bounceForce = 6;
                _countFactor = 0.9f;

            }
            else if (_speed >= NORM_SPEED)
            {
                _bounceForce = 5;
                CalcCountFactor();
                _varCountFactor = true;

            }
            else if (_speed >= SLOW_SPEED)
            {
                _bounceForce = 5;
                _countFactor = 0f;

            }
            else
            {
                _bounceForce = 3;
                _countFactor = 0f;

            }       
        }

        private void CalcCountFactor()
        {
            if (_speed >= MAX_SPEED)
            {
                _countFactor = _counter >= 2 ? 0.9f : 1f;
            }
            else
            {
                _countFactor = (_bounceForce - 1) / _bounceForce;
            }
        }
        
        public void OnResetCount()
        {
            _counter = 0;
        }
    }
}