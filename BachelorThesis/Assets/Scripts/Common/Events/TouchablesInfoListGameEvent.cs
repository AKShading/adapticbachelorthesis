﻿using System.Collections.Generic;
using UnityEngine;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewMTouchablesInfoListGameEvent", menuName = "CustomSO/Events/TouchablesInfoListGameEvent")]
    public class TouchablesInfoListGameEvent : GameEvent
    {
        public class TouchableInfo
        {
            public Vector3 colliderPosUnits;
        }

        public List<TouchableInfo> parameter;
    }
}