﻿using System;
using Common.Events;
using UnityEngine.Events;

[Serializable]
public class UnityGameEvent : UnityEvent<GameEvent> {}
