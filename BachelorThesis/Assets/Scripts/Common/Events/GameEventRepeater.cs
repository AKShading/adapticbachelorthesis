﻿using Common.Events;
using UnityEngine;

public class GameEventRepeater : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEvent gameEvent;

    [Tooltip("Response to invoke when Event is raised with event as parameter.")]
    public UnityGameEvent response;

    private void OnEnable()
    {
        gameEvent.RegisterRepeater(this);
    }

    private void OnDisable()
    {
        gameEvent.UnregisterRepeater(this);
    }

    public virtual void OnEventRaised()
    {
        response.Invoke(gameEvent);
    }
}
