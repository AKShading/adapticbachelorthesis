﻿using Environment.MoleGame.Mole;
using UnityEngine;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewMoleControllerGameEvent", menuName = "CustomSO/Events/MoleControllerGameEvent")]
    public class MoleControllerGameEvent : GameEvent
    {
        public MoleController moleController;
    }
}
