using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewBoolGameEvent", menuName = "CustomSO/Events/BoolGameEvent")]

    public class BoolGameEvent : GameEvent
    {
        public bool value;
    }
}