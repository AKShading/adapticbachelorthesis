using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewBool2GameEvent", menuName = "CustomSO/Events/Bool2GameEvent")]
    public class HitSthGameEvent : GameEvent
    {
        [FormerlySerializedAs("parameter")] public bool hit;
        [FormerlySerializedAs("flower")] public bool destroyable;
    }
}