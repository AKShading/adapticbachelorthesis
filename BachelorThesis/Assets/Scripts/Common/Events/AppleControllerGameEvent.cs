using Environment.FlowerbedGame.Controller;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewChestnutControllerGameEvent", menuName = "CustomSO/Events/ChestnutControllerGameEvent")]
    public class AppleControllerGameEvent : GameEvent
    {
        [FormerlySerializedAs("chestnutController")] public AppleController appleController;
    }
}
