﻿using UnityEngine;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewFloatGameEvent", menuName = "CustomSO/Events/FloatGameEvent")]
    public class FloatGameEvent : GameEvent
    {
        public float parameter;
    }
}
