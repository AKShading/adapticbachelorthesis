using UnityEngine;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewIntGameEvent", menuName = "CustomSO/Events/IntGameEvent")]
    public class IntGameEvent : GameEvent
    {
        public int parameter;
    }
}