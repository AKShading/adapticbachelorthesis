﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Events
{
    [CreateAssetMenu(fileName = "NewGameEvent", menuName = "CustomSO/Events/GameEvent")]
    public class GameEvent : ScriptableObject
    {
        [TextArea(3,5)]
        public string description;
        private readonly List<GameEventRepeater> _eventRepeaters = new List<GameEventRepeater>();
        public Action<GameEvent> onEvent;
    
        public void Raise()
        {
            for (var i = _eventRepeaters.Count - 1; i >= 0; i--)
            {
                _eventRepeaters[i].OnEventRaised();
            }

            if (onEvent != null)
            {
                onEvent(this);
            }
        }
    
        public void RegisterRepeater(GameEventRepeater repeater)
        {
            if (!_eventRepeaters.Contains(repeater))
            {
                _eventRepeaters.Add(repeater);
            }
        }

        public void UnregisterRepeater(GameEventRepeater repeater)
        {
            if (_eventRepeaters.Contains(repeater))
            {
                _eventRepeaters.Remove(repeater);
            }
        }
    }
}
