﻿using Common.Enum;
using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewLevelStateVariable", menuName = "CustomSO/Types/LevelStateVariable")]
    public class LevelStateVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public LevelStateEnum value;

        public void SetValue(LevelStateEnum val)
        {
            this.value = val;
        }
    }
}
