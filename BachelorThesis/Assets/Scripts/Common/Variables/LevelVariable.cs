using Common.Configs;
using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewLevelVariable", menuName = "CustomSO/Types/LevelVariable")]
    public class LevelVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public LevelConfig value;
    }
}