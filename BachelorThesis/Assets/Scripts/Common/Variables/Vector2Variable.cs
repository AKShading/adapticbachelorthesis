using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewVector2Variable", menuName = "CustomSO/Types/Vector2Variable")]
    public class Vector2Variable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public Vector2 value;
    }
}
