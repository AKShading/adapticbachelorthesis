using System.Collections.Generic;
using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewVector3ListVariable", menuName = "CustomSO/Types/Vector3ListVariable")]
    public class Vector3ListVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public List<Vector3> value;
    }
}