using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewLevelPointsReachedVariable", menuName = "CustomSO/Variables/LevelPointsReachedVariable")]
    public class IntListVariable : ScriptableObject
    {
        public List<int> value = new List<int>();

        public int AddAllElements()
        {
            var total = 0;
            
            foreach (var points in value)
            {
                total += points;
            }
            
            return total;
        }
    }
}