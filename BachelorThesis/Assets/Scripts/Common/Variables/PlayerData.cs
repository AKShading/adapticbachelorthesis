using System;
using System.Collections.Generic;
using System.Linq;
using Common.Configs;
using UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewPlayerData", menuName = "CustomSO/Configs/PlayerData")]
    public class PlayerData : ScriptableObject
    {
        public string playerName;
        public int playerAge;
        public string gender;
        public string email;
        public string deviceID;
        public Vector2 deviceSizeInPixel;
        public float dpi;
        public float ppp;
        public bool handHeld;
        public Vector2 deviceSizeInCm;
        public string deviceType;
        public int language; // 0 = deutsch 1 = englisch
        public float soundVolume;
        public FloatVariable minTapableSize;
        public FloatVariable minPadding;
        //public FloatVariable bestFontSize;
        public bool tremble;
        public bool redGreenBlind;
        public IntVariable handedness;
        public float tapDuration;
        public IntListVariable levelPointsReached;
        public IntListVariable infoButtonPressedCount;
        public BoolListVariable heldInHand; 
        public SendFormConfig sendFormConfig;
        // some more values as highest, lowest... 
        public List<InformationPreparator.LevelInfo> moleLevelInfo = new List<InformationPreparator.LevelInfo>();
        public List<InformationPreparator.LevelInfo> flowerLevelInfo = new List<InformationPreparator.LevelInfo>();
        public List<InformationPreparator.LevelInfo> waterLevelInfo = new List<InformationPreparator.LevelInfo>();

        public void SendData(MonoBehaviour mono)
        {
            if (sendFormConfig != null)
            {
                var heldInHandString = heldInHand.value.Count > 0
                    ? string.Join(";", heldInHand.value.Select(x => x.ToString()).ToArray())
                    : "Accelerometer nicht unterstützt!";
                
                var values = new List<string>
                {
                    deviceID,
                    playerName,
                    string.Format("{0}", playerAge),
                    string.Format("{0}", soundVolume),
                    string.Format("{0}", minTapableSize.value),
                    string.Format("{0}", minPadding.value),
                    string.Format("{0}", tremble),
                    gender,
                    string.Format("{0}", handedness.name),
                    string.Format("{0}", tapDuration),
                    string.Join(";", levelPointsReached.value.Select(x => x.ToString()).ToArray()),
                    string.Join(";", infoButtonPressedCount.value.Select(x => x.ToString()).ToArray()),
                    heldInHandString,
                    email,
                    string.Format("{0}", deviceSizeInPixel),
                    string.Format("{0}", dpi),
                    string.Format("{0}", ppp),
                    deviceType
                };
                
                sendFormConfig.Send(mono, values);
            }   
        }
    }
}