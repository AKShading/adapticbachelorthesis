using System.Collections.Generic;
using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewBoolListVariable", menuName = "CustomSO/Types/BoolListVariable")]
    public class BoolListVariable : ScriptableObject
    {
        public List<bool> value = new List<bool>();
    }
}