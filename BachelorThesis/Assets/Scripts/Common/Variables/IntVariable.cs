﻿using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewIntVariable", menuName = "CustomSO/Types/IntVariable")]
    public class IntVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public int value;

        public void SetValue(int val)
        {
            this.value = val;
        }

        public void SetValue(IntVariable val)
        {
            this.value = val.value;
        }

        public void ApplyChange(int amount)
        {
            value += amount;
        }

        public void ApplyChange(IntVariable amount)
        {
            value += amount.value;
        }
    }
}
