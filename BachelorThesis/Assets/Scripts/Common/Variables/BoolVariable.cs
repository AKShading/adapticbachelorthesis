﻿using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewBoolVariable", menuName = "CustomSO/Types/BoolVariable")]
    public class BoolVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public bool value;

        public void SetValue(bool val)
        {
            this.value = val;
        }

        public void SetValue(BoolVariable val)
        {
            this.value = val.value;
        }

        public void ChangeVariable()
        {
            this.value = !this.value;
        }
   
    }
}
