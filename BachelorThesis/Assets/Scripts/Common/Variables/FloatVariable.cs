﻿using System;
using UnityEngine;

namespace Common.Variables
{
    [CreateAssetMenu(fileName = "NewFloatVariable", menuName = "CustomSO/Types/FloatVariable")]
    public class FloatVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public float value;

        public void SetValue(float val)
        {
            this.value = val;
        }

        public void SetValue(FloatVariable val)
        {
            this.value = val.value;
        }

        public void ApplyChange(float amount)
        {
            value += amount;
        }

        public void ApplyChange(FloatVariable amount)
        {
            value += amount.value;
        }
    }
}
