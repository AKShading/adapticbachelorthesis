﻿using System;
using System.Collections.Generic;
using Common.Variables;
using Environment.MoleGame.Controller;
using UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewMoleLevelConfig", menuName = "CustomSO/Configs/MoleLevelConfig")]
    public class MoleLevelConfig : LevelConfig
    {
        [Serializable]
        public class SizeAndAmount
        {
            public FloatReference size; //could be paddingsize or actualsize
            public int amount;
        }
                 
        public List<InputController.TapInitInfo> tapInitInfoObjects = new List<InputController.TapInitInfo>(); // stores all touch information happened in this level
        public int molesAmountAtSameTime;
        public int molesVisibleTime; //-1 means doesn't disappear 
        public List<SizeAndAmount> sizesAndAmounts;
        public bool bossLevel; 
        public bool paddingLevel;
        // TODO: Save anything needed for evaluation of touches
    }
}
