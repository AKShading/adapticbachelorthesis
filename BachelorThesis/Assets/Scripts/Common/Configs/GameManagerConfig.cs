using Common.Variables;
using UnityEngine;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewGameManagerConfig", menuName = "CustomSO/Configs/GameManagerConfig")]
    public class GameManagerConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public LevelVariable curLevel;
        public bool firstStart;
        public bool finished;
        public int nextScene;
        public bool debug;
    }
}