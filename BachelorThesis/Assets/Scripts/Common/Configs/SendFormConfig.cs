using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewSendFormConfig", menuName = "CustomSO/Configs/SendFormConfig")]
    public class SendFormConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public string BASE_URL;
        public List<string> keys;
        public List<SendDataInfo> values = new List<SendDataInfo>();
        //public bool sendingFailed;

        [Serializable]
        public class SendDataInfo
        {
            public List<string> dataStrings = new List<string>();
            public bool sendingFailed;
        }
        private IEnumerator Post(SendDataInfo data)
        { 
            var form = new WWWForm();
            for (var i = 0; i < data.dataStrings.Count; i++)
            {
                form.AddField(keys[i], data.dataStrings[i]);   
            }
            byte[] ramData = form.data; 
            var www = new WWW(BASE_URL, ramData);
            yield return www;

            data.sendingFailed = www.error != null;
        } 
    
        public void Send(MonoBehaviour monoBehaviour, List<string> data) 
        {
            if (data.Count == keys.Count)
            {
                var sendDataInfo = new SendDataInfo{dataStrings = data};
                values.Add(sendDataInfo);
                monoBehaviour.StartCoroutine(Post(sendDataInfo));
            }
            else
            {
                Debug.Log("Form needs more values than the given list stores!");
            } 
        }

        public void TryManuallyAgain(MonoBehaviour monoBehaviour)
        {
            foreach (var val in values)
            {
                monoBehaviour.StartCoroutine(Post(val));   
            }
        }
        
        public void TryAgain(MonoBehaviour monoBehaviour, SendDataInfo dataInfo)
        {
             monoBehaviour.StartCoroutine(Post(dataInfo));   
        }
    }
}