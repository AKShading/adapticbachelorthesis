using System.Collections.Generic;
using Common.Variables;
using Environment.MoleGame.Controller;
using UI;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewFlowerLevelConfig", menuName = "CustomSO/Configs/FlowerLevelConfig")]
    public class FlowerLevelConfig : LevelConfig
    {
        public bool _extraLevel;
        public List<InputController.PanInfo> panInfoObjects = new List<InputController.PanInfo>(); // stores all touch information happened in this level
        public List<Vector3> bucketPositions;
        public List<Vector3> chestnutPositions;
        public List<Vector3> flowerPositions;
        
        [FormerlySerializedAs("redInTargetBucket")] public int redYellowInTargetBucket;
        [FormerlySerializedAs("redInWrongBucket")] public int redYellowInWrongBucket;
        [FormerlySerializedAs("greenInTargetBucket")] public int greenBlueInTargetBucket;
        [FormerlySerializedAs("greenInWrongBucket")] public int greenBlueInWrongBucket;
        
        // TODO: Save anything needed for evaluation of touches  
    }
}