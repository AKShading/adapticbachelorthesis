using System.Collections.Generic;
using UI;
using UnityEngine;

namespace Common.Configs
{
    public abstract class LevelConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public int levelNumber;
        public float levelTimeInSec;
        public readonly InputController.AccuracyObject avgAccuracy = new InputController.AccuracyObject();       
        public bool levelDone;
        public List<string> description;
        public List<string> levelEnded;
        public bool lastLevel;
        public SendFormConfig sendFormConfig;
        public bool tapLevel;
        public bool panLevel;
        public bool portraitModus;
        public int scoreMark;
    }
}