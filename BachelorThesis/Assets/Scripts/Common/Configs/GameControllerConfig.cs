using System.Collections.Generic;
using Common.Variables;
using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewGameControllerConfig", menuName = "CustomSO/Configs/GameControllerConfig")]
    public class GameControllerConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        public LevelStateVariable levelState;
        public LevelVariable curLevel;      
        public FloatVariable tapAreaActualNeededSize;
        public FloatVariable paddingActual;
        public FloatVariable minTapableSize;
        public FloatVariable suggestedMinSize;
        public Vector2Variable referenceScreenResolution;
        public FloatVariable pixelsPerPoint;
        public IntListVariable levelPointsReached;
        public IntListVariable infoButtonPressed; 
        public IntVariable curLevelScore;
    }
}