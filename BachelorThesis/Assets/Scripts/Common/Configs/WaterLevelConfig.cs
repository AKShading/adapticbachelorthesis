using System.Collections.Generic;
using UI;
using UnityEngine;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewWaterLevelConfig", menuName = "CustomSO/Configs/WaterLevelConfig")]
    public class WaterLevelConfig : LevelConfig
    {
        public List<InputController.PanInfo> panInfoObjects = new List<InputController.PanInfo>(); // stores all touch information happened in this level
        public int amountOfPositions;
        public int amountOfThrowingsPerPos;
        public bool startFromRight;
        public bool initHandednessLevel;
    }
}