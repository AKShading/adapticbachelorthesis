using Common.Enum;
using Common.Variables;
using UnityEngine;

namespace Common.Configs
{
    [CreateAssetMenu(fileName = "NewInputControllerConfig", menuName = "CustomSO/Configs/InputControllerConfig")]
    public class InputControllerConfig : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif     
        public FloatVariable tapAreaActualNeededSize;
        public FloatVariable pixelsPerPoint;
        public AccuracyEnum perfectAccuracy;
        public AccuracyEnum veryGoodAccuracy;
        public AccuracyEnum goodAccuracy;
        public AccuracyEnum okAccuracy;
        public AccuracyEnum badAccuracy;
        public AccuracyEnum undefinedAccuracy;
        public LevelVariable curLevel;
        public LevelStateVariable levelState;
    }
}