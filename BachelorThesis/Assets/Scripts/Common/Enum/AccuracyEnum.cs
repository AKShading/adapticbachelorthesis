﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Enum
{
    [CreateAssetMenu(fileName = "NewAccuracyEnum", menuName = "CustomSO/Enums/AccuracyEnum")]
    public class AccuracyEnum : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif
        [FormerlySerializedAs("name")] public string stringName;
        public float percentageFrom;
        public float percentageTo;
    }
}
