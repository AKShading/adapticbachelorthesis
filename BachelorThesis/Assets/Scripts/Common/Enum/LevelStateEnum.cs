﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Common.Enum
{
    [CreateAssetMenu(fileName = "NewLevelState", menuName = "CustomSO/Enums/LevelStateEnum")]
    public class LevelStateEnum : ScriptableObject
    {
#if UNITY_EDITOR
        [TextArea] public string developerDescription = "";
#endif

        public bool initValues;
        public bool running;
        public bool showGameOverMenu;
        public bool showStartMenu;
        public bool calculateGameOverValues;
        public bool showPauseMenu;
        public bool canceled;
    }
}
