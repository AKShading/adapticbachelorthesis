﻿using Common.Enum;
using Common.Events;
using Environment;
using Environment.MoleGame.Controller;
using UnityEngine;

namespace Common
{
    public class LevelStateBehaviour : StateMachineBehaviour 
    {
        [SerializeField] private LevelStateEnum _levelStateEnum;
        [SerializeField] private GameEvent _levelStateChangedEvent;
	
		
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            animator.GetComponent<GameController>().config.levelState.value = _levelStateEnum;
			
            if (_levelStateChangedEvent)
            {
                _levelStateChangedEvent.Raise();
            }
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //
        //}
    }
}
