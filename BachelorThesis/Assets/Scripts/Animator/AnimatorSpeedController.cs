﻿using UnityEngine;

namespace Environment
{
    public class AnimatorSpeedController : MonoBehaviour
    {
        [SerializeField] private float _animationTime = 1.0f;

        private void Start()
        {
            SetAnimationTime(_animationTime);
        }

        public void SetAnimationTime(float time)
        {
            var animators = GetComponentsInChildren<Animator>();

            foreach (var animator in animators)
            {
                animator.speed = 1.0f / time;
            }
        }
    }
}